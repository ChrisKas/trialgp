package trialGP.live;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class desnationsevent extends Activity implements B4AActivity{
	public static desnationsevent mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "trialGP.live", "trialGP.live.desnationsevent");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (desnationsevent).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "trialGP.live", "trialGP.live.desnationsevent");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "trialGP.live.desnationsevent", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (desnationsevent) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (desnationsevent) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return desnationsevent.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (desnationsevent) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (desnationsevent) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        Object[] o;
        if (permissions.length > 0)
            o = new Object[] {permissions[0], grantResults[0] == 0};
        else
            o = new Object[] {"", false};
        processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.Timer _refreshtimer = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlfooter = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlheader = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnltabs = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnltabs2 = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlqualificationbar = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlday1bar = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlday2bar = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlheadersstart = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgmainlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgleftlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgrightlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imghome = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgarrowstart = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgarrowlive = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbleventname = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblqualification = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblday1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblday2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblstart = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbllive = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblsupport = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblno = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblrider = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblclass = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbltime = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblpos = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbltotal = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblnodata = null;
public anywheresoftware.b4a.objects.ScrollViewWrapper _svresults = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlresults = null;
public static boolean _isqualification = false;
public static boolean _isdayone = false;
public static boolean _isstart = false;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _whitearrow = null;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _greyarrow = null;
public static int _querytype = 0;
public static int _currentscroll = 0;
public trialGP.live.main _main = null;
public trialGP.live.onedayevent _onedayevent = null;
public trialGP.live.twodaysevent _twodaysevent = null;
public trialGP.live.starter _starter = null;
public trialGP.live.misc _misc = null;
public trialGP.live.httputils2service _httputils2service = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
 //BA.debugLineNum = 63;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 64;BA.debugLine="If Main.SQL1.IsInitialized = False Then";
if (mostCurrent._main._sql1.IsInitialized()==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 65;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 66;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 69;BA.debugLine="Activity.LoadLayout(\"DesNationsEventLayout\")";
mostCurrent._activity.LoadLayout("DesNationsEventLayout",mostCurrent.activityBA);
 //BA.debugLineNum = 71;BA.debugLine="Misc.HorizontalCenterView(imgMainLogo, pnlHeader)";
mostCurrent._misc._horizontalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgmainlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 73;BA.debugLine="Misc.VerticalCenterView(imgLeftLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgleftlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 74;BA.debugLine="Misc.VerticalCenterView(imgRightLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgrightlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 75;BA.debugLine="Misc.VerticalCenterView(imgMainLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgmainlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 76;BA.debugLine="Misc.VerticalCenterView(imgHome, pnlFooter)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imghome.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlfooter.getObject())));
 //BA.debugLineNum = 78;BA.debugLine="lblQualification.Width = (pnlTabs.Width / 2) - 4d";
mostCurrent._lblqualification.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)2)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4))));
 //BA.debugLineNum = 79;BA.debugLine="lblDay1.Width = (pnlTabs.Width / 4) - 2dip";
mostCurrent._lblday1.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)4)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 80;BA.debugLine="lblDay2.Width = (pnlTabs.Width / 4) - 2dip";
mostCurrent._lblday2.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)4)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 81;BA.debugLine="pnlQualificationBar.Width = (pnlTabs.Width / 2) -";
mostCurrent._pnlqualificationbar.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)2)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4))));
 //BA.debugLineNum = 82;BA.debugLine="pnlDay1Bar.Width = (pnlTabs.Width / 4) - 2dip";
mostCurrent._pnlday1bar.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)4)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 83;BA.debugLine="pnlDay2Bar.Width = (pnlTabs.Width / 4) - 2dip";
mostCurrent._pnlday2bar.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)4)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 84;BA.debugLine="lblDay1.Left = lblQualification.Left + lblQualifi";
mostCurrent._lblday1.setLeft((int) (mostCurrent._lblqualification.getLeft()+mostCurrent._lblqualification.getWidth()));
 //BA.debugLineNum = 85;BA.debugLine="lblDay2.Left = lblDay1.Left + lblDay1.Width";
mostCurrent._lblday2.setLeft((int) (mostCurrent._lblday1.getLeft()+mostCurrent._lblday1.getWidth()));
 //BA.debugLineNum = 86;BA.debugLine="pnlDay1Bar.Left = pnlQualificationBar.Left + pnlQ";
mostCurrent._pnlday1bar.setLeft((int) (mostCurrent._pnlqualificationbar.getLeft()+mostCurrent._pnlqualificationbar.getWidth()));
 //BA.debugLineNum = 87;BA.debugLine="pnlDay2Bar.Left = pnlDay1Bar.Left + pnlDay1Bar.Wi";
mostCurrent._pnlday2bar.setLeft((int) (mostCurrent._pnlday1bar.getLeft()+mostCurrent._pnlday1bar.getWidth()));
 //BA.debugLineNum = 89;BA.debugLine="lblStart.Width = (pnlTabs2.Width / 2) - 51dip";
mostCurrent._lblstart.setWidth((int) ((mostCurrent._pnltabs2.getWidth()/(double)2)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (51))));
 //BA.debugLineNum = 90;BA.debugLine="lblLive.Width = (pnlTabs2.Width / 2) - 51dip";
mostCurrent._lbllive.setWidth((int) ((mostCurrent._pnltabs2.getWidth()/(double)2)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (51))));
 //BA.debugLineNum = 91;BA.debugLine="lblStart.Left = imgArrowStart.Left + imgArrowStar";
mostCurrent._lblstart.setLeft((int) (mostCurrent._imgarrowstart.getLeft()+mostCurrent._imgarrowstart.getWidth()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (10))));
 //BA.debugLineNum = 92;BA.debugLine="imgArrowLive.Left = pnlTabs2.Width / 2 + 15dip";
mostCurrent._imgarrowlive.setLeft((int) (mostCurrent._pnltabs2.getWidth()/(double)2+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (15))));
 //BA.debugLineNum = 93;BA.debugLine="lblLive.Left = imgArrowLive.Left + imgArrowLive.W";
mostCurrent._lbllive.setLeft((int) (mostCurrent._imgarrowlive.getLeft()+mostCurrent._imgarrowlive.getWidth()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (10))));
 //BA.debugLineNum = 95;BA.debugLine="whiteArrow.Initialize(LoadBitmap(File.DirAssets,";
mostCurrent._whitearrow.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"white-arrow.png").getObject()));
 //BA.debugLineNum = 96;BA.debugLine="greyArrow.Initialize(LoadBitmap(File.DirAssets, \"";
mostCurrent._greyarrow.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"grey-arrow.png").getObject()));
 //BA.debugLineNum = 98;BA.debugLine="pnlResults = SVResults.Panel";
mostCurrent._pnlresults = mostCurrent._svresults.getPanel();
 //BA.debugLineNum = 99;BA.debugLine="lblEventName.Text = Main.eventName";
mostCurrent._lbleventname.setText((Object)(mostCurrent._main._eventname));
 //BA.debugLineNum = 101;BA.debugLine="GetRefreshInterval";
_getrefreshinterval();
 //BA.debugLineNum = 103;BA.debugLine="SetActiveTab(True, False, True)";
_setactivetab(anywheresoftware.b4a.keywords.Common.True,anywheresoftware.b4a.keywords.Common.False,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 104;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 117;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 118;BA.debugLine="refreshTimer.Enabled = False";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 119;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 106;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 107;BA.debugLine="If Main.SQL1.IsInitialized = False Then";
if (mostCurrent._main._sql1.IsInitialized()==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 108;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 109;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 112;BA.debugLine="If refreshTimer.Interval > 0 Then";
if (_refreshtimer.getInterval()>0) { 
 //BA.debugLineNum = 113;BA.debugLine="refreshTimer.Enabled = True";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 115;BA.debugLine="End Sub";
return "";
}
public static String  _clearscreen() throws Exception{
 //BA.debugLineNum = 1463;BA.debugLine="Sub ClearScreen";
 //BA.debugLineNum = 1464;BA.debugLine="lblNoData.Visible = True";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1466;BA.debugLine="pnlResults.RemoveAllViews";
mostCurrent._pnlresults.RemoveAllViews();
 //BA.debugLineNum = 1467;BA.debugLine="End Sub";
return "";
}
public static String  _displaydaylive(int _daynum) throws Exception{
int _placedpanels = 0;
int _placedclasspanels = 0;
int _panelheight = 0;
int _classpanelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
String _currentclass = "";
int _n = 0;
int _i = 0;
String _lvnames = "";
String[] _lvnamesarray = null;
anywheresoftware.b4a.objects.PanelWrapper _classpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblclassname = null;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblposition = null;
anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
String _ridername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblfirstdriver = null;
String _firstdrivername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblseconddriver = null;
String _seconddrivername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblthirddriver = null;
String _thirddrivername = "";
anywheresoftware.b4a.objects.LabelWrapper _lbllap1 = null;
anywheresoftware.b4a.objects.LabelWrapper _lbllap2 = null;
anywheresoftware.b4a.objects.PanelWrapper _classpanelinside = null;
anywheresoftware.b4a.objects.PanelWrapper _classpanelinside2 = null;
anywheresoftware.b4a.objects.LabelWrapper _lbllap1results = null;
anywheresoftware.b4a.objects.LabelWrapper _lbllap2results = null;
anywheresoftware.b4a.objects.LabelWrapper _lblafter1 = null;
anywheresoftware.b4a.objects.LabelWrapper _lblafter2 = null;
anywheresoftware.b4a.objects.LabelWrapper _lblafters1 = null;
anywheresoftware.b4a.objects.LabelWrapper _lblafters2 = null;
anywheresoftware.b4a.objects.LabelWrapper _lblriderpenalty = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridertotal = null;
 //BA.debugLineNum = 813;BA.debugLine="Sub DisplayDayLive(dayNum As Int)";
 //BA.debugLineNum = 814;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 815;BA.debugLine="Dim placedClassPanels As Int = 0";
_placedclasspanels = (int) (0);
 //BA.debugLineNum = 817;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 6 'SV";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)6);
 //BA.debugLineNum = 818;BA.debugLine="Dim classPanelHeight As Int = SVResults.Height /";
_classpanelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 819;BA.debugLine="Log(\"Panel Height:\"&panelHeight)";
anywheresoftware.b4a.keywords.Common.Log("Panel Height:"+BA.NumberToString(_panelheight));
 //BA.debugLineNum = 820;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 822;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 823;BA.debugLine="If dayNum = 1 Then";
if (_daynum==1) { 
 //BA.debugLineNum = 824;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSu";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrNationality, trrClass, trrTeam, trdlL1 As L1, trdlL2 As L2, trdlPosition As position, trdlTotal As total, trdlPenalty As penalty, trdlStunt1 As section1, trdlStunt2 As section2, trdlStunt3 As section3, trdlStunt4 As section4, trdlStunt5 As section5, trdlStunt6 As section6, trdlStunt7 As section7, trdlStunt8 As section8, trdlStunt9 As section9, trdlStunt10 As section10, trdlStunt11 As section11, trdlStunt12 As section12, trdlStunt13 As section13, trdlStunt14 As section14, trdlStunt15 As section15 FROM trRiders, trDayLive WHERE trRiders.trrCode = trDayLive.trdlCode ORDER BY trdlIndex ASC")));
 }else {
 //BA.debugLineNum = 826;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSu";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrNationality, trrClass, trrTeam, trdl2L1 As L1, trdl2L2 As L2, trdl2Position As position, trdl2Total As total, trdl2Penalty As penalty, trdl2Stunt1 As section1, trdl2Stunt2 As section2, trdl2Stunt3 As section3, trdl2Stunt4 As section4, trdl2Stunt5 As section5, trdl2Stunt6 As section6, trdl2Stunt7 As section7, trdl2Stunt8 As section8, trdl2Stunt9 As section9, trdl2Stunt10 As section10, trdl2Stunt11 As section11, trdl2Stunt12 As section12, trdl2Stunt13 As section13, trdl2Stunt14 As section14, trdl2Stunt15 As section15 FROM trRiders, trDay2Live WHERE trRiders.trrCode = trDay2Live.trdl2Code ORDER BY trdl2Index ASC")));
 };
 //BA.debugLineNum = 829;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 830;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 834;BA.debugLine="Dim currentClass As String = \"\"";
_currentclass = "";
 //BA.debugLineNum = 835;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 836;BA.debugLine="For i = 0 To n";
{
final int step18 = 1;
final int limit18 = _n;
for (_i = (int) (0) ; (step18 > 0 && _i <= limit18) || (step18 < 0 && _i >= limit18); _i = ((int)(0 + _i + step18)) ) {
 //BA.debugLineNum = 837;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 838;BA.debugLine="Dim lvNames As String = crs.GetString(\"section1\"";
_lvnames = _crs.GetString("section1")+_crs.GetString("section2")+_crs.GetString("section3")+_crs.GetString("section4")+_crs.GetString("section5")+_crs.GetString("section6")+_crs.GetString("section7")+_crs.GetString("section8")+_crs.GetString("section9")+_crs.GetString("section10")+_crs.GetString("section11")+_crs.GetString("section12")+_crs.GetString("section13")+_crs.GetString("section14")+_crs.GetString("section15");
 //BA.debugLineNum = 839;BA.debugLine="Dim lvNamesArray(-1) As String = Regex.split(\"##";
_lvnamesarray = anywheresoftware.b4a.keywords.Common.Regex.Split("##",_lvnames);
 //BA.debugLineNum = 842;BA.debugLine="If currentClass <> crs.GetString(\"trrClass\") The";
if ((_currentclass).equals(_crs.GetString("trrClass")) == false) { 
 //BA.debugLineNum = 843;BA.debugLine="Dim classPanel As Panel";
_classpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 844;BA.debugLine="classPanel.Initialize(\"classPanel\")";
_classpanel.Initialize(mostCurrent.activityBA,"classPanel");
 //BA.debugLineNum = 845;BA.debugLine="classPanel.Color = Colors.RGB(34, 180, 231)";
_classpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 846;BA.debugLine="pnlResults.AddView(classPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_classpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)+(_classpanelheight*_placedclasspanels)),mostCurrent._svresults.getWidth(),_classpanelheight);
 //BA.debugLineNum = 847;BA.debugLine="placedClassPanels = placedClassPanels + 1";
_placedclasspanels = (int) (_placedclasspanels+1);
 //BA.debugLineNum = 848;BA.debugLine="currentClass = crs.GetString(\"trrClass\")";
_currentclass = _crs.GetString("trrClass");
 //BA.debugLineNum = 851;BA.debugLine="Dim lblClassName As Label";
_lblclassname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 852;BA.debugLine="lblClassName.Initialize(\"lblClassName\")";
_lblclassname.Initialize(mostCurrent.activityBA,"lblClassName");
 //BA.debugLineNum = 853;BA.debugLine="lblClassName.Typeface = Typeface.LoadFromAssets";
_lblclassname.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 854;BA.debugLine="lblClassName.Text = currentClass";
_lblclassname.setText((Object)(_currentclass));
 //BA.debugLineNum = 855;BA.debugLine="lblClassName.TextSize = lblSupport.TextSize";
_lblclassname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 856;BA.debugLine="lblClassName.TextColor = Colors.RGB(255, 255, 2";
_lblclassname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 857;BA.debugLine="lblClassName.Gravity = Gravity.CENTER";
_lblclassname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 858;BA.debugLine="classPanel.AddView(lblClassName, 0, 0, classPan";
_classpanel.AddView((android.view.View)(_lblclassname.getObject()),(int) (0),(int) (0),_classpanel.getWidth(),_classpanel.getHeight());
 };
 //BA.debugLineNum = 862;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 863;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 864;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 865;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 867;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 870;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)+(_classpanelheight*_placedclasspanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 871;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 874;BA.debugLine="Dim lblPosition As Label";
_lblposition = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 875;BA.debugLine="lblPosition.Initialize(\"lblPosition\")";
_lblposition.Initialize(mostCurrent.activityBA,"lblPosition");
 //BA.debugLineNum = 876;BA.debugLine="lblPosition.Typeface = Typeface.LoadFromAssets(\"";
_lblposition.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 877;BA.debugLine="lblPosition.Text = crs.GetString(\"position\")";
_lblposition.setText((Object)(_crs.GetString("position")));
 //BA.debugLineNum = 878;BA.debugLine="lblPosition.TextSize = lblSupport.TextSize";
_lblposition.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 879;BA.debugLine="lblPosition.TextColor = Colors.RGB(34, 180, 231)";
_lblposition.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 880;BA.debugLine="lblPosition.Gravity = Gravity.LEFT";
_lblposition.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 881;BA.debugLine="lblPosition.Gravity = Gravity.CENTER_VERTICAL";
_lblposition.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 882;BA.debugLine="resultPanel.AddView(lblPosition, lblPos.Left, 1d";
_resultpanel.AddView((android.view.View)(_lblposition.getObject()),mostCurrent._lblpos.getLeft(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1)),mostCurrent._lblpos.getWidth(),(int) (_resultpanel.getHeight()/(double)3.2));
 //BA.debugLineNum = 885;BA.debugLine="Dim lblName As Label";
_lblname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 886;BA.debugLine="lblName.Initialize(\"lblName\")";
_lblname.Initialize(mostCurrent.activityBA,"lblName");
 //BA.debugLineNum = 887;BA.debugLine="lblName.Typeface = Typeface.LoadFromAssets(\"helv";
_lblname.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 888;BA.debugLine="Dim riderName As String = crs.GetString(\"trrSurn";
_ridername = _crs.GetString("trrSurname");
 //BA.debugLineNum = 889;BA.debugLine="If riderName.Length > 11 Then";
if (_ridername.length()>11) { 
 //BA.debugLineNum = 890;BA.debugLine="lblName.Text = riderName.SubString2(0, 11)";
_lblname.setText((Object)(_ridername.substring((int) (0),(int) (11))));
 }else {
 //BA.debugLineNum = 892;BA.debugLine="lblName.Text = riderName";
_lblname.setText((Object)(_ridername));
 };
 //BA.debugLineNum = 894;BA.debugLine="lblName.TextSize = lblSupport.TextSize - 1";
_lblname.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-1));
 //BA.debugLineNum = 895;BA.debugLine="lblName.TextColor = Colors.RGB(255, 255, 255)";
_lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 896;BA.debugLine="lblName.Gravity = Gravity.CENTER_VERTICAL";
_lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 897;BA.debugLine="resultPanel.AddView(lblName, lblRider.Left, 1dip";
_resultpanel.AddView((android.view.View)(_lblname.getObject()),mostCurrent._lblrider.getLeft(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1)),mostCurrent._lblrider.getWidth(),(int) (_resultpanel.getHeight()/(double)3.2));
 //BA.debugLineNum = 901;BA.debugLine="Dim lblFirstDriver As Label";
_lblfirstdriver = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 902;BA.debugLine="lblFirstDriver.Initialize(\"lblFirstDriver\")";
_lblfirstdriver.Initialize(mostCurrent.activityBA,"lblFirstDriver");
 //BA.debugLineNum = 903;BA.debugLine="lblFirstDriver.Typeface = Typeface.LoadFromAsset";
_lblfirstdriver.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 904;BA.debugLine="Dim firstDriverName As String = lvNamesArray(0)";
_firstdrivername = _lvnamesarray[(int) (0)];
 //BA.debugLineNum = 905;BA.debugLine="Log(\"Len1\"&firstDriverName.Length)";
anywheresoftware.b4a.keywords.Common.Log("Len1"+BA.NumberToString(_firstdrivername.length()));
 //BA.debugLineNum = 906;BA.debugLine="If firstDriverName.Length > 25 Then";
if (_firstdrivername.length()>25) { 
 //BA.debugLineNum = 907;BA.debugLine="lblFirstDriver.Text = firstDriverName.SubString";
_lblfirstdriver.setText((Object)(_firstdrivername.substring((int) (0),(int) (25))));
 }else {
 //BA.debugLineNum = 909;BA.debugLine="lblFirstDriver.Text = firstDriverName";
_lblfirstdriver.setText((Object)(_firstdrivername));
 };
 //BA.debugLineNum = 911;BA.debugLine="lblFirstDriver.TextSize = lblSupport.TextSize -";
_lblfirstdriver.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-5));
 //BA.debugLineNum = 912;BA.debugLine="lblFirstDriver.TextColor = Colors.RGB(255, 255,";
_lblfirstdriver.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 913;BA.debugLine="lblFirstDriver.Gravity = Gravity.CENTER_VERTICAL";
_lblfirstdriver.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 914;BA.debugLine="resultPanel.AddView(lblFirstDriver, lblRider.Lef";
_resultpanel.AddView((android.view.View)(_lblfirstdriver.getObject()),mostCurrent._lblrider.getLeft(),(int) (_lblname.getTop()+_lblname.getHeight()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1))),mostCurrent._lblrider.getWidth(),(int) (_resultpanel.getHeight()/(double)5));
 //BA.debugLineNum = 917;BA.debugLine="Dim lblSecondDriver As Label";
_lblseconddriver = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 918;BA.debugLine="lblSecondDriver.Initialize(\"lblSecondDriver\")";
_lblseconddriver.Initialize(mostCurrent.activityBA,"lblSecondDriver");
 //BA.debugLineNum = 919;BA.debugLine="lblSecondDriver.Typeface = Typeface.LoadFromAsse";
_lblseconddriver.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 920;BA.debugLine="Dim secondDriverName As String = lvNamesArray(1)";
_seconddrivername = _lvnamesarray[(int) (1)];
 //BA.debugLineNum = 921;BA.debugLine="Log(\"Len2\"&secondDriverName.Length)";
anywheresoftware.b4a.keywords.Common.Log("Len2"+BA.NumberToString(_seconddrivername.length()));
 //BA.debugLineNum = 922;BA.debugLine="If secondDriverName.Length > 25 Then";
if (_seconddrivername.length()>25) { 
 //BA.debugLineNum = 923;BA.debugLine="lblSecondDriver.Text = secondDriverName.SubStri";
_lblseconddriver.setText((Object)(_seconddrivername.substring((int) (0),(int) (25))));
 }else {
 //BA.debugLineNum = 925;BA.debugLine="lblSecondDriver.Text = secondDriverName";
_lblseconddriver.setText((Object)(_seconddrivername));
 };
 //BA.debugLineNum = 927;BA.debugLine="lblSecondDriver.TextSize = lblSupport.TextSize -";
_lblseconddriver.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-5));
 //BA.debugLineNum = 928;BA.debugLine="lblSecondDriver.TextColor = Colors.RGB(255, 255,";
_lblseconddriver.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 929;BA.debugLine="lblSecondDriver.Gravity = Gravity.CENTER_VERTICA";
_lblseconddriver.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 930;BA.debugLine="resultPanel.AddView(lblSecondDriver, lblRider.Le";
_resultpanel.AddView((android.view.View)(_lblseconddriver.getObject()),mostCurrent._lblrider.getLeft(),(int) (_lblfirstdriver.getTop()+_lblfirstdriver.getHeight()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1))),mostCurrent._lblrider.getWidth(),(int) (_resultpanel.getHeight()/(double)5));
 //BA.debugLineNum = 933;BA.debugLine="Dim lblThirdDriver As Label";
_lblthirddriver = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 934;BA.debugLine="lblThirdDriver.Initialize(\"lblThirdDriver\")";
_lblthirddriver.Initialize(mostCurrent.activityBA,"lblThirdDriver");
 //BA.debugLineNum = 935;BA.debugLine="lblThirdDriver.Typeface = Typeface.LoadFromAsset";
_lblthirddriver.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 936;BA.debugLine="Log(\"Length\"&lvNamesArray.Length)";
anywheresoftware.b4a.keywords.Common.Log("Length"+BA.NumberToString(_lvnamesarray.length));
 //BA.debugLineNum = 937;BA.debugLine="Dim thirdDriverName As String = lvNamesArray(2)";
_thirddrivername = _lvnamesarray[(int) (2)];
 //BA.debugLineNum = 938;BA.debugLine="Log(\"Len3\"&thirdDriverName.Length)";
anywheresoftware.b4a.keywords.Common.Log("Len3"+BA.NumberToString(_thirddrivername.length()));
 //BA.debugLineNum = 939;BA.debugLine="If thirdDriverName.Length > 25 Then";
if (_thirddrivername.length()>25) { 
 //BA.debugLineNum = 940;BA.debugLine="lblThirdDriver.Text = thirdDriverName.SubString";
_lblthirddriver.setText((Object)(_thirddrivername.substring((int) (0),(int) (25))));
 }else {
 //BA.debugLineNum = 942;BA.debugLine="lblThirdDriver.Text = thirdDriverName";
_lblthirddriver.setText((Object)(_thirddrivername));
 };
 //BA.debugLineNum = 944;BA.debugLine="lblThirdDriver.TextSize = lblSupport.TextSize -";
_lblthirddriver.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-5));
 //BA.debugLineNum = 945;BA.debugLine="lblThirdDriver.TextColor = Colors.RGB(255, 255,";
_lblthirddriver.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 946;BA.debugLine="lblThirdDriver.Gravity = Gravity.CENTER_VERTICAL";
_lblthirddriver.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 947;BA.debugLine="resultPanel.AddView(lblThirdDriver, lblRider.Lef";
_resultpanel.AddView((android.view.View)(_lblthirddriver.getObject()),mostCurrent._lblrider.getLeft(),(int) (_lblseconddriver.getTop()+_lblseconddriver.getHeight()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1))),mostCurrent._lblrider.getWidth(),(int) (_resultpanel.getHeight()/(double)5));
 //BA.debugLineNum = 950;BA.debugLine="Dim lblLap1 As Label";
_lbllap1 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 951;BA.debugLine="lblLap1.Initialize(\"lblLap1\")";
_lbllap1.Initialize(mostCurrent.activityBA,"lblLap1");
 //BA.debugLineNum = 952;BA.debugLine="lblLap1.Typeface = Typeface.LoadFromAssets(\"helv";
_lbllap1.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 953;BA.debugLine="lblLap1.Text = \"LAP 1\"";
_lbllap1.setText((Object)("LAP 1"));
 //BA.debugLineNum = 954;BA.debugLine="lblLap1.TextSize = lblSupport.TextSize - 1";
_lbllap1.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-1));
 //BA.debugLineNum = 955;BA.debugLine="lblLap1.TextColor =  Colors.RGB(255, 255, 255)";
_lbllap1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 956;BA.debugLine="lblLap1.Gravity = Gravity.CENTER";
_lbllap1.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 957;BA.debugLine="resultPanel.AddView(lblLap1, lblClass.Left, 1dip";
_resultpanel.AddView((android.view.View)(_lbllap1.getObject()),mostCurrent._lblclass.getLeft(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1)),(int) (mostCurrent._lblclass.getWidth()/(double)2),(int) (_resultpanel.getHeight()/(double)3.2));
 //BA.debugLineNum = 960;BA.debugLine="Dim lblLap2 As Label";
_lbllap2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 961;BA.debugLine="lblLap2.Initialize(\"lblLap2\")";
_lbllap2.Initialize(mostCurrent.activityBA,"lblLap2");
 //BA.debugLineNum = 962;BA.debugLine="lblLap2.Typeface = Typeface.LoadFromAssets(\"helv";
_lbllap2.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 963;BA.debugLine="lblLap2.Text = \"LAP 2\"";
_lbllap2.setText((Object)("LAP 2"));
 //BA.debugLineNum = 964;BA.debugLine="lblLap2.TextSize = lblSupport.TextSize - 1";
_lbllap2.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-1));
 //BA.debugLineNum = 965;BA.debugLine="lblLap2.TextColor =  Colors.RGB(255, 255, 255)";
_lbllap2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 966;BA.debugLine="lblLap2.Gravity = Gravity.CENTER";
_lbllap2.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 967;BA.debugLine="resultPanel.AddView(lblLap2, lblClass.Left+lblCl";
_resultpanel.AddView((android.view.View)(_lbllap2.getObject()),(int) (mostCurrent._lblclass.getLeft()+mostCurrent._lblclass.getWidth()/(double)2),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1)),(int) (mostCurrent._lblclass.getWidth()/(double)2),(int) (_resultpanel.getHeight()/(double)3.2));
 //BA.debugLineNum = 970;BA.debugLine="Dim classPanelInside As Panel";
_classpanelinside = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 971;BA.debugLine="classPanelInside.Initialize(\"classPanelInside\")";
_classpanelinside.Initialize(mostCurrent.activityBA,"classPanelInside");
 //BA.debugLineNum = 972;BA.debugLine="classPanelInside.Color = Colors.RGB(142, 142, 14";
_classpanelinside.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (142),(int) (142),(int) (142)));
 //BA.debugLineNum = 975;BA.debugLine="resultPanel.AddView(classPanelInside, lblLap1.Le";
_resultpanel.AddView((android.view.View)(_classpanelinside.getObject()),_lbllap1.getLeft(),(int) (_lblname.getTop()+_lblname.getHeight()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1))),(int) (_lbllap1.getWidth()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (3))),(int) (_resultpanel.getHeight()/(double)2.5));
 //BA.debugLineNum = 977;BA.debugLine="Dim classPanelInside2 As Panel";
_classpanelinside2 = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 978;BA.debugLine="classPanelInside2.Initialize(\"classPanelInside2\"";
_classpanelinside2.Initialize(mostCurrent.activityBA,"classPanelInside2");
 //BA.debugLineNum = 979;BA.debugLine="classPanelInside2.Color = Colors.RGB(142, 142, 1";
_classpanelinside2.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (142),(int) (142),(int) (142)));
 //BA.debugLineNum = 982;BA.debugLine="resultPanel.AddView(classPanelInside2, lblLap2.L";
_resultpanel.AddView((android.view.View)(_classpanelinside2.getObject()),_lbllap2.getLeft(),(int) (_lblname.getTop()+_lblname.getHeight()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1))),(int) (_lbllap2.getWidth()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (3))),(int) (_resultpanel.getHeight()/(double)2.5));
 //BA.debugLineNum = 985;BA.debugLine="Dim lblLap1Results As Label";
_lbllap1results = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 986;BA.debugLine="lblLap1Results.Initialize(\"lblLap1Results\")";
_lbllap1results.Initialize(mostCurrent.activityBA,"lblLap1Results");
 //BA.debugLineNum = 987;BA.debugLine="lblLap1Results.Typeface = Typeface.LoadFromAsset";
_lbllap1results.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 988;BA.debugLine="lblLap1Results.Text = crs.GetString(\"L1\")";
_lbllap1results.setText((Object)(_crs.GetString("L1")));
 //BA.debugLineNum = 989;BA.debugLine="lblLap1Results.TextSize = lblSupport.TextSize+5";
_lbllap1results.setTextSize((float) (mostCurrent._lblsupport.getTextSize()+5));
 //BA.debugLineNum = 990;BA.debugLine="lblLap1Results.TextColor =  Colors.RGB(0, 0, 0)";
_lbllap1results.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 //BA.debugLineNum = 991;BA.debugLine="lblLap1Results.Gravity = Gravity.CENTER";
_lbllap1results.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 992;BA.debugLine="resultPanel.AddView(lblLap1Results, classPanelIn";
_resultpanel.AddView((android.view.View)(_lbllap1results.getObject()),_classpanelinside.getLeft(),(int) (_classpanelinside.getTop()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))),_classpanelinside.getWidth(),(int) (_classpanelinside.getHeight()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 995;BA.debugLine="Dim lblLap2Results As Label";
_lbllap2results = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 996;BA.debugLine="lblLap2Results.Initialize(\"lblLap2Results\")";
_lbllap2results.Initialize(mostCurrent.activityBA,"lblLap2Results");
 //BA.debugLineNum = 997;BA.debugLine="lblLap2Results.Typeface = Typeface.LoadFromAsset";
_lbllap2results.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 998;BA.debugLine="lblLap2Results.Text = crs.GetString(\"L2\")";
_lbllap2results.setText((Object)(_crs.GetString("L2")));
 //BA.debugLineNum = 999;BA.debugLine="lblLap2Results.TextSize = lblSupport.TextSize +5";
_lbllap2results.setTextSize((float) (mostCurrent._lblsupport.getTextSize()+5));
 //BA.debugLineNum = 1000;BA.debugLine="lblLap2Results.TextColor =  Colors.RGB(0, 0, 0)";
_lbllap2results.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 //BA.debugLineNum = 1001;BA.debugLine="lblLap2Results.Gravity = Gravity.CENTER";
_lbllap2results.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 1002;BA.debugLine="resultPanel.AddView(lblLap2Results, classPanelIn";
_resultpanel.AddView((android.view.View)(_lbllap2results.getObject()),_classpanelinside2.getLeft(),(int) (_classpanelinside2.getTop()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))),_classpanelinside2.getWidth(),(int) (_classpanelinside.getHeight()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 1005;BA.debugLine="Dim lblAfter1 As Label";
_lblafter1 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1006;BA.debugLine="lblAfter1.Initialize(\"lblAfter1\")";
_lblafter1.Initialize(mostCurrent.activityBA,"lblAfter1");
 //BA.debugLineNum = 1007;BA.debugLine="lblAfter1.Typeface = Typeface.LoadFromAssets(\"he";
_lblafter1.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 1008;BA.debugLine="lblAfter1.Text = \"AFTER\"";
_lblafter1.setText((Object)("AFTER"));
 //BA.debugLineNum = 1009;BA.debugLine="lblAfter1.TextSize = lblSupport.TextSize - 6";
_lblafter1.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-6));
 //BA.debugLineNum = 1010;BA.debugLine="lblAfter1.TextColor =  Colors.RGB(255, 255, 255)";
_lblafter1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1011;BA.debugLine="lblAfter1.Gravity = Gravity.CENTER_VERTICAL";
_lblafter1.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1012;BA.debugLine="lblAfter1.Gravity = Gravity.LEFT";
_lblafter1.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 1013;BA.debugLine="resultPanel.AddView(lblAfter1, lblLap1.left, cla";
_resultpanel.AddView((android.view.View)(_lblafter1.getObject()),_lbllap1.getLeft(),(int) (_classpanelinside.getTop()+_classpanelinside.getHeight()),_lbllap1.getWidth(),(int) (_resultpanel.getHeight()/(double)5));
 //BA.debugLineNum = 1016;BA.debugLine="Dim lblAfter2 As Label";
_lblafter2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1017;BA.debugLine="lblAfter2.Initialize(\"lblAfter2\")";
_lblafter2.Initialize(mostCurrent.activityBA,"lblAfter2");
 //BA.debugLineNum = 1018;BA.debugLine="lblAfter2.Typeface = Typeface.LoadFromAssets(\"he";
_lblafter2.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 1019;BA.debugLine="lblAfter2.Text = \"AFTER\"";
_lblafter2.setText((Object)("AFTER"));
 //BA.debugLineNum = 1020;BA.debugLine="lblAfter2.TextSize = lblSupport.TextSize - 6";
_lblafter2.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-6));
 //BA.debugLineNum = 1021;BA.debugLine="lblAfter2.TextColor =  Colors.RGB(255, 255, 255)";
_lblafter2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1022;BA.debugLine="lblAfter2.Gravity = Gravity.CENTER_VERTICAL";
_lblafter2.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1023;BA.debugLine="lblAfter2.Gravity = Gravity.LEFT";
_lblafter2.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 1024;BA.debugLine="resultPanel.AddView(lblAfter2,  lblLap2.left, cl";
_resultpanel.AddView((android.view.View)(_lblafter2.getObject()),_lbllap2.getLeft(),(int) (_classpanelinside2.getTop()+_classpanelinside2.getHeight()),_lbllap2.getWidth(),(int) (_resultpanel.getHeight()/(double)5));
 //BA.debugLineNum = 1027;BA.debugLine="Dim lblAfterS1 As Label";
_lblafters1 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1028;BA.debugLine="lblAfterS1.Initialize(\"lblAfterS1\")";
_lblafters1.Initialize(mostCurrent.activityBA,"lblAfterS1");
 //BA.debugLineNum = 1029;BA.debugLine="lblAfterS1.Typeface = Typeface.LoadFromAssets(\"h";
_lblafters1.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 1030;BA.debugLine="lblAfterS1.Text = crs.GetString(\"section14\")";
_lblafters1.setText((Object)(_crs.GetString("section14")));
 //BA.debugLineNum = 1031;BA.debugLine="lblAfterS1.TextSize = lblSupport.TextSize - 6";
_lblafters1.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-6));
 //BA.debugLineNum = 1032;BA.debugLine="lblAfterS1.TextColor = Colors.RGB(34, 180, 231)";
_lblafters1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1033;BA.debugLine="lblAfterS1.Gravity = Gravity.CENTER_VERTICAL";
_lblafters1.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1034;BA.debugLine="lblAfterS1.Gravity = Gravity.RIGHT";
_lblafters1.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.RIGHT);
 //BA.debugLineNum = 1036;BA.debugLine="resultPanel.AddView(lblAfterS1, lblLap1.left-3di";
_resultpanel.AddView((android.view.View)(_lblafters1.getObject()),(int) (_lbllap1.getLeft()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (3))),(int) (_classpanelinside.getTop()+_classpanelinside.getHeight()),_lbllap1.getWidth(),(int) (_resultpanel.getHeight()/(double)5));
 //BA.debugLineNum = 1039;BA.debugLine="Dim lblAfterS2 As Label";
_lblafters2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1040;BA.debugLine="lblAfterS2.Initialize(\"lblAfterS2\")";
_lblafters2.Initialize(mostCurrent.activityBA,"lblAfterS2");
 //BA.debugLineNum = 1041;BA.debugLine="lblAfterS2.Typeface = Typeface.LoadFromAssets(\"h";
_lblafters2.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 1042;BA.debugLine="lblAfterS2.Text = crs.GetString(\"section15\")";
_lblafters2.setText((Object)(_crs.GetString("section15")));
 //BA.debugLineNum = 1043;BA.debugLine="lblAfterS2.TextSize = lblSupport.TextSize - 6";
_lblafters2.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-6));
 //BA.debugLineNum = 1044;BA.debugLine="lblAfterS2.TextColor =  Colors.RGB(34, 180, 231)";
_lblafters2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1045;BA.debugLine="lblAfterS2.Gravity = Gravity.CENTER_VERTICAL";
_lblafters2.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1046;BA.debugLine="lblAfterS2.Gravity = Gravity.RIGHT";
_lblafters2.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.RIGHT);
 //BA.debugLineNum = 1048;BA.debugLine="resultPanel.AddView(lblAfterS2,  lblLap2.left-3d";
_resultpanel.AddView((android.view.View)(_lblafters2.getObject()),(int) (_lbllap2.getLeft()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (3))),(int) (_classpanelinside2.getTop()+_classpanelinside2.getHeight()),_lbllap2.getWidth(),(int) (_resultpanel.getHeight()/(double)5));
 //BA.debugLineNum = 1051;BA.debugLine="Dim lblRiderPenalty As Label";
_lblriderpenalty = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1052;BA.debugLine="lblRiderPenalty.Initialize(\"lblTime\")";
_lblriderpenalty.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1053;BA.debugLine="lblRiderPenalty.Text = crs.GetString(\"penalty\")";
_lblriderpenalty.setText((Object)(_crs.GetString("penalty")));
 //BA.debugLineNum = 1054;BA.debugLine="lblRiderPenalty.TextSize = lblSupport.TextSize -";
_lblriderpenalty.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-1));
 //BA.debugLineNum = 1055;BA.debugLine="lblRiderPenalty.TextColor = Colors.RGB(255, 255,";
_lblriderpenalty.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1056;BA.debugLine="lblRiderPenalty.Gravity = Gravity.CENTER";
_lblriderpenalty.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 1059;BA.debugLine="resultPanel.AddView(lblRiderPenalty, lblTime.Lef";
_resultpanel.AddView((android.view.View)(_lblriderpenalty.getObject()),mostCurrent._lbltime.getLeft(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1)),mostCurrent._lbltime.getWidth(),(int) (_resultpanel.getHeight()/(double)3.2));
 //BA.debugLineNum = 1062;BA.debugLine="Dim lblRiderTotal As Label";
_lblridertotal = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1063;BA.debugLine="lblRiderTotal.Initialize(\"lblTime\")";
_lblridertotal.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1064;BA.debugLine="lblRiderTotal.Text = crs.GetString(\"total\")";
_lblridertotal.setText((Object)(_crs.GetString("total")));
 //BA.debugLineNum = 1065;BA.debugLine="lblRiderTotal.TextSize = lblSupport.TextSize-1";
_lblridertotal.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-1));
 //BA.debugLineNum = 1066;BA.debugLine="lblRiderTotal.TextColor = Colors.RGB(34, 180, 23";
_lblridertotal.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1067;BA.debugLine="lblRiderTotal.Gravity = Gravity.CENTER";
_lblridertotal.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 1069;BA.debugLine="resultPanel.AddView(lblRiderTotal, lblTotal.Left";
_resultpanel.AddView((android.view.View)(_lblridertotal.getObject()),mostCurrent._lbltotal.getLeft(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1)),mostCurrent._lbltotal.getWidth(),(int) (_resultpanel.getHeight()/(double)3.2));
 }
};
 //BA.debugLineNum = 1071;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)+(_placedclasspanels*_classpanelheight)));
 //BA.debugLineNum = 1073;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1074;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1075;BA.debugLine="End Sub";
return "";
}
public static String  _displaydayonestart() throws Exception{
int _placedpanels = 0;
int _panelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblindex = null;
anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
String _ridername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblriderclass = null;
String[] _str = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridertime = null;
 //BA.debugLineNum = 1077;BA.debugLine="Sub DisplayDayOneStart";
 //BA.debugLineNum = 1078;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 1079;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 14";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 1081;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 1083;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 1084;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSur";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trdsTime FROM trRiders, trDayStart WHERE trRiders.trrCode = trDayStart.trdsCode ORDER BY trdsPosition ASC")));
 //BA.debugLineNum = 1086;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 1087;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 1091;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 1092;BA.debugLine="For i = 0 To n";
{
final int step10 = 1;
final int limit10 = _n;
for (_i = (int) (0) ; (step10 > 0 && _i <= limit10) || (step10 < 0 && _i >= limit10); _i = ((int)(0 + _i + step10)) ) {
 //BA.debugLineNum = 1093;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 1096;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1097;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 1098;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 1099;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 1101;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 1104;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1105;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1108;BA.debugLine="Dim lblIndex As Label";
_lblindex = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1109;BA.debugLine="lblIndex.Initialize(\"lblIndex\")";
_lblindex.Initialize(mostCurrent.activityBA,"lblIndex");
 //BA.debugLineNum = 1110;BA.debugLine="lblIndex.Text = i + 1";
_lblindex.setText((Object)(_i+1));
 //BA.debugLineNum = 1111;BA.debugLine="lblIndex.TextSize = lblSupport.TextSize";
_lblindex.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1112;BA.debugLine="lblIndex.TextColor = Colors.RGB(150, 150, 150)";
_lblindex.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1113;BA.debugLine="lblIndex.Gravity = Gravity.CENTER_VERTICAL";
_lblindex.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1114;BA.debugLine="resultPanel.AddView(lblIndex, 5dip, 0, resultPan";
_resultpanel.AddView((android.view.View)(_lblindex.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),(int) (0),(int) (_resultpanel.getWidth()/(double)10),_resultpanel.getHeight());
 //BA.debugLineNum = 1126;BA.debugLine="Dim lblName As Label";
_lblname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1127;BA.debugLine="lblName.Initialize(\"lblName\")";
_lblname.Initialize(mostCurrent.activityBA,"lblName");
 //BA.debugLineNum = 1128;BA.debugLine="Dim riderName As String = crs.GetString(\"trrSurn";
_ridername = _crs.GetString("trrSurname");
 //BA.debugLineNum = 1129;BA.debugLine="If riderName.Length > 12 Then";
if (_ridername.length()>12) { 
 //BA.debugLineNum = 1130;BA.debugLine="lblName.Text = riderName.SubString2(0, 12)";
_lblname.setText((Object)(_ridername.substring((int) (0),(int) (12))));
 }else {
 //BA.debugLineNum = 1132;BA.debugLine="lblName.Text = riderName";
_lblname.setText((Object)(_ridername));
 };
 //BA.debugLineNum = 1134;BA.debugLine="lblName.TextSize = lblSupport.TextSize";
_lblname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1135;BA.debugLine="lblName.TextColor = Colors.RGB(255, 255, 255)";
_lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1136;BA.debugLine="lblName.Gravity = Gravity.CENTER_VERTICAL";
_lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1137;BA.debugLine="resultPanel.AddView(lblName, lblRider.Left, 0, l";
_resultpanel.AddView((android.view.View)(_lblname.getObject()),mostCurrent._lblrider.getLeft(),(int) (0),mostCurrent._lblrider.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1140;BA.debugLine="Dim lblRiderClass As Label";
_lblriderclass = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1141;BA.debugLine="lblRiderClass.Initialize(\"lblClass\")";
_lblriderclass.Initialize(mostCurrent.activityBA,"lblClass");
 //BA.debugLineNum = 1142;BA.debugLine="Dim str(2) As String";
_str = new String[(int) (2)];
java.util.Arrays.fill(_str,"");
 //BA.debugLineNum = 1143;BA.debugLine="str = Regex.split(\" \", crs.GetString(\"trrClass\")";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split(" ",_crs.GetString("trrClass"));
 //BA.debugLineNum = 1144;BA.debugLine="lblRiderClass.Text = str(0)";
_lblriderclass.setText((Object)(_str[(int) (0)]));
 //BA.debugLineNum = 1145;BA.debugLine="lblRiderClass.TextSize = lblSupport.TextSize";
_lblriderclass.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1146;BA.debugLine="lblRiderClass.TextColor = Colors.RGB(150, 150, 1";
_lblriderclass.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1147;BA.debugLine="lblRiderClass.Gravity = Gravity.CENTER_VERTICAL";
_lblriderclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1148;BA.debugLine="resultPanel.AddView(lblRiderClass, lblClass.Left";
_resultpanel.AddView((android.view.View)(_lblriderclass.getObject()),mostCurrent._lblclass.getLeft(),(int) (0),(int) (mostCurrent._lblclass.getLeft()-mostCurrent._lbltime.getLeft()),_resultpanel.getHeight());
 //BA.debugLineNum = 1151;BA.debugLine="Dim lblRiderTime As Label";
_lblridertime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1152;BA.debugLine="lblRiderTime.Initialize(\"lblTime\")";
_lblridertime.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1153;BA.debugLine="lblRiderTime.Text = crs.GetString(\"trdsTime\")";
_lblridertime.setText((Object)(_crs.GetString("trdsTime")));
 //BA.debugLineNum = 1154;BA.debugLine="lblRiderTime.TextSize = lblSupport.TextSize";
_lblridertime.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1155;BA.debugLine="lblRiderTime.TextColor = Colors.RGB(34, 180, 231";
_lblridertime.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1156;BA.debugLine="lblRiderTime.Gravity = Gravity.CENTER_VERTICAL";
_lblridertime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1157;BA.debugLine="resultPanel.AddView(lblRiderTime, lblTime.Left,";
_resultpanel.AddView((android.view.View)(_lblridertime.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 1159;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)));
 //BA.debugLineNum = 1161;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1162;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1163;BA.debugLine="End Sub";
return "";
}
public static String  _displaydaytwostart() throws Exception{
int _placedpanels = 0;
int _panelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblindex = null;
anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
String _ridername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblriderclass = null;
String[] _str = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridertime = null;
 //BA.debugLineNum = 1165;BA.debugLine="Sub DisplayDayTwoStart";
 //BA.debugLineNum = 1166;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 1167;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 14";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 1169;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 1171;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 1172;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSur";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trds2Time FROM trRiders, trDay2Start WHERE trRiders.trrCode = trDay2Start.trds2Code ORDER BY trds2Position ASC")));
 //BA.debugLineNum = 1174;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 1175;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 1179;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 1180;BA.debugLine="For i = 0 To n";
{
final int step10 = 1;
final int limit10 = _n;
for (_i = (int) (0) ; (step10 > 0 && _i <= limit10) || (step10 < 0 && _i >= limit10); _i = ((int)(0 + _i + step10)) ) {
 //BA.debugLineNum = 1181;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 1184;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1185;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 1186;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 1187;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 1189;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 1192;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1193;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1196;BA.debugLine="Dim lblIndex As Label";
_lblindex = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1197;BA.debugLine="lblIndex.Initialize(\"lblIndex\")";
_lblindex.Initialize(mostCurrent.activityBA,"lblIndex");
 //BA.debugLineNum = 1198;BA.debugLine="lblIndex.Text = i + 1";
_lblindex.setText((Object)(_i+1));
 //BA.debugLineNum = 1199;BA.debugLine="lblIndex.TextSize = lblSupport.TextSize";
_lblindex.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1200;BA.debugLine="lblIndex.TextColor = Colors.RGB(150, 150, 150)";
_lblindex.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1201;BA.debugLine="lblIndex.Gravity = Gravity.CENTER_VERTICAL";
_lblindex.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1202;BA.debugLine="resultPanel.AddView(lblIndex, 5dip, 0, resultPan";
_resultpanel.AddView((android.view.View)(_lblindex.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),(int) (0),(int) (_resultpanel.getWidth()/(double)10),_resultpanel.getHeight());
 //BA.debugLineNum = 1208;BA.debugLine="Dim lblName As Label";
_lblname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1209;BA.debugLine="lblName.Initialize(\"lblName\")";
_lblname.Initialize(mostCurrent.activityBA,"lblName");
 //BA.debugLineNum = 1210;BA.debugLine="Dim riderName As String = crs.GetString(\"trrSurn";
_ridername = _crs.GetString("trrSurname");
 //BA.debugLineNum = 1211;BA.debugLine="If riderName.Length > 12 Then";
if (_ridername.length()>12) { 
 //BA.debugLineNum = 1212;BA.debugLine="lblName.Text = riderName.SubString2(0, 12)";
_lblname.setText((Object)(_ridername.substring((int) (0),(int) (12))));
 }else {
 //BA.debugLineNum = 1214;BA.debugLine="lblName.Text = riderName";
_lblname.setText((Object)(_ridername));
 };
 //BA.debugLineNum = 1216;BA.debugLine="lblName.TextSize = lblSupport.TextSize";
_lblname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1217;BA.debugLine="lblName.TextColor = Colors.RGB(255, 255, 255)";
_lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1218;BA.debugLine="lblName.Gravity = Gravity.CENTER_VERTICAL";
_lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1219;BA.debugLine="resultPanel.AddView(lblName, lblRider.Left, 0, l";
_resultpanel.AddView((android.view.View)(_lblname.getObject()),mostCurrent._lblrider.getLeft(),(int) (0),mostCurrent._lblrider.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1222;BA.debugLine="Dim lblRiderClass As Label";
_lblriderclass = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1223;BA.debugLine="lblRiderClass.Initialize(\"lblClass\")";
_lblriderclass.Initialize(mostCurrent.activityBA,"lblClass");
 //BA.debugLineNum = 1224;BA.debugLine="Dim str(2) As String";
_str = new String[(int) (2)];
java.util.Arrays.fill(_str,"");
 //BA.debugLineNum = 1225;BA.debugLine="str = Regex.split(\" \", crs.GetString(\"trrClass\")";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split(" ",_crs.GetString("trrClass"));
 //BA.debugLineNum = 1226;BA.debugLine="lblRiderClass.Text = str(0)";
_lblriderclass.setText((Object)(_str[(int) (0)]));
 //BA.debugLineNum = 1227;BA.debugLine="lblRiderClass.TextSize = lblSupport.TextSize";
_lblriderclass.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1228;BA.debugLine="lblRiderClass.TextColor = Colors.RGB(150, 150, 1";
_lblriderclass.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1229;BA.debugLine="lblRiderClass.Gravity = Gravity.CENTER_VERTICAL";
_lblriderclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1230;BA.debugLine="resultPanel.AddView(lblRiderClass, lblClass.Left";
_resultpanel.AddView((android.view.View)(_lblriderclass.getObject()),mostCurrent._lblclass.getLeft(),(int) (0),(int) (mostCurrent._lblclass.getLeft()-mostCurrent._lbltime.getLeft()),_resultpanel.getHeight());
 //BA.debugLineNum = 1233;BA.debugLine="Dim lblRiderTime As Label";
_lblridertime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1234;BA.debugLine="lblRiderTime.Initialize(\"lblTime\")";
_lblridertime.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1235;BA.debugLine="lblRiderTime.Text = crs.GetString(\"trds2Time\")";
_lblridertime.setText((Object)(_crs.GetString("trds2Time")));
 //BA.debugLineNum = 1236;BA.debugLine="lblRiderTime.TextSize = lblSupport.TextSize";
_lblridertime.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1237;BA.debugLine="lblRiderTime.TextColor = Colors.RGB(34, 180, 231";
_lblridertime.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1238;BA.debugLine="lblRiderTime.Gravity = Gravity.CENTER_VERTICAL";
_lblridertime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1239;BA.debugLine="resultPanel.AddView(lblRiderTime, lblTime.Left,";
_resultpanel.AddView((android.view.View)(_lblridertime.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 1241;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)));
 //BA.debugLineNum = 1243;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1244;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1245;BA.debugLine="End Sub";
return "";
}
public static String  _displayqualificationlive() throws Exception{
int _placedpanels = 0;
int _panelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
String _currentclass = "";
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _classpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblclassname = null;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblposition = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridername = null;
anywheresoftware.b4a.objects.LabelWrapper _lblscore = null;
anywheresoftware.b4a.objects.LabelWrapper _lblresulttime = null;
 //BA.debugLineNum = 1247;BA.debugLine="Sub DisplayQualificationLive";
 //BA.debugLineNum = 1248;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 1249;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 14";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 1251;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 1253;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 1254;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trqlPosition, t";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trqlPosition, trqlIndex, trrCode, trrSurname, trrClass, trqlScore, trqlTime FROM trRiders, trQuLive WHERE trRiders.trrCode = trQuLive.trqlCode ORDER BY trqlPosition ASC")));
 //BA.debugLineNum = 1256;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 1257;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 1261;BA.debugLine="Dim currentClass As String = \"\"";
_currentclass = "";
 //BA.debugLineNum = 1262;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 1263;BA.debugLine="For i = 0 To n";
{
final int step11 = 1;
final int limit11 = _n;
for (_i = (int) (0) ; (step11 > 0 && _i <= limit11) || (step11 < 0 && _i >= limit11); _i = ((int)(0 + _i + step11)) ) {
 //BA.debugLineNum = 1264;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 1267;BA.debugLine="If currentClass <> crs.GetString(\"trrClass\") The";
if ((_currentclass).equals(_crs.GetString("trrClass")) == false) { 
 //BA.debugLineNum = 1268;BA.debugLine="Dim classPanel As Panel";
_classpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1269;BA.debugLine="classPanel.Initialize(\"classPanel\")";
_classpanel.Initialize(mostCurrent.activityBA,"classPanel");
 //BA.debugLineNum = 1270;BA.debugLine="classPanel.Color = Colors.RGB(34, 180, 231)";
_classpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1271;BA.debugLine="pnlResults.AddView(classPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_classpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1272;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1273;BA.debugLine="currentClass = crs.GetString(\"trrClass\")";
_currentclass = _crs.GetString("trrClass");
 //BA.debugLineNum = 1276;BA.debugLine="Dim lblClassName As Label";
_lblclassname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1277;BA.debugLine="lblClassName.Initialize(\"lblClassName\")";
_lblclassname.Initialize(mostCurrent.activityBA,"lblClassName");
 //BA.debugLineNum = 1278;BA.debugLine="lblClassName.Text = currentClass";
_lblclassname.setText((Object)(_currentclass));
 //BA.debugLineNum = 1279;BA.debugLine="lblClassName.TextSize = lblSupport.TextSize";
_lblclassname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1280;BA.debugLine="lblClassName.TextColor = Colors.RGB(255, 255, 2";
_lblclassname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1281;BA.debugLine="lblClassName.Gravity = Gravity.CENTER";
_lblclassname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 1282;BA.debugLine="classPanel.AddView(lblClassName, 0, 0, classPan";
_classpanel.AddView((android.view.View)(_lblclassname.getObject()),(int) (0),(int) (0),_classpanel.getWidth(),_classpanel.getHeight());
 };
 //BA.debugLineNum = 1286;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1287;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 1288;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 1289;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 1291;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 1294;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1295;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1298;BA.debugLine="Dim lblPosition As Label";
_lblposition = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1299;BA.debugLine="lblPosition.Initialize(\"lblPosition\")";
_lblposition.Initialize(mostCurrent.activityBA,"lblPosition");
 //BA.debugLineNum = 1300;BA.debugLine="lblPosition.Text = crs.GetString(\"trqlIndex\")";
_lblposition.setText((Object)(_crs.GetString("trqlIndex")));
 //BA.debugLineNum = 1301;BA.debugLine="lblPosition.TextSize = lblSupport.TextSize";
_lblposition.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1302;BA.debugLine="lblPosition.TextColor = Colors.RGB(34, 180, 231)";
_lblposition.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1303;BA.debugLine="lblPosition.Gravity = Gravity.CENTER_VERTICAL";
_lblposition.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1304;BA.debugLine="resultPanel.AddView(lblPosition, lblPos.Left, 0,";
_resultpanel.AddView((android.view.View)(_lblposition.getObject()),mostCurrent._lblpos.getLeft(),(int) (0),mostCurrent._lblpos.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1316;BA.debugLine="Dim lblRiderName As Label";
_lblridername = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1317;BA.debugLine="lblRiderName.Initialize(\"lblRiderName\")";
_lblridername.Initialize(mostCurrent.activityBA,"lblRiderName");
 //BA.debugLineNum = 1318;BA.debugLine="lblRiderName.Text = crs.GetString(\"trrSurname\")";
_lblridername.setText((Object)(_crs.GetString("trrSurname")));
 //BA.debugLineNum = 1319;BA.debugLine="lblRiderName.TextSize = lblSupport.TextSize";
_lblridername.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1320;BA.debugLine="lblRiderName.TextColor = Colors.RGB(255, 255, 25";
_lblridername.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1321;BA.debugLine="lblRiderName.Gravity = Gravity.CENTER_VERTICAL";
_lblridername.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1322;BA.debugLine="resultPanel.AddView(lblRiderName, lblRider.Left,";
_resultpanel.AddView((android.view.View)(_lblridername.getObject()),mostCurrent._lblrider.getLeft(),(int) (0),mostCurrent._lblrider.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1325;BA.debugLine="Dim lblScore As Label";
_lblscore = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1326;BA.debugLine="lblScore.Initialize(\"lblScore\")";
_lblscore.Initialize(mostCurrent.activityBA,"lblScore");
 //BA.debugLineNum = 1327;BA.debugLine="lblScore.Text = crs.GetInt(\"trqlScore\")";
_lblscore.setText((Object)(_crs.GetInt("trqlScore")));
 //BA.debugLineNum = 1328;BA.debugLine="lblScore.TextSize = lblSupport.TextSize";
_lblscore.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1329;BA.debugLine="lblScore.TextColor = Colors.RGB(255, 255, 255)";
_lblscore.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1330;BA.debugLine="lblScore.Gravity = Gravity.CENTER_VERTICAL";
_lblscore.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1331;BA.debugLine="resultPanel.AddView(lblScore, lblClass.Left, 0,";
_resultpanel.AddView((android.view.View)(_lblscore.getObject()),mostCurrent._lblclass.getLeft(),(int) (0),mostCurrent._lblclass.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1334;BA.debugLine="Dim lblResultTime As Label";
_lblresulttime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1335;BA.debugLine="lblResultTime.Initialize(\"lblTime\")";
_lblresulttime.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1336;BA.debugLine="lblResultTime.Text = crs.GetString(\"trqlTime\")";
_lblresulttime.setText((Object)(_crs.GetString("trqlTime")));
 //BA.debugLineNum = 1337;BA.debugLine="lblResultTime.TextSize = lblSupport.TextSize";
_lblresulttime.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1338;BA.debugLine="lblResultTime.TextColor = Colors.RGB(34, 180, 23";
_lblresulttime.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1339;BA.debugLine="lblResultTime.Gravity = Gravity.CENTER_VERTICAL";
_lblresulttime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1340;BA.debugLine="resultPanel.AddView(lblResultTime, lblTime.Left,";
_resultpanel.AddView((android.view.View)(_lblresulttime.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 1343;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)));
 //BA.debugLineNum = 1345;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1346;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1347;BA.debugLine="End Sub";
return "";
}
public static String  _displayqualificationstart() throws Exception{
int _placedpanels = 0;
int _panelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblindex = null;
anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
String _ridername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblriderclass = null;
String[] _str = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridertime = null;
 //BA.debugLineNum = 1373;BA.debugLine="Sub DisplayQualificationStart";
 //BA.debugLineNum = 1374;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 1375;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 14";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 1377;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 1379;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 1380;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSur";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trqsTime FROM trRiders, trQuStart WHERE trRiders.trrCode = trQuStart.trqsCode ORDER BY trqsPosition ASC")));
 //BA.debugLineNum = 1382;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 1383;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 1387;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 1388;BA.debugLine="For i = 0 To n";
{
final int step10 = 1;
final int limit10 = _n;
for (_i = (int) (0) ; (step10 > 0 && _i <= limit10) || (step10 < 0 && _i >= limit10); _i = ((int)(0 + _i + step10)) ) {
 //BA.debugLineNum = 1389;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 1392;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1393;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 1394;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 1395;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 1397;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 1400;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1401;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1404;BA.debugLine="Dim lblIndex As Label";
_lblindex = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1405;BA.debugLine="lblIndex.Initialize(\"lblIndex\")";
_lblindex.Initialize(mostCurrent.activityBA,"lblIndex");
 //BA.debugLineNum = 1406;BA.debugLine="lblIndex.Text = i + 1";
_lblindex.setText((Object)(_i+1));
 //BA.debugLineNum = 1407;BA.debugLine="lblIndex.TextSize = lblSupport.TextSize";
_lblindex.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1408;BA.debugLine="lblIndex.TextColor = Colors.RGB(150, 150, 150)";
_lblindex.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1409;BA.debugLine="lblIndex.Gravity = Gravity.CENTER_VERTICAL";
_lblindex.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1410;BA.debugLine="resultPanel.AddView(lblIndex, 5dip, 0, resultPan";
_resultpanel.AddView((android.view.View)(_lblindex.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),(int) (0),(int) (_resultpanel.getWidth()/(double)10),_resultpanel.getHeight());
 //BA.debugLineNum = 1422;BA.debugLine="Dim lblName As Label";
_lblname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1423;BA.debugLine="lblName.Initialize(\"lblName\")";
_lblname.Initialize(mostCurrent.activityBA,"lblName");
 //BA.debugLineNum = 1424;BA.debugLine="Dim riderName As String = crs.GetString(\"trrSurn";
_ridername = _crs.GetString("trrSurname");
 //BA.debugLineNum = 1425;BA.debugLine="If riderName.Length > 12 Then";
if (_ridername.length()>12) { 
 //BA.debugLineNum = 1426;BA.debugLine="lblName.Text = riderName.SubString2(0, 12)";
_lblname.setText((Object)(_ridername.substring((int) (0),(int) (12))));
 }else {
 //BA.debugLineNum = 1428;BA.debugLine="lblName.Text = riderName";
_lblname.setText((Object)(_ridername));
 };
 //BA.debugLineNum = 1430;BA.debugLine="lblName.TextSize = lblSupport.TextSize";
_lblname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1431;BA.debugLine="lblName.TextColor = Colors.RGB(255, 255, 255)";
_lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1432;BA.debugLine="lblName.Gravity = Gravity.CENTER_VERTICAL";
_lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1433;BA.debugLine="resultPanel.AddView(lblName, lblRider.Left, 0, l";
_resultpanel.AddView((android.view.View)(_lblname.getObject()),mostCurrent._lblrider.getLeft(),(int) (0),mostCurrent._lblrider.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1436;BA.debugLine="Dim lblRiderClass As Label";
_lblriderclass = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1437;BA.debugLine="lblRiderClass.Initialize(\"lblClass\")";
_lblriderclass.Initialize(mostCurrent.activityBA,"lblClass");
 //BA.debugLineNum = 1438;BA.debugLine="Dim str(2) As String";
_str = new String[(int) (2)];
java.util.Arrays.fill(_str,"");
 //BA.debugLineNum = 1439;BA.debugLine="str = Regex.split(\" \", crs.GetString(\"trrClass\")";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split(" ",_crs.GetString("trrClass"));
 //BA.debugLineNum = 1440;BA.debugLine="lblRiderClass.Text = str(0)";
_lblriderclass.setText((Object)(_str[(int) (0)]));
 //BA.debugLineNum = 1441;BA.debugLine="lblRiderClass.TextSize = lblSupport.TextSize";
_lblriderclass.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1442;BA.debugLine="lblRiderClass.TextColor = Colors.RGB(150, 150, 1";
_lblriderclass.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1443;BA.debugLine="lblRiderClass.Gravity = Gravity.LEFT";
_lblriderclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 1444;BA.debugLine="lblRiderClass.Gravity = Gravity.CENTER_VERTICAL";
_lblriderclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1445;BA.debugLine="resultPanel.AddView(lblRiderClass, lblClass.Left";
_resultpanel.AddView((android.view.View)(_lblriderclass.getObject()),mostCurrent._lblclass.getLeft(),(int) (0),(int) (mostCurrent._lblrider.getLeft()+mostCurrent._lblrider.getWidth()),_resultpanel.getHeight());
 //BA.debugLineNum = 1448;BA.debugLine="Dim lblRiderTime As Label";
_lblridertime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1449;BA.debugLine="lblRiderTime.Initialize(\"lblTime\")";
_lblridertime.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1450;BA.debugLine="lblRiderTime.Text = crs.GetString(\"trqsTime\")";
_lblridertime.setText((Object)(_crs.GetString("trqsTime")));
 //BA.debugLineNum = 1451;BA.debugLine="lblRiderTime.TextSize = lblSupport.TextSize";
_lblridertime.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1452;BA.debugLine="lblRiderTime.TextColor = Colors.RGB(34, 180, 231";
_lblridertime.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1453;BA.debugLine="lblRiderTime.Gravity = Gravity.LEFT";
_lblridertime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 1454;BA.debugLine="lblRiderTime.Gravity = Gravity.CENTER_VERTICAL";
_lblridertime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1455;BA.debugLine="resultPanel.AddView(lblRiderTime, lblTime.Left,";
_resultpanel.AddView((android.view.View)(_lblridertime.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 1457;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)));
 //BA.debugLineNum = 1459;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1460;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1461;BA.debugLine="End Sub";
return "";
}
public static String  _getrefreshinterval() throws Exception{
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
 //BA.debugLineNum = 121;BA.debugLine="Sub GetRefreshInterval";
 //BA.debugLineNum = 122;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 123;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trgUrl12 FROM t";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trgUrl12 FROM trGeneral")));
 //BA.debugLineNum = 125;BA.debugLine="If crs.RowCount = 0 Then";
if (_crs.getRowCount()==0) { 
 //BA.debugLineNum = 126;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 129;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 131;BA.debugLine="If crs.GetInt(\"trgUrl12\") > 0 Then";
if (_crs.GetInt("trgUrl12")>0) { 
 //BA.debugLineNum = 132;BA.debugLine="refreshTimer.Initialize(\"refreshTimer\", crs.GetI";
_refreshtimer.Initialize(processBA,"refreshTimer",(long) (_crs.GetInt("trgUrl12")*1000));
 //BA.debugLineNum = 133;BA.debugLine="refreshTimer.Enabled = True";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 135;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 12;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 15;BA.debugLine="Private pnlFooter As Panel";
mostCurrent._pnlfooter = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 16;BA.debugLine="Private pnlHeader As Panel";
mostCurrent._pnlheader = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 17;BA.debugLine="Private pnlTabs As Panel";
mostCurrent._pnltabs = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Private pnlTabs2 As Panel";
mostCurrent._pnltabs2 = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 19;BA.debugLine="Private pnlQualificationBar As Panel";
mostCurrent._pnlqualificationbar = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Private pnlDay1Bar As Panel";
mostCurrent._pnlday1bar = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private pnlDay2Bar As Panel";
mostCurrent._pnlday2bar = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private pnlHeadersStart As Panel";
mostCurrent._pnlheadersstart = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 24;BA.debugLine="Private imgMainLogo As ImageView";
mostCurrent._imgmainlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Private imgLeftLogo As ImageView";
mostCurrent._imgleftlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 26;BA.debugLine="Private imgRightLogo As ImageView";
mostCurrent._imgrightlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 27;BA.debugLine="Private imgHome As ImageView";
mostCurrent._imghome = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 28;BA.debugLine="Private imgArrowStart As ImageView";
mostCurrent._imgarrowstart = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 29;BA.debugLine="Private imgArrowLive As ImageView";
mostCurrent._imgarrowlive = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 31;BA.debugLine="Private lblEventName As Label";
mostCurrent._lbleventname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 32;BA.debugLine="Private lblQualification As Label";
mostCurrent._lblqualification = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 33;BA.debugLine="Private lblDay1 As Label";
mostCurrent._lblday1 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 34;BA.debugLine="Private lblDay2 As Label";
mostCurrent._lblday2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 35;BA.debugLine="Private lblStart As Label";
mostCurrent._lblstart = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 36;BA.debugLine="Private lblLive As Label";
mostCurrent._lbllive = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 37;BA.debugLine="Private lblSupport As Label";
mostCurrent._lblsupport = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Private lblNo As Label";
mostCurrent._lblno = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 40;BA.debugLine="Private lblRider As Label";
mostCurrent._lblrider = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 41;BA.debugLine="Private lblClass As Label";
mostCurrent._lblclass = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 42;BA.debugLine="Private lblTime As Label";
mostCurrent._lbltime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 43;BA.debugLine="Private lblPos As Label";
mostCurrent._lblpos = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 44;BA.debugLine="Private lblTotal As Label";
mostCurrent._lbltotal = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 45;BA.debugLine="Private lblNoData As Label";
mostCurrent._lblnodata = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 47;BA.debugLine="Private SVResults As ScrollView";
mostCurrent._svresults = new anywheresoftware.b4a.objects.ScrollViewWrapper();
 //BA.debugLineNum = 48;BA.debugLine="Dim pnlResults As Panel";
mostCurrent._pnlresults = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 50;BA.debugLine="Dim isQualification As Boolean = False";
_isqualification = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 51;BA.debugLine="Dim isDayOne As Boolean = False";
_isdayone = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 52;BA.debugLine="Dim isStart As Boolean = False";
_isstart = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 54;BA.debugLine="Dim whiteArrow As BitmapDrawable";
mostCurrent._whitearrow = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 55;BA.debugLine="Dim greyArrow As BitmapDrawable";
mostCurrent._greyarrow = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 57;BA.debugLine="Dim queryType As Int '0 = Qualification Live, 1 =";
_querytype = 0;
 //BA.debugLineNum = 60;BA.debugLine="Dim currentScroll As Int";
_currentscroll = 0;
 //BA.debugLineNum = 61;BA.debugLine="End Sub";
return "";
}
public static String  _imgarrowlive_click() throws Exception{
 //BA.debugLineNum = 1499;BA.debugLine="Sub imgArrowLive_Click";
 //BA.debugLineNum = 1500;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1501;BA.debugLine="SetActiveTab(isQualification, isDayOne, False)";
_setactivetab(_isqualification,_isdayone,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 1502;BA.debugLine="End Sub";
return "";
}
public static String  _imgarrowstart_click() throws Exception{
 //BA.debugLineNum = 1494;BA.debugLine="Sub imgArrowStart_Click";
 //BA.debugLineNum = 1495;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1496;BA.debugLine="SetActiveTab(isQualification, isDayOne, True)";
_setactivetab(_isqualification,_isdayone,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1497;BA.debugLine="End Sub";
return "";
}
public static String  _imghome_click() throws Exception{
 //BA.debugLineNum = 1504;BA.debugLine="Sub imgHome_Click";
 //BA.debugLineNum = 1505;BA.debugLine="StartActivity(Main)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._main.getObject()));
 //BA.debugLineNum = 1506;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 1507;BA.debugLine="End Sub";
return "";
}
public static String  _jobdone(trialGP.live.httpjob _job) throws Exception{
 //BA.debugLineNum = 434;BA.debugLine="Sub JobDone (Job As HttpJob)";
 //BA.debugLineNum = 436;BA.debugLine="If Job.Success = False Then";
if (_job._success==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 438;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"Job0","Job1","Job2","Job3","Job4","Job5")) {
case 0: {
 //BA.debugLineNum = 440;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 break; }
case 1: {
 //BA.debugLineNum = 442;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 break; }
case 2: {
 //BA.debugLineNum = 444;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 break; }
case 3: {
 //BA.debugLineNum = 446;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 break; }
case 4: {
 //BA.debugLineNum = 448;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 break; }
case 5: {
 //BA.debugLineNum = 450;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 break; }
}
;
 }else {
 //BA.debugLineNum = 454;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"Job0","Job1","Job2","Job3","Job4","Job5")) {
case 0: {
 //BA.debugLineNum = 456;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 457;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 }else {
 //BA.debugLineNum = 459;BA.debugLine="ParseJSONReplies(\"qualificationLive\", Job.GetS";
_parsejsonreplies("qualificationLive",_job._getstring());
 };
 break; }
case 1: {
 //BA.debugLineNum = 462;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 463;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 }else {
 //BA.debugLineNum = 465;BA.debugLine="ParseJSONReplies(\"dayOneStart\", Job.GetString)";
_parsejsonreplies("dayOneStart",_job._getstring());
 };
 break; }
case 2: {
 //BA.debugLineNum = 469;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 470;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 }else {
 //BA.debugLineNum = 472;BA.debugLine="ParseJSONReplies(\"dayOneLive\", Job.GetString)";
_parsejsonreplies("dayOneLive",_job._getstring());
 };
 break; }
case 3: {
 //BA.debugLineNum = 475;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 476;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 }else {
 //BA.debugLineNum = 478;BA.debugLine="ParseJSONReplies(\"dayTwoStart\", Job.GetString)";
_parsejsonreplies("dayTwoStart",_job._getstring());
 };
 break; }
case 4: {
 //BA.debugLineNum = 482;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 483;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 }else {
 //BA.debugLineNum = 485;BA.debugLine="ParseJSONReplies(\"dayTwoLive\", Job.GetString)";
_parsejsonreplies("dayTwoLive",_job._getstring());
 };
 break; }
case 5: {
 //BA.debugLineNum = 489;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 490;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 }else {
 //BA.debugLineNum = 492;BA.debugLine="ParseJSONReplies(\"qualificationStart\", Job.Get";
_parsejsonreplies("qualificationStart",_job._getstring());
 };
 break; }
}
;
 };
 //BA.debugLineNum = 497;BA.debugLine="Job.Release";
_job._release();
 //BA.debugLineNum = 498;BA.debugLine="End Sub";
return "";
}
public static String  _lblday1_click() throws Exception{
 //BA.debugLineNum = 1469;BA.debugLine="Sub lblDay1_Click";
 //BA.debugLineNum = 1470;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1471;BA.debugLine="SetActiveTab(False, True, isStart)";
_setactivetab(anywheresoftware.b4a.keywords.Common.False,anywheresoftware.b4a.keywords.Common.True,_isstart);
 //BA.debugLineNum = 1472;BA.debugLine="End Sub";
return "";
}
public static String  _lblday2_click() throws Exception{
 //BA.debugLineNum = 1474;BA.debugLine="Sub lblDay2_Click";
 //BA.debugLineNum = 1475;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1476;BA.debugLine="SetActiveTab(False, False, isStart)";
_setactivetab(anywheresoftware.b4a.keywords.Common.False,anywheresoftware.b4a.keywords.Common.False,_isstart);
 //BA.debugLineNum = 1477;BA.debugLine="End Sub";
return "";
}
public static String  _lbllive_click() throws Exception{
 //BA.debugLineNum = 1489;BA.debugLine="Sub lblLive_Click";
 //BA.debugLineNum = 1490;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1491;BA.debugLine="SetActiveTab(isQualification, isDayOne, False)";
_setactivetab(_isqualification,_isdayone,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 1492;BA.debugLine="End Sub";
return "";
}
public static String  _lblqualification_click() throws Exception{
 //BA.debugLineNum = 1479;BA.debugLine="Sub lblQualification_Click";
 //BA.debugLineNum = 1480;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1481;BA.debugLine="SetActiveTab(True, False, isStart)";
_setactivetab(anywheresoftware.b4a.keywords.Common.True,anywheresoftware.b4a.keywords.Common.False,_isstart);
 //BA.debugLineNum = 1482;BA.debugLine="End Sub";
return "";
}
public static String  _lblstart_click() throws Exception{
 //BA.debugLineNum = 1484;BA.debugLine="Sub lblStart_Click";
 //BA.debugLineNum = 1485;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1486;BA.debugLine="SetActiveTab(isQualification, isDayOne, True)";
_setactivetab(_isqualification,_isdayone,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1487;BA.debugLine="End Sub";
return "";
}
public static String  _parsejsonreplies(String _mode,String _reply) throws Exception{
boolean _success = false;
anywheresoftware.b4a.objects.collections.JSONParser _jpar = null;
anywheresoftware.b4a.objects.collections.Map _m = null;
String _updatedon = "";
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.collections.Map _tmpmap = null;
String[] _stuntsone = null;
String[] _stuntstwo = null;
anywheresoftware.b4a.objects.collections.Map _stuntsmap = null;
int _k = 0;
int _pos = 0;
String _lvnames = "";
int _counter = 0;
 //BA.debugLineNum = 501;BA.debugLine="Sub ParseJSONReplies(mode As String, reply As Stri";
 //BA.debugLineNum = 502;BA.debugLine="Dim success As Boolean = False";
_success = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 504;BA.debugLine="If reply = \"\" Then";
if ((_reply).equals("")) { 
 //BA.debugLineNum = 505;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 506;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 509;BA.debugLine="Dim jPar As JSONParser";
_jpar = new anywheresoftware.b4a.objects.collections.JSONParser();
 //BA.debugLineNum = 510;BA.debugLine="Dim m As Map";
_m = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 511;BA.debugLine="jPar.Initialize(reply)";
_jpar.Initialize(_reply);
 //BA.debugLineNum = 512;BA.debugLine="m = jPar.NextObject";
_m = _jpar.NextObject();
 //BA.debugLineNum = 514;BA.debugLine="Log(\"RESULT: \" & m.Get(\"Result\"))";
anywheresoftware.b4a.keywords.Common.Log("RESULT: "+BA.ObjectToString(_m.Get((Object)("Result"))));
 //BA.debugLineNum = 516;BA.debugLine="If m.Get(\"Result\") <> \"0\" Then";
if ((_m.Get((Object)("Result"))).equals((Object)("0")) == false) { 
 //BA.debugLineNum = 517;BA.debugLine="If m.Get(\"Result\") = \"2\" Then";
if ((_m.Get((Object)("Result"))).equals((Object)("2"))) { 
 //BA.debugLineNum = 518;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 520;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trQuStart";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trQuStart");
 //BA.debugLineNum = 521;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 break; }
case 1: {
 //BA.debugLineNum = 523;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trQuLive\"";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trQuLive");
 //BA.debugLineNum = 524;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 break; }
case 2: {
 //BA.debugLineNum = 526;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDayStar";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDayStart");
 //BA.debugLineNum = 527;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 break; }
case 3: {
 //BA.debugLineNum = 529;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDayLive";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDayLive");
 //BA.debugLineNum = 530;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 break; }
case 4: {
 //BA.debugLineNum = 532;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDay2Sta";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDay2Start");
 //BA.debugLineNum = 533;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 break; }
case 5: {
 //BA.debugLineNum = 535;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDay2Liv";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDay2Live");
 //BA.debugLineNum = 536;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 break; }
}
;
 }else {
 //BA.debugLineNum = 539;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 541;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 break; }
case 1: {
 //BA.debugLineNum = 543;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 break; }
case 2: {
 //BA.debugLineNum = 545;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 break; }
case 3: {
 //BA.debugLineNum = 547;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 break; }
case 4: {
 //BA.debugLineNum = 549;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 break; }
case 5: {
 //BA.debugLineNum = 551;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 break; }
}
;
 };
 //BA.debugLineNum = 554;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 555;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 558;BA.debugLine="Dim updatedOn As String";
_updatedon = "";
 //BA.debugLineNum = 559;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 563;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 564;BA.debugLine="For i = 0 To n";
{
final int step56 = 1;
final int limit56 = _n;
for (_i = (int) (0) ; (step56 > 0 && _i <= limit56) || (step56 < 0 && _i >= limit56); _i = ((int)(0 + _i + step56)) ) {
 //BA.debugLineNum = 566;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 567;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 568;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 571;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 573;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 574;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 575;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trRi";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trRiders (trrCode, trrSurname, trrClass, trrTeam, trrName, trrNationality) VALUES (?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("surname")),_tmpmap.Get((Object)("motoClass")),_tmpmap.Get((Object)("team")),_tmpmap.Get((Object)("name")),_tmpmap.Get((Object)("nationality"))}));
 //BA.debugLineNum = 577;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trQu";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trQuStart (trqsCode, trqsPosition, trqstime) VALUES (?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time"))}));
 };
 }
};
 break; }
case 1: {
 //BA.debugLineNum = 583;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 584;BA.debugLine="For i = 0 To n";
{
final int step72 = 1;
final int limit72 = _n;
for (_i = (int) (0) ; (step72 > 0 && _i <= limit72) || (step72 < 0 && _i >= limit72); _i = ((int)(0 + _i + step72)) ) {
 //BA.debugLineNum = 586;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 587;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 588;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 591;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 594;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 595;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 596;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trQ";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trQuLive (trqlCode, trqlPosition, trqlTime, trqlScore, trqlIndex) VALUES (?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time")),_tmpmap.Get((Object)("score")),_tmpmap.Get((Object)("index"))}));
 };
 }
};
 break; }
case 2: {
 //BA.debugLineNum = 602;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 603;BA.debugLine="For i = 0 To n";
{
final int step87 = 1;
final int limit87 = _n;
for (_i = (int) (0) ; (step87 > 0 && _i <= limit87) || (step87 < 0 && _i >= limit87); _i = ((int)(0 + _i + step87)) ) {
 //BA.debugLineNum = 605;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 606;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 607;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 610;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 613;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 614;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 615;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trD";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trDayStart (trdsCode, trdsPosition, trdsTime) VALUES (?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time"))}));
 };
 }
};
 break; }
case 3: {
 //BA.debugLineNum = 621;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 622;BA.debugLine="For i = 0 To n";
{
final int step102 = 1;
final int limit102 = _n;
for (_i = (int) (0) ; (step102 > 0 && _i <= limit102) || (step102 < 0 && _i >= limit102); _i = ((int)(0 + _i + step102)) ) {
 //BA.debugLineNum = 624;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 625;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 626;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 629;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 632;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 633;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 634;BA.debugLine="Log(\"TMPMAP:\"&tmpMap)";
anywheresoftware.b4a.keywords.Common.Log("TMPMAP:"+BA.ObjectToString(_tmpmap));
 //BA.debugLineNum = 635;BA.debugLine="Dim stuntsOne(15) As String";
_stuntsone = new String[(int) (15)];
java.util.Arrays.fill(_stuntsone,"");
 //BA.debugLineNum = 636;BA.debugLine="Dim stuntsTwo(15) As String";
_stuntstwo = new String[(int) (15)];
java.util.Arrays.fill(_stuntstwo,"");
 //BA.debugLineNum = 637;BA.debugLine="If tmpMap.ContainsKey(\"one\") Then";
if (_tmpmap.ContainsKey((Object)("one"))) { 
 //BA.debugLineNum = 638;BA.debugLine="Dim stuntsMap As Map";
_stuntsmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 639;BA.debugLine="stuntsMap = tmpMap.Get(\"one\")";
_stuntsmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_tmpmap.Get((Object)("one"))));
 //BA.debugLineNum = 640;BA.debugLine="For k = 0 To stuntsMap.Size - 1";
{
final int step118 = 1;
final int limit118 = (int) (_stuntsmap.getSize()-1);
for (_k = (int) (0) ; (step118 > 0 && _k <= limit118) || (step118 < 0 && _k >= limit118); _k = ((int)(0 + _k + step118)) ) {
 //BA.debugLineNum = 641;BA.debugLine="Dim pos As Int = stuntsMap.GetKeyAt(k)";
_pos = (int)(BA.ObjectToNumber(_stuntsmap.GetKeyAt(_k)));
 //BA.debugLineNum = 642;BA.debugLine="pos = pos -1";
_pos = (int) (_pos-1);
 //BA.debugLineNum = 643;BA.debugLine="stuntsOne(pos) = stuntsMap.GetValueAt(k)";
_stuntsone[_pos] = BA.ObjectToString(_stuntsmap.GetValueAt(_k));
 }
};
 };
 //BA.debugLineNum = 646;BA.debugLine="If tmpMap.ContainsKey(\"two\") Then";
if (_tmpmap.ContainsKey((Object)("two"))) { 
 //BA.debugLineNum = 647;BA.debugLine="Dim stuntsMap As Map";
_stuntsmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 648;BA.debugLine="stuntsMap = tmpMap.Get(\"two\")";
_stuntsmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_tmpmap.Get((Object)("two"))));
 //BA.debugLineNum = 649;BA.debugLine="For k = 0 To stuntsMap.Size - 1";
{
final int step127 = 1;
final int limit127 = (int) (_stuntsmap.getSize()-1);
for (_k = (int) (0) ; (step127 > 0 && _k <= limit127) || (step127 < 0 && _k >= limit127); _k = ((int)(0 + _k + step127)) ) {
 //BA.debugLineNum = 650;BA.debugLine="Dim pos As Int = stuntsMap.GetKeyAt(k)";
_pos = (int)(BA.ObjectToNumber(_stuntsmap.GetKeyAt(_k)));
 //BA.debugLineNum = 651;BA.debugLine="pos = pos -1";
_pos = (int) (_pos-1);
 //BA.debugLineNum = 652;BA.debugLine="stuntsTwo(pos) = stuntsMap.GetValueAt(k)";
_stuntstwo[_pos] = BA.ObjectToString(_stuntsmap.GetValueAt(_k));
 }
};
 };
 //BA.debugLineNum = 655;BA.debugLine="Dim lvNames As String = stuntsOne(0)&\"##\"&stu";
_lvnames = _stuntsone[(int) (0)]+"##"+_stuntsone[(int) (1)]+"##"+_stuntsone[(int) (2)]+"##"+_stuntsone[(int) (3)]+"##"+_stuntsone[(int) (4)]+"##"+_stuntsone[(int) (5)]+"##"+_stuntsone[(int) (6)]+"##"+_stuntsone[(int) (7)]+"##"+_stuntsone[(int) (8)]+"##"+_stuntsone[(int) (9)]+"##"+_stuntsone[(int) (10)]+"##"+_stuntsone[(int) (11)]+"##"+_stuntsone[(int) (12)]+"##"+_stuntsone[(int) (13)];
 //BA.debugLineNum = 656;BA.debugLine="Dim counter As Int=0";
_counter = (int) (0);
 //BA.debugLineNum = 657;BA.debugLine="Do While (lvNames.Length>0)";
while ((_lvnames.length()>0)) {
 //BA.debugLineNum = 658;BA.debugLine="Log(counter)";
anywheresoftware.b4a.keywords.Common.Log(BA.NumberToString(_counter));
 //BA.debugLineNum = 659;BA.debugLine="If counter <> 14 And counter <> 13 Then";
if (_counter!=14 && _counter!=13) { 
 //BA.debugLineNum = 660;BA.debugLine="If (lvNames.Length<10) Then";
if ((_lvnames.length()<10)) { 
 //BA.debugLineNum = 661;BA.debugLine="stuntsOne(counter) = lvNames.SubString2(0,";
_stuntsone[_counter] = _lvnames.substring((int) (0),_lvnames.length());
 }else {
 //BA.debugLineNum = 663;BA.debugLine="stuntsOne(counter) = lvNames.SubString2(0,";
_stuntsone[_counter] = _lvnames.substring((int) (0),(int) (10));
 };
 };
 //BA.debugLineNum = 666;BA.debugLine="If (lvNames.Length>10) Then";
if ((_lvnames.length()>10)) { 
 //BA.debugLineNum = 667;BA.debugLine="lvNames = lvNames.SubString(10)";
_lvnames = _lvnames.substring((int) (10));
 }else {
 //BA.debugLineNum = 669;BA.debugLine="lvNames = \"\"";
_lvnames = "";
 };
 //BA.debugLineNum = 671;BA.debugLine="counter = counter + 1";
_counter = (int) (_counter+1);
 }
;
 //BA.debugLineNum = 673;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trD";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trDayLive (trdlCode, trdlL1, trdlL2, trdlPosition, trdlIndex, trdlPenalty, trdlTotal, trdlStunt1, trdlStunt2, trdlStunt3, trdlStunt4, trdlStunt5, trdlStunt6, trdlStunt7, trdlStunt8, trdlStunt9, trdlStunt10, trdlStunt11, trdlStunt12, trdlStunt13, trdlStunt14, trdlStunt15) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("l1")),_tmpmap.Get((Object)("l2")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("index")),_tmpmap.Get((Object)("penalty")),_tmpmap.Get((Object)("total")),(Object)(_stuntsone[(int) (0)]),(Object)(_stuntsone[(int) (1)]),(Object)(_stuntsone[(int) (2)]),(Object)(_stuntsone[(int) (3)]),(Object)(_stuntsone[(int) (4)]),(Object)(_stuntsone[(int) (5)]),(Object)(_stuntsone[(int) (6)]),(Object)(_stuntsone[(int) (7)]),(Object)(_stuntsone[(int) (8)]),(Object)(_stuntsone[(int) (9)]),(Object)(_stuntsone[(int) (10)]),(Object)(_stuntsone[(int) (11)]),(Object)(_stuntsone[(int) (12)]),(Object)(_stuntsone[(int) (13)]),(Object)(_stuntsone[(int) (14)])}));
 };
 }
};
 break; }
case 4: {
 //BA.debugLineNum = 679;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 680;BA.debugLine="For i = 0 To n";
{
final int step156 = 1;
final int limit156 = _n;
for (_i = (int) (0) ; (step156 > 0 && _i <= limit156) || (step156 < 0 && _i >= limit156); _i = ((int)(0 + _i + step156)) ) {
 //BA.debugLineNum = 682;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 683;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 684;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 687;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 690;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 691;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 692;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trD";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trDay2Start (trds2Code, trds2Position, trds2Time) VALUES (?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time"))}));
 };
 }
};
 break; }
case 5: {
 //BA.debugLineNum = 698;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 699;BA.debugLine="For i = 0 To n";
{
final int step171 = 1;
final int limit171 = _n;
for (_i = (int) (0) ; (step171 > 0 && _i <= limit171) || (step171 < 0 && _i >= limit171); _i = ((int)(0 + _i + step171)) ) {
 //BA.debugLineNum = 701;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 702;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 703;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 706;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 709;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 710;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 712;BA.debugLine="Dim stuntsOne(15) As String";
_stuntsone = new String[(int) (15)];
java.util.Arrays.fill(_stuntsone,"");
 //BA.debugLineNum = 713;BA.debugLine="Dim stuntsTwo(15) As String";
_stuntstwo = new String[(int) (15)];
java.util.Arrays.fill(_stuntstwo,"");
 //BA.debugLineNum = 714;BA.debugLine="If tmpMap.ContainsKey(\"one\") Then";
if (_tmpmap.ContainsKey((Object)("one"))) { 
 //BA.debugLineNum = 715;BA.debugLine="Dim stuntsMap As Map";
_stuntsmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 716;BA.debugLine="stuntsMap = tmpMap.Get(\"one\")";
_stuntsmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_tmpmap.Get((Object)("one"))));
 //BA.debugLineNum = 717;BA.debugLine="For k = 0 To stuntsMap.Size - 1";
{
final int step186 = 1;
final int limit186 = (int) (_stuntsmap.getSize()-1);
for (_k = (int) (0) ; (step186 > 0 && _k <= limit186) || (step186 < 0 && _k >= limit186); _k = ((int)(0 + _k + step186)) ) {
 //BA.debugLineNum = 718;BA.debugLine="Dim pos As Int = stuntsMap.GetKeyAt(k)";
_pos = (int)(BA.ObjectToNumber(_stuntsmap.GetKeyAt(_k)));
 //BA.debugLineNum = 719;BA.debugLine="pos = pos -1";
_pos = (int) (_pos-1);
 //BA.debugLineNum = 720;BA.debugLine="stuntsOne(pos) = stuntsMap.GetValueAt(k)";
_stuntsone[_pos] = BA.ObjectToString(_stuntsmap.GetValueAt(_k));
 }
};
 };
 //BA.debugLineNum = 724;BA.debugLine="If tmpMap.ContainsKey(\"two\") Then";
if (_tmpmap.ContainsKey((Object)("two"))) { 
 //BA.debugLineNum = 725;BA.debugLine="Dim stuntsMap As Map";
_stuntsmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 726;BA.debugLine="stuntsMap = tmpMap.Get(\"two\")";
_stuntsmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_tmpmap.Get((Object)("two"))));
 //BA.debugLineNum = 727;BA.debugLine="For k = 0 To stuntsMap.Size - 1";
{
final int step195 = 1;
final int limit195 = (int) (_stuntsmap.getSize()-1);
for (_k = (int) (0) ; (step195 > 0 && _k <= limit195) || (step195 < 0 && _k >= limit195); _k = ((int)(0 + _k + step195)) ) {
 //BA.debugLineNum = 728;BA.debugLine="Dim pos As Int = stuntsMap.GetKeyAt(k)";
_pos = (int)(BA.ObjectToNumber(_stuntsmap.GetKeyAt(_k)));
 //BA.debugLineNum = 729;BA.debugLine="pos = pos -1";
_pos = (int) (_pos-1);
 //BA.debugLineNum = 730;BA.debugLine="stuntsTwo(pos) = stuntsMap.GetValueAt(k)";
_stuntstwo[_pos] = BA.ObjectToString(_stuntsmap.GetValueAt(_k));
 }
};
 };
 //BA.debugLineNum = 734;BA.debugLine="Dim lvNames As String = stuntsOne(0)&\"##\"&stu";
_lvnames = _stuntsone[(int) (0)]+"##"+_stuntsone[(int) (1)]+"##"+_stuntsone[(int) (2)]+"##"+_stuntsone[(int) (3)]+"##"+_stuntsone[(int) (4)]+"##"+_stuntsone[(int) (5)]+"##"+_stuntsone[(int) (6)]+"##"+_stuntsone[(int) (7)]+"##"+_stuntsone[(int) (8)]+"##"+_stuntsone[(int) (9)]+"##"+_stuntsone[(int) (10)]+"##"+_stuntsone[(int) (11)]+"##"+_stuntsone[(int) (12)]+"##"+_stuntsone[(int) (13)];
 //BA.debugLineNum = 735;BA.debugLine="Dim counter As Int=0";
_counter = (int) (0);
 //BA.debugLineNum = 736;BA.debugLine="Do While (lvNames.Length>0)";
while ((_lvnames.length()>0)) {
 //BA.debugLineNum = 737;BA.debugLine="Log(counter)";
anywheresoftware.b4a.keywords.Common.Log(BA.NumberToString(_counter));
 //BA.debugLineNum = 738;BA.debugLine="If counter <> 14 And counter <> 13 Then";
if (_counter!=14 && _counter!=13) { 
 //BA.debugLineNum = 739;BA.debugLine="If (lvNames.Length<10) Then";
if ((_lvnames.length()<10)) { 
 //BA.debugLineNum = 740;BA.debugLine="stuntsOne(counter) = lvNames.SubString2(0,";
_stuntsone[_counter] = _lvnames.substring((int) (0),_lvnames.length());
 }else {
 //BA.debugLineNum = 742;BA.debugLine="stuntsOne(counter) = lvNames.SubString2(0,";
_stuntsone[_counter] = _lvnames.substring((int) (0),(int) (10));
 };
 };
 //BA.debugLineNum = 745;BA.debugLine="If (lvNames.Length>10) Then";
if ((_lvnames.length()>10)) { 
 //BA.debugLineNum = 746;BA.debugLine="lvNames = lvNames.SubString(10)";
_lvnames = _lvnames.substring((int) (10));
 }else {
 //BA.debugLineNum = 748;BA.debugLine="lvNames = \"\"";
_lvnames = "";
 };
 //BA.debugLineNum = 750;BA.debugLine="counter = counter + 1";
_counter = (int) (_counter+1);
 }
;
 //BA.debugLineNum = 753;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trD";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trDay2Live (trdl2Code, trdl2L1, trdl2L2, trdl2Position, trdl2Index, trdl2Penalty, trdl2Total, trdl2Stunt1, trdl2Stunt2, trdl2Stunt3, trdl2Stunt4, trdl2Stunt5, trdl2Stunt6, trdl2Stunt7, trdl2Stunt8, trdl2Stunt9, trdl2Stunt10, trdl2Stunt11, trdl2Stunt12, trdl2Stunt13, trdl2Stunt14, trdl2Stunt15) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("l1")),_tmpmap.Get((Object)("l2")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("index")),_tmpmap.Get((Object)("penalty")),_tmpmap.Get((Object)("total")),(Object)(_stuntsone[(int) (0)]),(Object)(_stuntsone[(int) (1)]),(Object)(_stuntsone[(int) (2)]),(Object)(_stuntsone[(int) (3)]),(Object)(_stuntsone[(int) (4)]),(Object)(_stuntsone[(int) (5)]),(Object)(_stuntsone[(int) (6)]),(Object)(_stuntsone[(int) (7)]),(Object)(_stuntsone[(int) (8)]),(Object)(_stuntsone[(int) (9)]),(Object)(_stuntsone[(int) (10)]),(Object)(_stuntsone[(int) (11)]),(Object)(_stuntsone[(int) (12)]),(Object)(_stuntsone[(int) (13)]),(Object)(_stuntsone[(int) (14)])}));
 };
 }
};
 break; }
}
;
 //BA.debugLineNum = 759;BA.debugLine="If success Then";
if (_success) { 
 //BA.debugLineNum = 760;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 762;BA.debugLine="queryType = 5";
_querytype = (int) (5);
 //BA.debugLineNum = 763;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trQuStart\"";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trQuStart");
 //BA.debugLineNum = 765;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trRiders\")";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trRiders");
 //BA.debugLineNum = 767;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 769;BA.debugLine="Log(\"UPDATED ON: \" & updatedOn)";
anywheresoftware.b4a.keywords.Common.Log("UPDATED ON: "+_updatedon);
 //BA.debugLineNum = 770;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trQuStart SET t";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trQuStart SET trqsUpdatedOn = '"+_updatedon+"'");
 break; }
case 1: {
 //BA.debugLineNum = 772;BA.debugLine="queryType = 0";
_querytype = (int) (0);
 //BA.debugLineNum = 773;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trQuLive\")";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trQuLive");
 //BA.debugLineNum = 774;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 776;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trQuLive SET tr";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trQuLive SET trqlUpdatedOn = '"+_updatedon+"'");
 break; }
case 2: {
 //BA.debugLineNum = 778;BA.debugLine="queryType = 1";
_querytype = (int) (1);
 //BA.debugLineNum = 779;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDayStart";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDayStart");
 //BA.debugLineNum = 780;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 782;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trDayStart SET";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trDayStart SET trdsUpdatedOn = '"+_updatedon+"'");
 break; }
case 3: {
 //BA.debugLineNum = 784;BA.debugLine="queryType = 2";
_querytype = (int) (2);
 //BA.debugLineNum = 785;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDayLive\"";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDayLive");
 //BA.debugLineNum = 786;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 788;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trDayLive SET t";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trDayLive SET trdlUpdatedOn = '"+_updatedon+"'");
 break; }
case 4: {
 //BA.debugLineNum = 790;BA.debugLine="queryType = 3";
_querytype = (int) (3);
 //BA.debugLineNum = 791;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDay2Star";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDay2Start");
 //BA.debugLineNum = 792;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 794;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trDay2Start SET";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trDay2Start SET trds2UpdatedOn = '"+_updatedon+"'");
 break; }
case 5: {
 //BA.debugLineNum = 796;BA.debugLine="queryType = 4";
_querytype = (int) (4);
 //BA.debugLineNum = 797;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDay2Live";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDay2Live");
 //BA.debugLineNum = 798;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 800;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trDay2Live SET";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trDay2Live SET trdl2UpdatedOn = '"+_updatedon+"'");
 break; }
}
;
 }else {
 //BA.debugLineNum = 804;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 805;BA.debugLine="Msgbox(\"Error while updating db\", \"Error!\")";
anywheresoftware.b4a.keywords.Common.Msgbox("Error while updating db","Error!",mostCurrent.activityBA);
 //BA.debugLineNum = 806;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 808;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim refreshTimer As Timer";
_refreshtimer = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 10;BA.debugLine="End Sub";
return "";
}
public static String  _refreshtimer_tick() throws Exception{
 //BA.debugLineNum = 1509;BA.debugLine="Sub refreshTimer_tick";
 //BA.debugLineNum = 1510;BA.debugLine="currentScroll = SVResults.ScrollPosition";
_currentscroll = mostCurrent._svresults.getScrollPosition();
 //BA.debugLineNum = 1511;BA.debugLine="If isStart = False Then";
if (_isstart==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 1512;BA.debugLine="SetActiveTab(isQualification, isDayOne, isStart)";
_setactivetab(_isqualification,_isdayone,_isstart);
 };
 //BA.debugLineNum = 1514;BA.debugLine="End Sub";
return "";
}
public static String  _requestresults(String _mode) throws Exception{
String _datestring = "";
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
trialGP.live.httpjob _job = null;
 //BA.debugLineNum = 300;BA.debugLine="Sub RequestResults(mode As String)";
 //BA.debugLineNum = 301;BA.debugLine="Dim dateString As String";
_datestring = "";
 //BA.debugLineNum = 302;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 //BA.debugLineNum = 304;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 307;BA.debugLine="ProgressDialogShow2(\"Retrieving Qualification S";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Qualification Start List...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 310;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 311;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trqsUpdatedOn";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trqsUpdatedOn FROM trQuStart")));
 //BA.debugLineNum = 312;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 313;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 314;BA.debugLine="dateString = crs.GetString(\"trqsUpdatedOn\")";
_datestring = _crs.GetString("trqsUpdatedOn");
 //BA.debugLineNum = 316;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 317;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 322;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 323;BA.debugLine="Job.Initialize(\"Job5\", Me)";
_job._initialize(processBA,"Job5",desnationsevent.getObject());
 //BA.debugLineNum = 324;BA.debugLine="Job.PostString(Main.server & \"/QualifiersStartL";
_job._poststring(mostCurrent._main._server+"/QualifiersStartList?date="+_datestring,"");
 //BA.debugLineNum = 325;BA.debugLine="Log(\"Date Qualification Start:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Qualification Start:"+_datestring);
 break; }
case 1: {
 //BA.debugLineNum = 328;BA.debugLine="ProgressDialogShow2(\"Retrieving Qualification L";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Qualification Live Results...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 331;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 332;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trqlUpdatedOn";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trqlUpdatedOn FROM trQuLive")));
 //BA.debugLineNum = 333;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 334;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 335;BA.debugLine="dateString = crs.GetString(\"trqlUpdatedOn\")";
_datestring = _crs.GetString("trqlUpdatedOn");
 //BA.debugLineNum = 337;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 338;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 343;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 344;BA.debugLine="Job.Initialize(\"Job0\", Me)";
_job._initialize(processBA,"Job0",desnationsevent.getObject());
 //BA.debugLineNum = 345;BA.debugLine="Job.PostString(Main.server & \"/QualifiersResult";
_job._poststring(mostCurrent._main._server+"/QualifiersResults?date="+_datestring,"");
 //BA.debugLineNum = 346;BA.debugLine="Log(\"Date Qualification Live:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Qualification Live:"+_datestring);
 break; }
case 2: {
 //BA.debugLineNum = 349;BA.debugLine="ProgressDialogShow2(\"Retrieving Women Start Lis";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Women Start List...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 352;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 353;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trdsUpdatedOn";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trdsUpdatedOn FROM trDayStart")));
 //BA.debugLineNum = 354;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 355;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 356;BA.debugLine="dateString = crs.GetString(\"trdsUpdatedOn\")";
_datestring = _crs.GetString("trdsUpdatedOn");
 //BA.debugLineNum = 358;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 359;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 364;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 365;BA.debugLine="Job.Initialize(\"Job1\", Me)";
_job._initialize(processBA,"Job1",desnationsevent.getObject());
 //BA.debugLineNum = 366;BA.debugLine="Job.PostString(Main.server & \"/DayOneStartList?";
_job._poststring(mostCurrent._main._server+"/DayOneStartList?date="+_datestring,"");
 //BA.debugLineNum = 367;BA.debugLine="Log(\"Date Day One Start:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Day One Start:"+_datestring);
 break; }
case 3: {
 //BA.debugLineNum = 370;BA.debugLine="ProgressDialogShow2(\"Retrieving Women Live Resu";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Women Live Results...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 373;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 374;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trdlUpdatedOn";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trdlUpdatedOn FROM trDayLive")));
 //BA.debugLineNum = 375;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 376;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 377;BA.debugLine="dateString = crs.GetString(\"trdlUpdatedOn\")";
_datestring = _crs.GetString("trdlUpdatedOn");
 //BA.debugLineNum = 379;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 380;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 385;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 386;BA.debugLine="Job.Initialize(\"Job2\", Me)";
_job._initialize(processBA,"Job2",desnationsevent.getObject());
 //BA.debugLineNum = 387;BA.debugLine="Job.PostString(Main.server & \"/DayOneResults?da";
_job._poststring(mostCurrent._main._server+"/DayOneResults?date="+_datestring,"");
 //BA.debugLineNum = 388;BA.debugLine="Log(\"Date Day One Live:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Day One Live:"+_datestring);
 break; }
case 4: {
 //BA.debugLineNum = 391;BA.debugLine="ProgressDialogShow2(\"Retrieving Men Start List.";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Men Start List...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 394;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 395;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trds2UpdatedO";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trds2UpdatedOn FROM trDay2Start")));
 //BA.debugLineNum = 396;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 397;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 398;BA.debugLine="dateString = crs.GetString(\"trds2UpdatedOn\")";
_datestring = _crs.GetString("trds2UpdatedOn");
 //BA.debugLineNum = 400;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 401;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 406;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 407;BA.debugLine="Job.Initialize(\"Job3\", Me)";
_job._initialize(processBA,"Job3",desnationsevent.getObject());
 //BA.debugLineNum = 408;BA.debugLine="Job.PostString(Main.server & \"/DayTwoStartList?";
_job._poststring(mostCurrent._main._server+"/DayTwoStartList?date="+_datestring,"");
 //BA.debugLineNum = 409;BA.debugLine="Log(\"Date Day 2 Start:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Day 2 Start:"+_datestring);
 break; }
case 5: {
 //BA.debugLineNum = 412;BA.debugLine="ProgressDialogShow2(\"Retrieving Men Live Result";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Men Live Results...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 415;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 416;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trdl2UpdatedO";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trdl2UpdatedOn FROM trDay2Live")));
 //BA.debugLineNum = 417;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 418;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 419;BA.debugLine="dateString = crs.GetString(\"trdl2UpdatedOn\")";
_datestring = _crs.GetString("trdl2UpdatedOn");
 //BA.debugLineNum = 421;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 422;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 427;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 428;BA.debugLine="Job.Initialize(\"Job4\", Me)";
_job._initialize(processBA,"Job4",desnationsevent.getObject());
 //BA.debugLineNum = 429;BA.debugLine="Job.PostString(Main.server & \"/DayTwoResults?da";
_job._poststring(mostCurrent._main._server+"/DayTwoResults?date="+_datestring,"");
 //BA.debugLineNum = 430;BA.debugLine="Log(\"Date Day 2 Live:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Day 2 Live:"+_datestring);
 break; }
}
;
 //BA.debugLineNum = 432;BA.debugLine="End Sub";
return "";
}
public static String  _setactivetab(boolean _isqual,boolean _isday1,boolean _isst) throws Exception{
 //BA.debugLineNum = 137;BA.debugLine="Sub SetActiveTab(isQual As Boolean, isDay1 As Bool";
 //BA.debugLineNum = 138;BA.debugLine="refreshTimer.Enabled = False";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 139;BA.debugLine="If refreshTimer.Interval > 0 Then";
if (_refreshtimer.getInterval()>0) { 
 //BA.debugLineNum = 140;BA.debugLine="refreshTimer.Enabled = True";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 143;BA.debugLine="isQualification = isQual";
_isqualification = _isqual;
 //BA.debugLineNum = 144;BA.debugLine="isStart = isSt";
_isstart = _isst;
 //BA.debugLineNum = 145;BA.debugLine="isDayOne = isDay1";
_isdayone = _isday1;
 //BA.debugLineNum = 147;BA.debugLine="SetupLabels";
_setuplabels();
 //BA.debugLineNum = 150;BA.debugLine="If isQual Then";
if (_isqual) { 
 //BA.debugLineNum = 151;BA.debugLine="lblQualification.TextColor = Colors.RGB(34, 180,";
mostCurrent._lblqualification.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 152;BA.debugLine="pnlQualificationBar.Color = Colors.RGB(34, 180,";
mostCurrent._pnlqualificationbar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 153;BA.debugLine="lblDay1.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblday1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 154;BA.debugLine="lblDay2.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblday2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 155;BA.debugLine="pnlDay1Bar.Color = Colors.RGB(106, 107, 108)";
mostCurrent._pnlday1bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 156;BA.debugLine="pnlDay2Bar.Color = Colors.RGB(106, 107, 108)";
mostCurrent._pnlday2bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 }else if(_isday1) { 
 //BA.debugLineNum = 158;BA.debugLine="lblQualification.TextColor = Colors.RGB(106, 107";
mostCurrent._lblqualification.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 159;BA.debugLine="pnlQualificationBar.Color = Colors.RGB(106, 107,";
mostCurrent._pnlqualificationbar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 160;BA.debugLine="lblDay1.TextColor = Colors.RGB(34, 180, 231)";
mostCurrent._lblday1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 161;BA.debugLine="lblDay2.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblday2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 162;BA.debugLine="pnlDay1Bar.Color = Colors.RGB(34, 180, 231)";
mostCurrent._pnlday1bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 163;BA.debugLine="pnlDay2Bar.Color = Colors.RGB(106, 107, 108)";
mostCurrent._pnlday2bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 }else {
 //BA.debugLineNum = 165;BA.debugLine="lblQualification.TextColor = Colors.RGB(106, 107";
mostCurrent._lblqualification.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 166;BA.debugLine="pnlQualificationBar.Color = Colors.RGB(106, 107,";
mostCurrent._pnlqualificationbar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 167;BA.debugLine="lblDay1.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblday1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 168;BA.debugLine="lblDay2.TextColor = Colors.RGB(34, 180, 231)";
mostCurrent._lblday2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 169;BA.debugLine="pnlDay1Bar.Color = Colors.RGB(106, 107, 108)";
mostCurrent._pnlday1bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 170;BA.debugLine="pnlDay2Bar.Color = Colors.RGB(34, 180, 231)";
mostCurrent._pnlday2bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 };
 //BA.debugLineNum = 173;BA.debugLine="If isSt Then";
if (_isst) { 
 //BA.debugLineNum = 174;BA.debugLine="lblStart.TextColor = Colors.RGB(255, 255, 255)";
mostCurrent._lblstart.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 175;BA.debugLine="lblLive.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lbllive.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 176;BA.debugLine="imgArrowStart.Background = whiteArrow";
mostCurrent._imgarrowstart.setBackground((android.graphics.drawable.Drawable)(mostCurrent._whitearrow.getObject()));
 //BA.debugLineNum = 177;BA.debugLine="imgArrowLive.Background = greyArrow";
mostCurrent._imgarrowlive.setBackground((android.graphics.drawable.Drawable)(mostCurrent._greyarrow.getObject()));
 }else {
 //BA.debugLineNum = 179;BA.debugLine="lblStart.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblstart.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 180;BA.debugLine="lblLive.TextColor = Colors.RGB(255, 255, 255)";
mostCurrent._lbllive.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 181;BA.debugLine="imgArrowStart.Background = greyArrow";
mostCurrent._imgarrowstart.setBackground((android.graphics.drawable.Drawable)(mostCurrent._greyarrow.getObject()));
 //BA.debugLineNum = 182;BA.debugLine="imgArrowLive.Background = whiteArrow";
mostCurrent._imgarrowlive.setBackground((android.graphics.drawable.Drawable)(mostCurrent._whitearrow.getObject()));
 };
 //BA.debugLineNum = 185;BA.debugLine="If isQual Then";
if (_isqual) { 
 //BA.debugLineNum = 186;BA.debugLine="If isSt Then";
if (_isst) { 
 //BA.debugLineNum = 188;BA.debugLine="RequestResults(\"qualificationStart\")";
_requestresults("qualificationStart");
 }else {
 //BA.debugLineNum = 190;BA.debugLine="RequestResults(\"qualificationLive\")";
_requestresults("qualificationLive");
 };
 }else if(_isdayone) { 
 //BA.debugLineNum = 193;BA.debugLine="If isSt Then";
if (_isst) { 
 //BA.debugLineNum = 194;BA.debugLine="RequestResults(\"dayOneStart\")";
_requestresults("dayOneStart");
 }else {
 //BA.debugLineNum = 196;BA.debugLine="RequestResults(\"dayOneLive\")";
_requestresults("dayOneLive");
 //BA.debugLineNum = 197;BA.debugLine="lblTime.Gravity = Gravity.LEFT";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 198;BA.debugLine="lblTime.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 199;BA.debugLine="lblClass.Gravity = Gravity.RIGHT";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.RIGHT);
 //BA.debugLineNum = 200;BA.debugLine="lblClass.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 };
 }else {
 //BA.debugLineNum = 203;BA.debugLine="If isSt Then";
if (_isst) { 
 //BA.debugLineNum = 204;BA.debugLine="RequestResults(\"dayTwoStart\")";
_requestresults("dayTwoStart");
 }else {
 //BA.debugLineNum = 206;BA.debugLine="RequestResults(\"dayTwoLive\")";
_requestresults("dayTwoLive");
 //BA.debugLineNum = 207;BA.debugLine="lblTime.Gravity = Gravity.LEFT";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 208;BA.debugLine="lblTime.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 209;BA.debugLine="lblClass.Gravity = Gravity.RIGHT";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.RIGHT);
 //BA.debugLineNum = 210;BA.debugLine="lblClass.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 };
 };
 //BA.debugLineNum = 213;BA.debugLine="End Sub";
return "";
}
public static String  _setuplabels() throws Exception{
int _labelwidth = 0;
 //BA.debugLineNum = 215;BA.debugLine="Sub SetupLabels";
 //BA.debugLineNum = 216;BA.debugLine="Dim labelWidth As Int";
_labelwidth = 0;
 //BA.debugLineNum = 217;BA.debugLine="Log(\"Width:\"&pnlHeadersStart.Width)";
anywheresoftware.b4a.keywords.Common.Log("Width:"+BA.NumberToString(mostCurrent._pnlheadersstart.getWidth()));
 //BA.debugLineNum = 218;BA.debugLine="labelWidth = pnlHeadersStart.Width / 5";
_labelwidth = (int) (mostCurrent._pnlheadersstart.getWidth()/(double)5);
 //BA.debugLineNum = 220;BA.debugLine="lblPos.Width = labelWidth / 2";
mostCurrent._lblpos.setWidth((int) (_labelwidth/(double)2));
 //BA.debugLineNum = 221;BA.debugLine="lblNo.Width = labelWidth / 2";
mostCurrent._lblno.setWidth((int) (_labelwidth/(double)2));
 //BA.debugLineNum = 222;BA.debugLine="lblRider.Width = labelWidth * 2";
mostCurrent._lblrider.setWidth((int) (_labelwidth*2));
 //BA.debugLineNum = 223;BA.debugLine="lblClass.Width = labelWidth * 2";
mostCurrent._lblclass.setWidth((int) (_labelwidth*2));
 //BA.debugLineNum = 224;BA.debugLine="lblTime.Width = labelWidth * 1.3";
mostCurrent._lbltime.setWidth((int) (_labelwidth*1.3));
 //BA.debugLineNum = 225;BA.debugLine="lblTotal.Width = labelWidth";
mostCurrent._lbltotal.setWidth(_labelwidth);
 //BA.debugLineNum = 227;BA.debugLine="lblNo.Left = lblPos.Left + lblPos.Width";
mostCurrent._lblno.setLeft((int) (mostCurrent._lblpos.getLeft()+mostCurrent._lblpos.getWidth()));
 //BA.debugLineNum = 228;BA.debugLine="lblRider.Left = lblPos.Left + lblPos.Width";
mostCurrent._lblrider.setLeft((int) (mostCurrent._lblpos.getLeft()+mostCurrent._lblpos.getWidth()));
 //BA.debugLineNum = 229;BA.debugLine="lblClass.Left = lblRider.Left + lblRider.Width";
mostCurrent._lblclass.setLeft((int) (mostCurrent._lblrider.getLeft()+mostCurrent._lblrider.getWidth()));
 //BA.debugLineNum = 230;BA.debugLine="lblTime.Left = lblClass.Left + lblClass.Width";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lblclass.getLeft()+mostCurrent._lblclass.getWidth()));
 //BA.debugLineNum = 232;BA.debugLine="If isQualification Then";
if (_isqualification) { 
 //BA.debugLineNum = 233;BA.debugLine="If isStart Then";
if (_isstart) { 
 //BA.debugLineNum = 234;BA.debugLine="lblPos.Visible = False";
mostCurrent._lblpos.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 235;BA.debugLine="lblNo.Visible = False";
mostCurrent._lblno.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 236;BA.debugLine="lblTotal.Visible = False";
mostCurrent._lbltotal.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 237;BA.debugLine="lblClass.Text = \"CLASS\"";
mostCurrent._lblclass.setText((Object)("CLASS"));
 //BA.debugLineNum = 238;BA.debugLine="lblTime.Text = \"TIME\"";
mostCurrent._lbltime.setText((Object)("TIME"));
 //BA.debugLineNum = 239;BA.debugLine="lblRider.Text = \"NATION\"";
mostCurrent._lblrider.setText((Object)("NATION"));
 //BA.debugLineNum = 240;BA.debugLine="lblTime.Left = lblTime.Left - 20dip";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lbltime.getLeft()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (20))));
 //BA.debugLineNum = 241;BA.debugLine="lblTime.Gravity = Gravity.LEFT";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 242;BA.debugLine="lblTime.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 243;BA.debugLine="lblClass.Gravity = Gravity.LEFT";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 244;BA.debugLine="lblClass.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 }else {
 //BA.debugLineNum = 246;BA.debugLine="lblPos.Visible = True";
mostCurrent._lblpos.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 247;BA.debugLine="lblNo.Visible = False";
mostCurrent._lblno.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 248;BA.debugLine="lblTotal.Visible = False";
mostCurrent._lbltotal.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 249;BA.debugLine="lblClass.Text = \"SCORE\"";
mostCurrent._lblclass.setText((Object)("SCORE"));
 //BA.debugLineNum = 250;BA.debugLine="lblTime.Text = \"TIME\"";
mostCurrent._lbltime.setText((Object)("TIME"));
 //BA.debugLineNum = 251;BA.debugLine="lblRider.Text = \"NATION\"";
mostCurrent._lblrider.setText((Object)("NATION"));
 //BA.debugLineNum = 252;BA.debugLine="lblRider.Width = labelWidth * 3";
mostCurrent._lblrider.setWidth((int) (_labelwidth*3));
 //BA.debugLineNum = 253;BA.debugLine="lblClass.Width = labelWidth";
mostCurrent._lblclass.setWidth(_labelwidth);
 //BA.debugLineNum = 254;BA.debugLine="lblClass.Left = lblRider.Left + lblRider.Width";
mostCurrent._lblclass.setLeft((int) (mostCurrent._lblrider.getLeft()+mostCurrent._lblrider.getWidth()));
 //BA.debugLineNum = 255;BA.debugLine="lblTime.Left = lblClass.Left + lblClass.Width";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lblclass.getLeft()+mostCurrent._lblclass.getWidth()));
 //BA.debugLineNum = 256;BA.debugLine="lblTime.Left = lblTime.Left - 20dip";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lbltime.getLeft()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (20))));
 //BA.debugLineNum = 257;BA.debugLine="lblClass.Gravity = Gravity.LEFT";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 258;BA.debugLine="lblClass.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 259;BA.debugLine="lblTime.Gravity = Gravity.LEFT";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 260;BA.debugLine="lblTime.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 261;BA.debugLine="Log(\"Time top:\"&lblTime.Top)";
anywheresoftware.b4a.keywords.Common.Log("Time top:"+BA.NumberToString(mostCurrent._lbltime.getTop()));
 //BA.debugLineNum = 262;BA.debugLine="Log(\"Time class:\"&lblClass.Top)";
anywheresoftware.b4a.keywords.Common.Log("Time class:"+BA.NumberToString(mostCurrent._lblclass.getTop()));
 };
 }else {
 //BA.debugLineNum = 265;BA.debugLine="If isStart Then";
if (_isstart) { 
 //BA.debugLineNum = 266;BA.debugLine="lblPos.Visible = False";
mostCurrent._lblpos.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 267;BA.debugLine="lblTotal.Visible = False";
mostCurrent._lbltotal.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 268;BA.debugLine="lblClass.Text = \"CLASS\"";
mostCurrent._lblclass.setText((Object)("CLASS"));
 //BA.debugLineNum = 269;BA.debugLine="lblTime.Text = \"TIME\"";
mostCurrent._lbltime.setText((Object)("TIME"));
 //BA.debugLineNum = 270;BA.debugLine="lblRider.Text = \"NATION\"";
mostCurrent._lblrider.setText((Object)("NATION"));
 //BA.debugLineNum = 271;BA.debugLine="lblTime.Gravity = Gravity.LEFT";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 272;BA.debugLine="lblTime.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 273;BA.debugLine="lblClass.Gravity = Gravity.LEFT";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 274;BA.debugLine="lblClass.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lblclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 275;BA.debugLine="lblTime.Left = lblTime.Left - 20dip";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lbltime.getLeft()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (20))));
 }else {
 //BA.debugLineNum = 277;BA.debugLine="lblPos.Visible = True";
mostCurrent._lblpos.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 278;BA.debugLine="lblTotal.Visible = True";
mostCurrent._lbltotal.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 279;BA.debugLine="lblClass.Text = \"LAP SCORES\"";
mostCurrent._lblclass.setText((Object)("LAP SCORES"));
 //BA.debugLineNum = 280;BA.debugLine="lblRider.Text = \"NATION + RIDERS\"";
mostCurrent._lblrider.setText((Object)("NATION + RIDERS"));
 //BA.debugLineNum = 281;BA.debugLine="lblTime.Text = \"PEN\"";
mostCurrent._lbltime.setText((Object)("PEN"));
 //BA.debugLineNum = 282;BA.debugLine="lblRider.Width = labelWidth * 2.2";
mostCurrent._lblrider.setWidth((int) (_labelwidth*2.2));
 //BA.debugLineNum = 283;BA.debugLine="lblClass.Width = labelWidth * 1.3";
mostCurrent._lblclass.setWidth((int) (_labelwidth*1.3));
 //BA.debugLineNum = 284;BA.debugLine="lblClass.Left = lblRider.Left + lblRider.Width";
mostCurrent._lblclass.setLeft((int) (mostCurrent._lblrider.getLeft()+mostCurrent._lblrider.getWidth()));
 //BA.debugLineNum = 285;BA.debugLine="lblTime.Width = labelWidth / 2";
mostCurrent._lbltime.setWidth((int) (_labelwidth/(double)2));
 //BA.debugLineNum = 286;BA.debugLine="lblTotal.Width = labelWidth / 2";
mostCurrent._lbltotal.setWidth((int) (_labelwidth/(double)2));
 //BA.debugLineNum = 287;BA.debugLine="lblTotal.Left = pnlHeadersStart.Width - lblTota";
mostCurrent._lbltotal.setLeft((int) (mostCurrent._pnlheadersstart.getWidth()-mostCurrent._lbltotal.getWidth()));
 //BA.debugLineNum = 288;BA.debugLine="lblTime.Left = lblTotal.Left - lblTime.Width +";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lbltotal.getLeft()-mostCurrent._lbltime.getWidth()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5))));
 //BA.debugLineNum = 289;BA.debugLine="lblTime.Gravity = Gravity.LEFT";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.LEFT);
 //BA.debugLineNum = 290;BA.debugLine="lblTime.Gravity = Gravity.CENTER_VERTICAL";
mostCurrent._lbltime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 291;BA.debugLine="Log(\"Class left:\"&lblClass.Left)";
anywheresoftware.b4a.keywords.Common.Log("Class left:"+BA.NumberToString(mostCurrent._lblclass.getLeft()));
 //BA.debugLineNum = 292;BA.debugLine="Log(\"Class width:\"&lblClass.Width)";
anywheresoftware.b4a.keywords.Common.Log("Class width:"+BA.NumberToString(mostCurrent._lblclass.getWidth()));
 //BA.debugLineNum = 293;BA.debugLine="Log(\"Pen left:\"&lblTime.Left)";
anywheresoftware.b4a.keywords.Common.Log("Pen left:"+BA.NumberToString(mostCurrent._lbltime.getLeft()));
 //BA.debugLineNum = 294;BA.debugLine="Log(\"Pen width:\"&lblTime.Width)";
anywheresoftware.b4a.keywords.Common.Log("Pen width:"+BA.NumberToString(mostCurrent._lbltime.getWidth()));
 };
 };
 //BA.debugLineNum = 298;BA.debugLine="End Sub";
return "";
}
public static String  _sql_nonquerycomplete(boolean _success) throws Exception{
 //BA.debugLineNum = 1349;BA.debugLine="Sub SQL_NonQueryComplete (Success As Boolean)";
 //BA.debugLineNum = 1351;BA.debugLine="If Success = False Then";
if (_success==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 1352;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1353;BA.debugLine="Msgbox(LastException, \"Error!\")";
anywheresoftware.b4a.keywords.Common.Msgbox(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)),"Error!",mostCurrent.activityBA);
 //BA.debugLineNum = 1354;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 1357;BA.debugLine="Select Case queryType";
switch (_querytype) {
case 0: {
 //BA.debugLineNum = 1359;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 break; }
case 1: {
 //BA.debugLineNum = 1361;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 break; }
case 2: {
 //BA.debugLineNum = 1363;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 break; }
case 3: {
 //BA.debugLineNum = 1365;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 break; }
case 4: {
 //BA.debugLineNum = 1367;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 break; }
case 5: {
 //BA.debugLineNum = 1369;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 break; }
}
;
 //BA.debugLineNum = 1371;BA.debugLine="End Sub";
return "";
}
}
