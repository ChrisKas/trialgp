package trialGP.live;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class main extends Activity implements B4AActivity{
	public static main mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "trialGP.live", "trialGP.live.main");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (main).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "trialGP.live", "trialGP.live.main");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "trialGP.live.main", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (main) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (main) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (main) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        Object[] o;
        if (permissions.length > 0)
            o = new Object[] {permissions[0], grantResults[0] == 0};
        else
            o = new Object[] {"", false};
        processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public static String _server = "";
public static anywheresoftware.b4a.objects.collections.List _riderslist = null;
public static anywheresoftware.b4a.objects.collections.List _classeslist = null;
public static String _eventname = "";
public static anywheresoftware.b4a.objects.Timer _splashtimer = null;
public static anywheresoftware.b4a.sql.SQL _sql1 = null;
public static String _dbfilename = "";
public static String _dbfiledir = "";
public anywheresoftware.b4a.objects.PanelWrapper _pnlfooter = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlheader = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlnavigation = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgmainlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgleftlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgrightlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imghome = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgevent = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imglogo = null;
public anywheresoftware.b4a.objects.ScrollViewWrapper _svnavigation = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlsplashpanel = null;
public static String _numofdays = "";
public static int _querytype = 0;
public static boolean _shouldrequestimage = false;
public static boolean _shouldrequestupdate = false;
public trialGP.live.onedayevent _onedayevent = null;
public trialGP.live.desnationsevent _desnationsevent = null;
public trialGP.live.twodaysevent _twodaysevent = null;
public trialGP.live.starter _starter = null;
public trialGP.live.misc _misc = null;
public trialGP.live.httputils2service _httputils2service = null;

public static boolean isAnyActivityVisible() {
    boolean vis = false;
vis = vis | (main.mostCurrent != null);
vis = vis | (onedayevent.mostCurrent != null);
vis = vis | (desnationsevent.mostCurrent != null);
vis = vis | (twodaysevent.mostCurrent != null);
return vis;}
public static String  _activity_create(boolean _firsttime) throws Exception{
anywheresoftware.b4a.objects.AnimationWrapper _fadeeffect = null;
 //BA.debugLineNum = 55;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 57;BA.debugLine="Activity.LoadLayout(\"MainLayout\")";
mostCurrent._activity.LoadLayout("MainLayout",mostCurrent.activityBA);
 //BA.debugLineNum = 60;BA.debugLine="Dim fadeEffect As Animation";
_fadeeffect = new anywheresoftware.b4a.objects.AnimationWrapper();
 //BA.debugLineNum = 61;BA.debugLine="fadeEffect.InitializeAlpha(\"\", 1, 0)";
_fadeeffect.InitializeAlpha(mostCurrent.activityBA,"",(float) (1),(float) (0));
 //BA.debugLineNum = 62;BA.debugLine="fadeEffect.Duration = 3000";
_fadeeffect.setDuration((long) (3000));
 //BA.debugLineNum = 64;BA.debugLine="Misc.HorizontalCenterView(imgLogo, Activity)";
mostCurrent._misc._horizontalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imglogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._activity.getObject())));
 //BA.debugLineNum = 65;BA.debugLine="Misc.VerticalCenterView(imgLogo, Activity)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imglogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._activity.getObject())));
 //BA.debugLineNum = 66;BA.debugLine="imgLogo.Top = imgLogo.Top - ((5 / 100) * Activity";
mostCurrent._imglogo.setTop((int) (mostCurrent._imglogo.getTop()-((5/(double)100)*mostCurrent._activity.getHeight())));
 //BA.debugLineNum = 68;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 69;BA.debugLine="pnlSplashPanel.Visible = True";
mostCurrent._pnlsplashpanel.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 72;BA.debugLine="pnlHeader.Enabled = False";
mostCurrent._pnlheader.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 73;BA.debugLine="pnlHeader.Visible = False";
mostCurrent._pnlheader.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 74;BA.debugLine="pnlFooter.Enabled = False";
mostCurrent._pnlfooter.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 75;BA.debugLine="pnlFooter.Visible = False";
mostCurrent._pnlfooter.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 76;BA.debugLine="svNavigation.Enabled = False";
mostCurrent._svnavigation.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 77;BA.debugLine="svNavigation.Visible = False";
mostCurrent._svnavigation.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 78;BA.debugLine="imgEvent.Enabled = False";
mostCurrent._imgevent.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 79;BA.debugLine="imgEvent.Visible = False";
mostCurrent._imgevent.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 82;BA.debugLine="splashTimer.Initialize(\"splashTimer\", 3000)";
_splashtimer.Initialize(processBA,"splashTimer",(long) (3000));
 //BA.debugLineNum = 83;BA.debugLine="splashTimer.Enabled = True";
_splashtimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 84;BA.debugLine="fadeEffect.Start(imgLogo)";
_fadeeffect.Start((android.view.View)(mostCurrent._imglogo.getObject()));
 //BA.debugLineNum = 86;BA.debugLine="If File.Exists(DBFileDir, DBFileName) = False Th";
if (anywheresoftware.b4a.keywords.Common.File.Exists(_dbfiledir,_dbfilename)==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 88;BA.debugLine="If File.Exists(File.DirAssets, DBFileName) Then";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_dbfilename)) { 
 //BA.debugLineNum = 89;BA.debugLine="File.Copy(File.DirAssets, DBFileName, DBFileDi";
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_dbfilename,_dbfiledir,_dbfilename);
 }else {
 //BA.debugLineNum = 91;BA.debugLine="Msgbox(\"No db file found!\", \"Error!\")";
anywheresoftware.b4a.keywords.Common.Msgbox("No db file found!","Error!",mostCurrent.activityBA);
 //BA.debugLineNum = 92;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 };
 //BA.debugLineNum = 95;BA.debugLine="SQL1.Initialize(DBFileDir, DBFileName, False)";
_sql1.Initialize(_dbfiledir,_dbfilename,anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 97;BA.debugLine="SQL1.Initialize(DBFileDir, DBFileName, False)";
_sql1.Initialize(_dbfiledir,_dbfilename,anywheresoftware.b4a.keywords.Common.False);
 };
 };
 //BA.debugLineNum = 101;BA.debugLine="Misc.HorizontalCenterView(imgMainLogo, pnlHeader)";
mostCurrent._misc._horizontalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgmainlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 102;BA.debugLine="Misc.HorizontalCenterView(imgEvent, Activity)";
mostCurrent._misc._horizontalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgevent.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._activity.getObject())));
 //BA.debugLineNum = 104;BA.debugLine="Misc.VerticalCenterView(imgLeftLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgleftlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 105;BA.debugLine="Misc.VerticalCenterView(imgRightLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgrightlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 106;BA.debugLine="Misc.VerticalCenterView(imgMainLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgmainlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 107;BA.debugLine="Misc.VerticalCenterView(imgHome, pnlFooter)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imghome.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlfooter.getObject())));
 //BA.debugLineNum = 109;BA.debugLine="imgEvent.Height = imgEvent.Width * 176/390";
mostCurrent._imgevent.setHeight((int) (mostCurrent._imgevent.getWidth()*176/(double)390));
 //BA.debugLineNum = 110;BA.debugLine="svNavigation.Top = imgEvent.Top + imgEvent.Height";
mostCurrent._svnavigation.setTop((int) (mostCurrent._imgevent.getTop()+mostCurrent._imgevent.getHeight()));
 //BA.debugLineNum = 111;BA.debugLine="svNavigation.Height = pnlFooter.Top - svNavigatio";
mostCurrent._svnavigation.setHeight((int) (mostCurrent._pnlfooter.getTop()-mostCurrent._svnavigation.getTop()));
 //BA.debugLineNum = 113;BA.debugLine="ridersList.Initialize";
_riderslist.Initialize();
 //BA.debugLineNum = 114;BA.debugLine="classesList.Initialize";
_classeslist.Initialize();
 //BA.debugLineNum = 116;BA.debugLine="RequestEventInfo";
_requesteventinfo();
 //BA.debugLineNum = 117;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 125;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 127;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 119;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 120;BA.debugLine="If shouldRequestUpdate Then";
if (_shouldrequestupdate) { 
 //BA.debugLineNum = 121;BA.debugLine="RequestEventInfo";
_requesteventinfo();
 };
 //BA.debugLineNum = 123;BA.debugLine="End Sub";
return "";
}
public static String  _btnmainmenu_click() throws Exception{
anywheresoftware.b4a.objects.LabelWrapper _sndr = null;
anywheresoftware.b4a.objects.IntentWrapper _i = null;
 //BA.debugLineNum = 465;BA.debugLine="Sub btnMainMenu_Click";
 //BA.debugLineNum = 466;BA.debugLine="Dim sndr As Label";
_sndr = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 467;BA.debugLine="sndr = Sender";
_sndr.setObject((android.widget.TextView)(anywheresoftware.b4a.keywords.Common.Sender(mostCurrent.activityBA)));
 //BA.debugLineNum = 468;BA.debugLine="Private i As Intent";
_i = new anywheresoftware.b4a.objects.IntentWrapper();
 //BA.debugLineNum = 469;BA.debugLine="i.Initialize(i.ACTION_VIEW, sndr.Tag)";
_i.Initialize(_i.ACTION_VIEW,BA.ObjectToString(_sndr.getTag()));
 //BA.debugLineNum = 471;BA.debugLine="StartActivity(i)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(_i.getObject()));
 //BA.debugLineNum = 472;BA.debugLine="End Sub";
return "";
}
public static String  _displayeventinfo() throws Exception{
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bgdark = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bglight = null;
int _buttonheight = 0;
int _i = 0;
anywheresoftware.b4a.objects.LabelWrapper _btn = null;
 //BA.debugLineNum = 225;BA.debugLine="Sub DisplayEventInfo";
 //BA.debugLineNum = 226;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 227;BA.debugLine="crs = SQL1.ExecQuery(\"SELECT trgCaption1, trgCapt";
_crs.setObject((android.database.Cursor)(_sql1.ExecQuery("SELECT trgCaption1, trgCaption2, trgCaption3, trgCaption4, trgCaption5, trgCaption6, trgCaption7, trgCaption8, trgUrl1, trgUrl2, trgUrl3, trgUrl4, trgUrl5, trgUrl6, trgUrl7, trgUrl8 FROM trGeneral")));
 //BA.debugLineNum = 230;BA.debugLine="If crs.RowCount = 0 Then";
if (_crs.getRowCount()==0) { 
 //BA.debugLineNum = 231;BA.debugLine="RequestMainImage";
_requestmainimage();
 //BA.debugLineNum = 232;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 235;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 237;BA.debugLine="pnlNavigation = svNavigation.Panel";
mostCurrent._pnlnavigation = mostCurrent._svnavigation.getPanel();
 //BA.debugLineNum = 239;BA.debugLine="Dim bgDark As BitmapDrawable";
_bgdark = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 240;BA.debugLine="Dim bgLight As BitmapDrawable";
_bglight = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 242;BA.debugLine="bgDark.Initialize(LoadBitmap(File.DirAssets, \"Str";
_bgdark.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"StripesDark.jpg").getObject()));
 //BA.debugLineNum = 243;BA.debugLine="bgLight.Initialize(LoadBitmap(File.DirAssets, \"St";
_bglight.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"StripesLight.jpg").getObject()));
 //BA.debugLineNum = 245;BA.debugLine="Dim buttonHeight As Int";
_buttonheight = 0;
 //BA.debugLineNum = 246;BA.debugLine="buttonHeight = ((svNavigation.Height - 8dip) / 8)";
_buttonheight = (int) (((mostCurrent._svnavigation.getHeight()-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (8)))/(double)8));
 //BA.debugLineNum = 247;BA.debugLine="If buttonHeight < 50 Then";
if (_buttonheight<50) { 
 //BA.debugLineNum = 248;BA.debugLine="buttonHeight = 50";
_buttonheight = (int) (50);
 };
 //BA.debugLineNum = 252;BA.debugLine="For i = 0 To 7";
{
final int step18 = 1;
final int limit18 = (int) (7);
for (_i = (int) (0) ; (step18 > 0 && _i <= limit18) || (step18 < 0 && _i >= limit18); _i = ((int)(0 + _i + step18)) ) {
 //BA.debugLineNum = 253;BA.debugLine="Dim btn As Label";
_btn = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 254;BA.debugLine="btn.Initialize(\"btnMainMenu\")";
_btn.Initialize(mostCurrent.activityBA,"btnMainMenu");
 //BA.debugLineNum = 255;BA.debugLine="Select Case i";
switch (_i) {
case 0: {
 //BA.debugLineNum = 257;BA.debugLine="btn.Tag = crs.GetString(\"trgUrl1\")";
_btn.setTag((Object)(_crs.GetString("trgUrl1")));
 //BA.debugLineNum = 258;BA.debugLine="btn.Text = crs.GetString(\"trgCaption1\")";
_btn.setText((Object)(_crs.GetString("trgCaption1")));
 break; }
case 1: {
 //BA.debugLineNum = 260;BA.debugLine="btn.Tag = crs.GetString(\"trgUrl2\")";
_btn.setTag((Object)(_crs.GetString("trgUrl2")));
 //BA.debugLineNum = 261;BA.debugLine="btn.Text = crs.GetString(\"trgCaption2\")";
_btn.setText((Object)(_crs.GetString("trgCaption2")));
 break; }
case 2: {
 //BA.debugLineNum = 263;BA.debugLine="btn.Tag = crs.GetString(\"trgUrl3\")";
_btn.setTag((Object)(_crs.GetString("trgUrl3")));
 //BA.debugLineNum = 264;BA.debugLine="btn.Text = crs.GetString(\"trgCaption3\")";
_btn.setText((Object)(_crs.GetString("trgCaption3")));
 break; }
case 3: {
 //BA.debugLineNum = 266;BA.debugLine="btn.Tag = crs.GetString(\"trgUrl4\")";
_btn.setTag((Object)(_crs.GetString("trgUrl4")));
 //BA.debugLineNum = 267;BA.debugLine="btn.Text = crs.GetString(\"trgCaption4\")";
_btn.setText((Object)(_crs.GetString("trgCaption4")));
 break; }
case 4: {
 //BA.debugLineNum = 269;BA.debugLine="btn.Tag = crs.GetString(\"trgUrl5\")";
_btn.setTag((Object)(_crs.GetString("trgUrl5")));
 //BA.debugLineNum = 270;BA.debugLine="btn.Text = crs.GetString(\"trgCaption5\")";
_btn.setText((Object)(_crs.GetString("trgCaption5")));
 break; }
case 5: {
 //BA.debugLineNum = 272;BA.debugLine="btn.Tag = crs.GetString(\"trgUrl6\")";
_btn.setTag((Object)(_crs.GetString("trgUrl6")));
 //BA.debugLineNum = 273;BA.debugLine="btn.Text = crs.GetString(\"trgCaption6\")";
_btn.setText((Object)(_crs.GetString("trgCaption6")));
 break; }
case 6: {
 //BA.debugLineNum = 275;BA.debugLine="btn.Tag = crs.GetString(\"trgUrl7\")";
_btn.setTag((Object)(_crs.GetString("trgUrl7")));
 //BA.debugLineNum = 276;BA.debugLine="btn.Text = crs.GetString(\"trgCaption7\")";
_btn.setText((Object)(_crs.GetString("trgCaption7")));
 break; }
case 7: {
 //BA.debugLineNum = 278;BA.debugLine="btn.Tag = crs.GetString(\"trgUrl8\")";
_btn.setTag((Object)(_crs.GetString("trgUrl8")));
 //BA.debugLineNum = 279;BA.debugLine="btn.Text = crs.GetString(\"trgCaption8\")";
_btn.setText((Object)(_crs.GetString("trgCaption8")));
 break; }
}
;
 //BA.debugLineNum = 282;BA.debugLine="btn.TextSize = 30";
_btn.setTextSize((float) (30));
 //BA.debugLineNum = 283;BA.debugLine="btn.Gravity = Gravity.CENTER";
_btn.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 284;BA.debugLine="btn.Typeface = Typeface.LoadFromAssets(\"bebasneu";
_btn.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("bebasneue-webfont.ttf"));
 //BA.debugLineNum = 285;BA.debugLine="Misc.SetTextShadow(btn, 5, -5, 5,Colors.Black)";
mostCurrent._misc._settextshadow(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_btn.getObject())),(float) (5),(float) (-5),(float) (5),anywheresoftware.b4a.keywords.Common.Colors.Black);
 //BA.debugLineNum = 287;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 288;BA.debugLine="btn.Background = bgLight";
_btn.setBackground((android.graphics.drawable.Drawable)(_bglight.getObject()));
 //BA.debugLineNum = 289;BA.debugLine="btn.TextColor = Colors.RGB(255, 255, 255)";
_btn.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 }else {
 //BA.debugLineNum = 291;BA.debugLine="btn.Background = bgDark";
_btn.setBackground((android.graphics.drawable.Drawable)(_bgdark.getObject()));
 //BA.debugLineNum = 292;BA.debugLine="btn.TextColor = Colors.RGB(34, 180, 231)";
_btn.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 };
 //BA.debugLineNum = 294;BA.debugLine="pnlNavigation.AddView(btn, 0dip, 1dip + (i * but";
mostCurrent._pnlnavigation.AddView((android.view.View)(_btn.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (0)),(int) (anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1))+(_i*_buttonheight)+(_i*anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1)))),mostCurrent._svnavigation.getWidth(),_buttonheight);
 }
};
 //BA.debugLineNum = 297;BA.debugLine="pnlNavigation.Height = 8 * (buttonHeight + 1dip)";
mostCurrent._pnlnavigation.setHeight((int) (8*(_buttonheight+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1)))));
 //BA.debugLineNum = 300;BA.debugLine="If shouldRequestImage Then";
if (_shouldrequestimage) { 
 //BA.debugLineNum = 301;BA.debugLine="RequestMainImage";
_requestmainimage();
 }else {
 //BA.debugLineNum = 303;BA.debugLine="DisplayMainImage";
_displaymainimage();
 };
 //BA.debugLineNum = 305;BA.debugLine="End Sub";
return "";
}
public static String  _displaymainimage() throws Exception{
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _b = null;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
 //BA.debugLineNum = 409;BA.debugLine="Sub DisplayMainImage";
 //BA.debugLineNum = 410;BA.debugLine="Dim b As Bitmap";
_b = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 411;BA.debugLine="If File.Exists(File.DirInternal, \"mainlogo.jpg\")";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"mainlogo.jpg")) { 
 //BA.debugLineNum = 412;BA.debugLine="b.Initialize(File.DirInternal, \"mainlogo.jpg\")";
_b.Initialize(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"mainlogo.jpg");
 }else {
 //BA.debugLineNum = 414;BA.debugLine="b.Initialize(File.DirAssets, \"mainlogo.jpg\")";
_b.Initialize(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"mainlogo.jpg");
 };
 //BA.debugLineNum = 416;BA.debugLine="imgEvent.SetBackgroundImage(b)";
mostCurrent._imgevent.SetBackgroundImage((android.graphics.Bitmap)(_b.getObject()));
 //BA.debugLineNum = 419;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 420;BA.debugLine="crs = SQL1.ExecQuery(\"SELECT treiDays, treiEventN";
_crs.setObject((android.database.Cursor)(_sql1.ExecQuery("SELECT treiDays, treiEventName FROM trEventInfo")));
 //BA.debugLineNum = 423;BA.debugLine="If crs.RowCount = 0 Then";
if (_crs.getRowCount()==0) { 
 //BA.debugLineNum = 424;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 427;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 429;BA.debugLine="numOfDays = crs.GetString(\"treiDays\")";
mostCurrent._numofdays = _crs.GetString("treiDays");
 //BA.debugLineNum = 430;BA.debugLine="eventName = crs.GetString(\"treiEventName\")";
_eventname = _crs.GetString("treiEventName");
 //BA.debugLineNum = 431;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 31;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 34;BA.debugLine="Private pnlFooter As Panel";
mostCurrent._pnlfooter = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 35;BA.debugLine="Private pnlHeader As Panel";
mostCurrent._pnlheader = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 36;BA.debugLine="Private pnlNavigation As Panel";
mostCurrent._pnlnavigation = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 38;BA.debugLine="Private imgMainLogo As ImageView";
mostCurrent._imgmainlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Private imgLeftLogo As ImageView";
mostCurrent._imgleftlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 40;BA.debugLine="Private imgRightLogo As ImageView";
mostCurrent._imgrightlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 41;BA.debugLine="Private imgHome As ImageView";
mostCurrent._imghome = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 42;BA.debugLine="Private imgEvent As ImageView";
mostCurrent._imgevent = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 43;BA.debugLine="Private imgLogo As ImageView";
mostCurrent._imglogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 45;BA.debugLine="Private svNavigation As ScrollView";
mostCurrent._svnavigation = new anywheresoftware.b4a.objects.ScrollViewWrapper();
 //BA.debugLineNum = 46;BA.debugLine="Private pnlNavigation As Panel";
mostCurrent._pnlnavigation = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 47;BA.debugLine="Private pnlSplashPanel As Panel";
mostCurrent._pnlsplashpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 49;BA.debugLine="Dim numOfDays As String";
mostCurrent._numofdays = "";
 //BA.debugLineNum = 50;BA.debugLine="Dim queryType As Int";
_querytype = 0;
 //BA.debugLineNum = 51;BA.debugLine="Dim shouldRequestImage As Boolean = False";
_shouldrequestimage = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 52;BA.debugLine="Dim shouldRequestUpdate As Boolean = False";
_shouldrequestupdate = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 53;BA.debugLine="End Sub";
return "";
}
public static String  _imgevent_click() throws Exception{
 //BA.debugLineNum = 433;BA.debugLine="Sub imgEvent_Click";
 //BA.debugLineNum = 434;BA.debugLine="Select Case numOfDays";
switch (BA.switchObjectToInt(mostCurrent._numofdays,"1","2","3")) {
case 0: {
 //BA.debugLineNum = 436;BA.debugLine="StartActivity(OneDayEvent)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._onedayevent.getObject()));
 break; }
case 1: {
 //BA.debugLineNum = 438;BA.debugLine="StartActivity(TwoDaysEvent)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._twodaysevent.getObject()));
 break; }
case 2: {
 //BA.debugLineNum = 440;BA.debugLine="StartActivity(DesNationsEvent)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._desnationsevent.getObject()));
 break; }
}
;
 //BA.debugLineNum = 442;BA.debugLine="End Sub";
return "";
}
public static String  _jobdone(trialGP.live.httpjob _job) throws Exception{
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _out = null;
 //BA.debugLineNum = 369;BA.debugLine="Sub JobDone (Job As HttpJob)";
 //BA.debugLineNum = 371;BA.debugLine="If Job.Success = False Then";
if (_job._success==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 372;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"Job0","Job1","Job2")) {
case 0: {
 break; }
case 1: {
 //BA.debugLineNum = 376;BA.debugLine="shouldRequestUpdate = True";
_shouldrequestupdate = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 377;BA.debugLine="DisplayEventInfo";
_displayeventinfo();
 break; }
case 2: {
 //BA.debugLineNum = 379;BA.debugLine="DisplayMainImage";
_displaymainimage();
 break; }
}
;
 }else {
 //BA.debugLineNum = 383;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"Job0","Job1","Job2")) {
case 0: {
 //BA.debugLineNum = 385;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 }else {
 //BA.debugLineNum = 388;BA.debugLine="ParseQualificationStart(Job.GetString)";
_parsequalificationstart(_job._getstring());
 };
 break; }
case 1: {
 //BA.debugLineNum = 391;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 392;BA.debugLine="shouldRequestUpdate = True";
_shouldrequestupdate = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 393;BA.debugLine="DisplayEventInfo";
_displayeventinfo();
 }else {
 //BA.debugLineNum = 395;BA.debugLine="shouldRequestUpdate = False";
_shouldrequestupdate = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 396;BA.debugLine="ParseEventInfo(Job.GetString)";
_parseeventinfo(_job._getstring());
 };
 break; }
case 2: {
 //BA.debugLineNum = 399;BA.debugLine="Dim out As OutputStream = File.OpenOutput(Fil";
_out = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
_out = anywheresoftware.b4a.keywords.Common.File.OpenOutput(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"mainlogo.jpg",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 400;BA.debugLine="File.Copy2(Job.GetInputStream, out)";
anywheresoftware.b4a.keywords.Common.File.Copy2((java.io.InputStream)(_job._getinputstream().getObject()),(java.io.OutputStream)(_out.getObject()));
 //BA.debugLineNum = 401;BA.debugLine="out.Close";
_out.Close();
 //BA.debugLineNum = 403;BA.debugLine="DisplayMainImage";
_displaymainimage();
 break; }
}
;
 };
 //BA.debugLineNum = 406;BA.debugLine="Job.Release";
_job._release();
 //BA.debugLineNum = 407;BA.debugLine="End Sub";
return "";
}
public static String  _parseeventinfo(String _s) throws Exception{
boolean _success = false;
anywheresoftware.b4a.objects.collections.JSONParser _jpar = null;
anywheresoftware.b4a.objects.collections.Map _m = null;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
 //BA.debugLineNum = 176;BA.debugLine="Sub ParseEventInfo(s As String)";
 //BA.debugLineNum = 177;BA.debugLine="Dim success As Boolean = False";
_success = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 179;BA.debugLine="If s = \"\" Then";
if ((_s).equals("")) { 
 //BA.debugLineNum = 180;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 184;BA.debugLine="Dim jPar As JSONParser";
_jpar = new anywheresoftware.b4a.objects.collections.JSONParser();
 //BA.debugLineNum = 185;BA.debugLine="Dim m As Map";
_m = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 186;BA.debugLine="jPar.Initialize(s)";
_jpar.Initialize(_s);
 //BA.debugLineNum = 187;BA.debugLine="m = jPar.NextObject";
_m = _jpar.NextObject();
 //BA.debugLineNum = 190;BA.debugLine="If m.Get(\"Result\") <> \"0\" Then";
if ((_m.Get((Object)("Result"))).equals((Object)("0")) == false) { 
 //BA.debugLineNum = 191;BA.debugLine="DisplayEventInfo";
_displayeventinfo();
 //BA.debugLineNum = 192;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 195;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 197;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 198;BA.debugLine="crs = SQL1.ExecQuery(\"SELECT trgUpdatedOn FROM tr";
_crs.setObject((android.database.Cursor)(_sql1.ExecQuery("SELECT trgUpdatedOn FROM trGeneral")));
 //BA.debugLineNum = 199;BA.debugLine="If crs.RowCount = 0 Then";
if (_crs.getRowCount()==0) { 
 //BA.debugLineNum = 200;BA.debugLine="shouldRequestImage = True";
_shouldrequestimage = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 202;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 203;BA.debugLine="Log(\"Found trgUpdatedOn: \" & crs.GetString(\"trgU";
anywheresoftware.b4a.keywords.Common.Log("Found trgUpdatedOn: "+_crs.GetString("trgUpdatedOn"));
 //BA.debugLineNum = 204;BA.debugLine="If crs.GetString(\"trgUpdatedOn\") = Null Then";
if (_crs.GetString("trgUpdatedOn")== null) { 
 //BA.debugLineNum = 205;BA.debugLine="shouldRequestImage = True";
_shouldrequestimage = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 207;BA.debugLine="Log(\"DATE LAST UPDATED:\" & crs.GetString(\"trgUp";
anywheresoftware.b4a.keywords.Common.Log("DATE LAST UPDATED:"+_crs.GetString("trgUpdatedOn")+"DATE OF IMAGE:"+BA.ObjectToString(_m.Get((Object)("imageTime"))));
 //BA.debugLineNum = 208;BA.debugLine="If crs.GetString(\"trgUpdatedOn\") <> m.Get(\"imag";
if ((_crs.GetString("trgUpdatedOn")).equals(BA.ObjectToString(_m.Get((Object)("imageTime")))) == false) { 
 //BA.debugLineNum = 209;BA.debugLine="shouldRequestImage = True";
_shouldrequestimage = anywheresoftware.b4a.keywords.Common.True;
 };
 };
 };
 //BA.debugLineNum = 214;BA.debugLine="SQL1.AddNonQueryToBatch(\"INSERT INTO trEventInfo";
_sql1.AddNonQueryToBatch("INSERT INTO trEventInfo (treiUpdatedOn, treiDays, treiEventName) VALUES (?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_m.Get((Object)("mtime")),_m.Get((Object)("days")),_m.Get((Object)("title"))}));
 //BA.debugLineNum = 215;BA.debugLine="SQL1.AddNonQueryToBatch(\"INSERT INTO trGeneral (t";
_sql1.AddNonQueryToBatch("INSERT INTO trGeneral (trgImageName, trgCaption1, trgCaption2, trgCaption3, trgCaption4, trgCaption5, trgCaption6, trgCaption7, trgCaption8, trgUrl1, trgUrl2, trgUrl3, trgUrl4, trgUrl5, trgUrl6, trgUrl7, trgUrl8, trgUrl12, trgUpdatedOn) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_m.Get((Object)("imageName")),_m.Get((Object)("caption1")),_m.Get((Object)("caption2")),_m.Get((Object)("caption3")),_m.Get((Object)("caption4")),_m.Get((Object)("caption5")),_m.Get((Object)("caption6")),_m.Get((Object)("caption7")),_m.Get((Object)("caption8")),_m.Get((Object)("url1")),_m.Get((Object)("url2")),_m.Get((Object)("url3")),_m.Get((Object)("url4")),_m.Get((Object)("url5")),_m.Get((Object)("url6")),_m.Get((Object)("url7")),_m.Get((Object)("url8")),_m.Get((Object)("refreshTime")),_m.Get((Object)("imageTime"))}));
 //BA.debugLineNum = 217;BA.debugLine="If success Then";
if (_success) { 
 //BA.debugLineNum = 218;BA.debugLine="SQL1.ExecNonQuery(\"DELETE FROM trEventInfo\")";
_sql1.ExecNonQuery("DELETE FROM trEventInfo");
 //BA.debugLineNum = 219;BA.debugLine="queryType = 1";
_querytype = (int) (1);
 //BA.debugLineNum = 220;BA.debugLine="SQL1.ExecNonQuery(\"DELETE FROM trGeneral\")";
_sql1.ExecNonQuery("DELETE FROM trGeneral");
 //BA.debugLineNum = 221;BA.debugLine="SQL1.ExecNonQueryBatch(\"SQL\")";
_sql1.ExecNonQueryBatch(processBA,"SQL");
 };
 //BA.debugLineNum = 223;BA.debugLine="End Sub";
return "";
}
public static String  _parsequalificationstart(String _s) throws Exception{
boolean _success = false;
anywheresoftware.b4a.objects.collections.JSONParser _jpar = null;
anywheresoftware.b4a.objects.collections.Map _m = null;
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.collections.Map _tmpmap = null;
 //BA.debugLineNum = 307;BA.debugLine="Sub ParseQualificationStart(s As String)";
 //BA.debugLineNum = 308;BA.debugLine="Dim success As Boolean = False";
_success = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 310;BA.debugLine="If s = \"\" Then";
if ((_s).equals("")) { 
 //BA.debugLineNum = 311;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 314;BA.debugLine="ridersList.Initialize";
_riderslist.Initialize();
 //BA.debugLineNum = 317;BA.debugLine="Dim jPar As JSONParser";
_jpar = new anywheresoftware.b4a.objects.collections.JSONParser();
 //BA.debugLineNum = 318;BA.debugLine="Dim m As Map";
_m = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 319;BA.debugLine="jPar.Initialize(s)";
_jpar.Initialize(_s);
 //BA.debugLineNum = 320;BA.debugLine="m = jPar.NextObject";
_m = _jpar.NextObject();
 //BA.debugLineNum = 323;BA.debugLine="If m.Get(\"Result\") <> \"0\" Then";
if ((_m.Get((Object)("Result"))).equals((Object)("0")) == false) { 
 //BA.debugLineNum = 324;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 328;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 329;BA.debugLine="For i = 0 To n";
{
final int step14 = 1;
final int limit14 = _n;
for (_i = (int) (0) ; (step14 > 0 && _i <= limit14) || (step14 < 0 && _i >= limit14); _i = ((int)(0 + _i + step14)) ) {
 //BA.debugLineNum = 331;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 332;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 333;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 }else {
 //BA.debugLineNum = 337;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 338;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 339;BA.debugLine="SQL1.AddNonQueryToBatch(\"INSERT INTO trRiders (";
_sql1.AddNonQueryToBatch("INSERT INTO trRiders (trrCode, trrSurname, trrClass, trrTeam, trrName, trrNationality) VALUES (?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("surname")),_tmpmap.Get((Object)("motoClass")),_tmpmap.Get((Object)("team")),_tmpmap.Get((Object)("name")),_tmpmap.Get((Object)("nationality"))}));
 //BA.debugLineNum = 340;BA.debugLine="SQL1.AddNonQueryToBatch(\"INSERT INTO trQuStart";
_sql1.AddNonQueryToBatch("INSERT INTO trQuStart (trqsCode, trqsPosition, trqstime) VALUES (?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time"))}));
 };
 }
};
 //BA.debugLineNum = 345;BA.debugLine="If success Then";
if (_success) { 
 //BA.debugLineNum = 346;BA.debugLine="queryType = 2";
_querytype = (int) (2);
 //BA.debugLineNum = 347;BA.debugLine="SQL1.ExecNonQuery(\"DELETE FROM trRiders\")";
_sql1.ExecNonQuery("DELETE FROM trRiders");
 //BA.debugLineNum = 348;BA.debugLine="SQL1.ExecNonQuery(\"DELETE FROM trQuStart\")";
_sql1.ExecNonQuery("DELETE FROM trQuStart");
 //BA.debugLineNum = 349;BA.debugLine="SQL1.ExecNonQueryBatch(\"SQL\")";
_sql1.ExecNonQueryBatch(processBA,"SQL");
 }else {
 //BA.debugLineNum = 351;BA.debugLine="Msgbox(\"Error while updating db\", \"Error!\")";
anywheresoftware.b4a.keywords.Common.Msgbox("Error while updating db","Error!",mostCurrent.activityBA);
 //BA.debugLineNum = 352;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 354;BA.debugLine="End Sub";
return "";
}

public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main._process_globals();
onedayevent._process_globals();
desnationsevent._process_globals();
twodaysevent._process_globals();
starter._process_globals();
misc._process_globals();
httputils2service._process_globals();
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 15;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 18;BA.debugLine="Dim server As String = \"https://ws.rowega.net:807";
_server = "https://ws.rowega.net:8076";
 //BA.debugLineNum = 20;BA.debugLine="Dim ridersList As List";
_riderslist = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 21;BA.debugLine="Dim classesList As List";
_classeslist = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 22;BA.debugLine="Dim eventName As String";
_eventname = "";
 //BA.debugLineNum = 24;BA.debugLine="Dim splashTimer As Timer";
_splashtimer = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 26;BA.debugLine="Dim SQL1 As SQL";
_sql1 = new anywheresoftware.b4a.sql.SQL();
 //BA.debugLineNum = 27;BA.debugLine="Dim DBFileName As String       : DBFileName = \"Tr";
_dbfilename = "";
 //BA.debugLineNum = 27;BA.debugLine="Dim DBFileName As String       : DBFileName = \"Tr";
_dbfilename = "TrialGP.sqlite";
 //BA.debugLineNum = 28;BA.debugLine="Dim DBFileDir As String        : DBFileDir = File";
_dbfiledir = "";
 //BA.debugLineNum = 28;BA.debugLine="Dim DBFileDir As String        : DBFileDir = File";
_dbfiledir = anywheresoftware.b4a.keywords.Common.File.getDirInternal();
 //BA.debugLineNum = 29;BA.debugLine="End Sub";
return "";
}
public static String  _requesteventinfo() throws Exception{
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
String _datestring = "";
trialGP.live.httpjob _job = null;
 //BA.debugLineNum = 149;BA.debugLine="Sub RequestEventInfo";
 //BA.debugLineNum = 151;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 152;BA.debugLine="crs = SQL1.ExecQuery(\"SELECT treiUpdatedOn FROM t";
_crs.setObject((android.database.Cursor)(_sql1.ExecQuery("SELECT treiUpdatedOn FROM trEventInfo")));
 //BA.debugLineNum = 154;BA.debugLine="Dim dateString As String = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 //BA.debugLineNum = 156;BA.debugLine="If crs.RowCount = 0 Then";
if (_crs.getRowCount()==0) { 
 //BA.debugLineNum = 157;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 }else {
 //BA.debugLineNum = 159;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 160;BA.debugLine="dateString = crs.GetString(\"treiUpdatedOn\")";
_datestring = _crs.GetString("treiUpdatedOn");
 };
 //BA.debugLineNum = 164;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 165;BA.debugLine="Job.Initialize(\"Job1\", Me)";
_job._initialize(processBA,"Job1",main.getObject());
 //BA.debugLineNum = 166;BA.debugLine="Job.PostString(server & \"/IosEventInfo?date=\" & d";
_job._poststring(_server+"/IosEventInfo?date="+_datestring,"");
 //BA.debugLineNum = 167;BA.debugLine="End Sub";
return "";
}
public static String  _requestmainimage() throws Exception{
trialGP.live.httpjob _job = null;
 //BA.debugLineNum = 169;BA.debugLine="Sub RequestMainImage";
 //BA.debugLineNum = 170;BA.debugLine="Log(\"Requested Image\")";
anywheresoftware.b4a.keywords.Common.Log("Requested Image");
 //BA.debugLineNum = 171;BA.debugLine="Dim job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 172;BA.debugLine="job.Initialize(\"Job2\", Me)";
_job._initialize(processBA,"Job2",main.getObject());
 //BA.debugLineNum = 173;BA.debugLine="job.Download(server & \"/GetMainImage?name=mainlog";
_job._download(_server+"/GetMainImage?name=mainlogo.jpg");
 //BA.debugLineNum = 174;BA.debugLine="End Sub";
return "";
}
public static String  _splashtimer_tick() throws Exception{
 //BA.debugLineNum = 444;BA.debugLine="Sub splashTimer_tick";
 //BA.debugLineNum = 446;BA.debugLine="If SQL1.IsInitialized Then";
if (_sql1.IsInitialized()) { 
 //BA.debugLineNum = 448;BA.debugLine="pnlSplashPanel.Enabled = False";
mostCurrent._pnlsplashpanel.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 449;BA.debugLine="pnlSplashPanel.Visible = False";
mostCurrent._pnlsplashpanel.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 450;BA.debugLine="splashTimer.Enabled = False";
_splashtimer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 451;BA.debugLine="pnlHeader.Enabled = True";
mostCurrent._pnlheader.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 452;BA.debugLine="pnlHeader.Visible = True";
mostCurrent._pnlheader.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 453;BA.debugLine="pnlFooter.Enabled = True";
mostCurrent._pnlfooter.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 454;BA.debugLine="pnlFooter.Visible = True";
mostCurrent._pnlfooter.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 455;BA.debugLine="svNavigation.Enabled = True";
mostCurrent._svnavigation.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 456;BA.debugLine="svNavigation.Visible = True";
mostCurrent._svnavigation.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 457;BA.debugLine="imgEvent.Enabled = True";
mostCurrent._imgevent.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 458;BA.debugLine="imgEvent.Visible = True";
mostCurrent._imgevent.setVisible(anywheresoftware.b4a.keywords.Common.True);
 }else {
 //BA.debugLineNum = 461;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 };
 //BA.debugLineNum = 463;BA.debugLine="End Sub";
return "";
}
public static String  _sql_nonquerycomplete(boolean _success) throws Exception{
 //BA.debugLineNum = 356;BA.debugLine="Sub SQL_NonQueryComplete (Success As Boolean)";
 //BA.debugLineNum = 358;BA.debugLine="If Success = False Then";
if (_success==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 359;BA.debugLine="Msgbox(LastException, \"Error!\")";
anywheresoftware.b4a.keywords.Common.Msgbox(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)),"Error!",mostCurrent.activityBA);
 //BA.debugLineNum = 360;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 363;BA.debugLine="Select Case queryType";
switch (_querytype) {
case 1: {
 //BA.debugLineNum = 365;BA.debugLine="DisplayEventInfo";
_displayeventinfo();
 break; }
}
;
 //BA.debugLineNum = 367;BA.debugLine="End Sub";
return "";
}
}
