﻿Type=Class
Version=6.31
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
Sub Class_Globals
	Private surname, team, class, code, time, position As String
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize(cl As String, s As String, t As String, cd As String , tm As String, pos As String)
	class = cl
	surname = s
	team = t
	code = cd
	time = tm
	position = pos
End Sub

Public Sub GetSurname As String
	Return surname
End Sub

Public Sub GetTeam As String
	Return team
End Sub

Public Sub GetClass As String
	Return class
End Sub

Public Sub GetCode As String
	Return code
End Sub

Public Sub GetTime As String
	Return time
End Sub

Public Sub GetPosition As String
	Return position
End Sub