package trialGP.live;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class rider extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new BA(_ba, this, htSubs, "trialGP.live.rider");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", trialGP.live.rider.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 public anywheresoftware.b4a.keywords.Common __c = null;
public String _surname = "";
public String _team = "";
public String _class = "";
public String _code = "";
public String _time = "";
public String _position = "";
public trialGP.live.main _main = null;
public trialGP.live.starter _starter = null;
public trialGP.live.misc _misc = null;
public trialGP.live.onedayevent _onedayevent = null;
public trialGP.live.twodaysevent _twodaysevent = null;
public trialGP.live.httputils2service _httputils2service = null;
public String  _class_globals() throws Exception{
 //BA.debugLineNum = 1;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 2;BA.debugLine="Private surname, team, class, code, time, positio";
_surname = "";
_team = "";
_class = "";
_code = "";
_time = "";
_position = "";
 //BA.debugLineNum = 3;BA.debugLine="End Sub";
return "";
}
public String  _getclass() throws Exception{
 //BA.debugLineNum = 23;BA.debugLine="Public Sub GetClass As String";
 //BA.debugLineNum = 24;BA.debugLine="Return class";
if (true) return _class;
 //BA.debugLineNum = 25;BA.debugLine="End Sub";
return "";
}
public String  _getcode() throws Exception{
 //BA.debugLineNum = 27;BA.debugLine="Public Sub GetCode As String";
 //BA.debugLineNum = 28;BA.debugLine="Return code";
if (true) return _code;
 //BA.debugLineNum = 29;BA.debugLine="End Sub";
return "";
}
public String  _getposition() throws Exception{
 //BA.debugLineNum = 35;BA.debugLine="Public Sub GetPosition As String";
 //BA.debugLineNum = 36;BA.debugLine="Return position";
if (true) return _position;
 //BA.debugLineNum = 37;BA.debugLine="End Sub";
return "";
}
public String  _getsurname() throws Exception{
 //BA.debugLineNum = 15;BA.debugLine="Public Sub GetSurname As String";
 //BA.debugLineNum = 16;BA.debugLine="Return surname";
if (true) return _surname;
 //BA.debugLineNum = 17;BA.debugLine="End Sub";
return "";
}
public String  _getteam() throws Exception{
 //BA.debugLineNum = 19;BA.debugLine="Public Sub GetTeam As String";
 //BA.debugLineNum = 20;BA.debugLine="Return team";
if (true) return _team;
 //BA.debugLineNum = 21;BA.debugLine="End Sub";
return "";
}
public String  _gettime() throws Exception{
 //BA.debugLineNum = 31;BA.debugLine="Public Sub GetTime As String";
 //BA.debugLineNum = 32;BA.debugLine="Return time";
if (true) return _time;
 //BA.debugLineNum = 33;BA.debugLine="End Sub";
return "";
}
public String  _initialize(anywheresoftware.b4a.BA _ba,String _cl,String _s,String _t,String _cd,String _tm,String _pos) throws Exception{
innerInitialize(_ba);
 //BA.debugLineNum = 6;BA.debugLine="Public Sub Initialize(cl As String, s As String, t";
 //BA.debugLineNum = 7;BA.debugLine="class = cl";
_class = _cl;
 //BA.debugLineNum = 8;BA.debugLine="surname = s";
_surname = _s;
 //BA.debugLineNum = 9;BA.debugLine="team = t";
_team = _t;
 //BA.debugLineNum = 10;BA.debugLine="code = cd";
_code = _cd;
 //BA.debugLineNum = 11;BA.debugLine="time = tm";
_time = _tm;
 //BA.debugLineNum = 12;BA.debugLine="position = pos";
_position = _pos;
 //BA.debugLineNum = 13;BA.debugLine="End Sub";
return "";
}
public Object callSub(String sub, Object sender, Object[] args) throws Exception {
BA.senderHolder.set(sender);
return BA.SubDelegator.SubNotFound;
}
}
