package trialGP.live;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class twodaysevent extends Activity implements B4AActivity{
	public static twodaysevent mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "trialGP.live", "trialGP.live.twodaysevent");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (twodaysevent).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "trialGP.live", "trialGP.live.twodaysevent");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "trialGP.live.twodaysevent", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (twodaysevent) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (twodaysevent) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return twodaysevent.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (twodaysevent) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (twodaysevent) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        Object[] o;
        if (permissions.length > 0)
            o = new Object[] {permissions[0], grantResults[0] == 0};
        else
            o = new Object[] {"", false};
        processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.Timer _refreshtimer = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlfooter = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlheader = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnltabs = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnltabs2 = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlqualificationbar = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlday1bar = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlday2bar = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlheadersstart = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgmainlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgleftlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgrightlogo = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imghome = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgarrowstart = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgarrowlive = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbleventname = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblqualification = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblday1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblday2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblstart = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbllive = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblsupport = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblno = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblrider = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblclass = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbltime = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblpos = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbltotal = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblnodata = null;
public anywheresoftware.b4a.objects.ScrollViewWrapper _svresults = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlresults = null;
public static boolean _isqualification = false;
public static boolean _isdayone = false;
public static boolean _isstart = false;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _whitearrow = null;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _greyarrow = null;
public static int _querytype = 0;
public static int _currentscroll = 0;
public trialGP.live.main _main = null;
public trialGP.live.starter _starter = null;
public trialGP.live.misc _misc = null;
public trialGP.live.onedayevent _onedayevent = null;
public trialGP.live.httputils2service _httputils2service = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
 //BA.debugLineNum = 63;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 64;BA.debugLine="If Main.SQL1.IsInitialized = False Then";
if (mostCurrent._main._sql1.IsInitialized()==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 65;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 66;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 69;BA.debugLine="Activity.LoadLayout(\"TwoDaysEventLayout\")";
mostCurrent._activity.LoadLayout("TwoDaysEventLayout",mostCurrent.activityBA);
 //BA.debugLineNum = 71;BA.debugLine="Misc.HorizontalCenterView(imgMainLogo, pnlHeader)";
mostCurrent._misc._horizontalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgmainlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 73;BA.debugLine="Misc.VerticalCenterView(imgLeftLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgleftlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 74;BA.debugLine="Misc.VerticalCenterView(imgRightLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgrightlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 75;BA.debugLine="Misc.VerticalCenterView(imgMainLogo, pnlHeader)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imgmainlogo.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlheader.getObject())));
 //BA.debugLineNum = 76;BA.debugLine="Misc.VerticalCenterView(imgHome, pnlFooter)";
mostCurrent._misc._verticalcenterview(mostCurrent.activityBA,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._imghome.getObject())),(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._pnlfooter.getObject())));
 //BA.debugLineNum = 78;BA.debugLine="lblQualification.Width = (pnlTabs.Width / 2) - 4d";
mostCurrent._lblqualification.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)2)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4))));
 //BA.debugLineNum = 79;BA.debugLine="lblDay1.Width = (pnlTabs.Width / 4) - 2dip";
mostCurrent._lblday1.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)4)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 80;BA.debugLine="lblDay2.Width = (pnlTabs.Width / 4) - 2dip";
mostCurrent._lblday2.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)4)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 81;BA.debugLine="pnlQualificationBar.Width = (pnlTabs.Width / 2) -";
mostCurrent._pnlqualificationbar.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)2)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4))));
 //BA.debugLineNum = 82;BA.debugLine="pnlDay1Bar.Width = (pnlTabs.Width / 4) - 2dip";
mostCurrent._pnlday1bar.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)4)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 83;BA.debugLine="pnlDay2Bar.Width = (pnlTabs.Width / 4) - 2dip";
mostCurrent._pnlday2bar.setWidth((int) ((mostCurrent._pnltabs.getWidth()/(double)4)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 84;BA.debugLine="lblDay1.Left = lblQualification.Left + lblQualifi";
mostCurrent._lblday1.setLeft((int) (mostCurrent._lblqualification.getLeft()+mostCurrent._lblqualification.getWidth()));
 //BA.debugLineNum = 85;BA.debugLine="lblDay2.Left = lblDay1.Left + lblDay1.Width";
mostCurrent._lblday2.setLeft((int) (mostCurrent._lblday1.getLeft()+mostCurrent._lblday1.getWidth()));
 //BA.debugLineNum = 86;BA.debugLine="pnlDay1Bar.Left = pnlQualificationBar.Left + pnlQ";
mostCurrent._pnlday1bar.setLeft((int) (mostCurrent._pnlqualificationbar.getLeft()+mostCurrent._pnlqualificationbar.getWidth()));
 //BA.debugLineNum = 87;BA.debugLine="pnlDay2Bar.Left = pnlDay1Bar.Left + pnlDay1Bar.Wi";
mostCurrent._pnlday2bar.setLeft((int) (mostCurrent._pnlday1bar.getLeft()+mostCurrent._pnlday1bar.getWidth()));
 //BA.debugLineNum = 89;BA.debugLine="lblStart.Width = (pnlTabs2.Width / 2) - 51dip";
mostCurrent._lblstart.setWidth((int) ((mostCurrent._pnltabs2.getWidth()/(double)2)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (51))));
 //BA.debugLineNum = 90;BA.debugLine="lblLive.Width = (pnlTabs2.Width / 2) - 51dip";
mostCurrent._lbllive.setWidth((int) ((mostCurrent._pnltabs2.getWidth()/(double)2)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (51))));
 //BA.debugLineNum = 91;BA.debugLine="lblStart.Left = imgArrowStart.Left + imgArrowStar";
mostCurrent._lblstart.setLeft((int) (mostCurrent._imgarrowstart.getLeft()+mostCurrent._imgarrowstart.getWidth()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (10))));
 //BA.debugLineNum = 92;BA.debugLine="imgArrowLive.Left = pnlTabs2.Width / 2 + 15dip";
mostCurrent._imgarrowlive.setLeft((int) (mostCurrent._pnltabs2.getWidth()/(double)2+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (15))));
 //BA.debugLineNum = 93;BA.debugLine="lblLive.Left = imgArrowLive.Left + imgArrowLive.W";
mostCurrent._lbllive.setLeft((int) (mostCurrent._imgarrowlive.getLeft()+mostCurrent._imgarrowlive.getWidth()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (10))));
 //BA.debugLineNum = 95;BA.debugLine="whiteArrow.Initialize(LoadBitmap(File.DirAssets,";
mostCurrent._whitearrow.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"white-arrow.png").getObject()));
 //BA.debugLineNum = 96;BA.debugLine="greyArrow.Initialize(LoadBitmap(File.DirAssets, \"";
mostCurrent._greyarrow.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"grey-arrow.png").getObject()));
 //BA.debugLineNum = 98;BA.debugLine="pnlResults = SVResults.Panel";
mostCurrent._pnlresults = mostCurrent._svresults.getPanel();
 //BA.debugLineNum = 99;BA.debugLine="lblEventName.Text = Main.eventName";
mostCurrent._lbleventname.setText((Object)(mostCurrent._main._eventname));
 //BA.debugLineNum = 101;BA.debugLine="GetRefreshInterval";
_getrefreshinterval();
 //BA.debugLineNum = 103;BA.debugLine="SetActiveTab(True, False, True)";
_setactivetab(anywheresoftware.b4a.keywords.Common.True,anywheresoftware.b4a.keywords.Common.False,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 104;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 117;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 118;BA.debugLine="refreshTimer.Enabled = False";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 119;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 106;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 107;BA.debugLine="If Main.SQL1.IsInitialized = False Then";
if (mostCurrent._main._sql1.IsInitialized()==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 108;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 109;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 112;BA.debugLine="If refreshTimer.Interval > 0 Then";
if (_refreshtimer.getInterval()>0) { 
 //BA.debugLineNum = 113;BA.debugLine="refreshTimer.Enabled = True";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 115;BA.debugLine="End Sub";
return "";
}
public static String  _clearscreen() throws Exception{
 //BA.debugLineNum = 1361;BA.debugLine="Sub ClearScreen";
 //BA.debugLineNum = 1362;BA.debugLine="lblNoData.Visible = True";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1364;BA.debugLine="pnlResults.RemoveAllViews";
mostCurrent._pnlresults.RemoveAllViews();
 //BA.debugLineNum = 1365;BA.debugLine="End Sub";
return "";
}
public static String  _displaydaylive(int _daynum) throws Exception{
int _placedpanels = 0;
int _placedclasspanels = 0;
int _panelheight = 0;
int _classpanelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
String _currentclass = "";
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _classpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblclassname = null;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblposition = null;
anywheresoftware.b4a.objects.LabelWrapper _lblnumber = null;
anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
String _ridername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblnationality = null;
anywheresoftware.b4a.objects.LabelWrapper _lblteam = null;
int _placedsectionlabels = 0;
int _sectionlabelwidth = 0;
int _j = 0;
anywheresoftware.b4a.objects.LabelWrapper _lblindex = null;
String[] _str = null;
anywheresoftware.b4a.objects.LabelWrapper _lblscore1 = null;
anywheresoftware.b4a.objects.LabelWrapper _lblscore2 = null;
String _s1 = "";
String _s2 = "";
anywheresoftware.b4a.objects.LabelWrapper _lbll1 = null;
anywheresoftware.b4a.objects.LabelWrapper _lbll1result = null;
anywheresoftware.b4a.objects.LabelWrapper _lblriderpenalty = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridertotal = null;
 //BA.debugLineNum = 740;BA.debugLine="Sub DisplayDayLive(dayNum As Int)";
 //BA.debugLineNum = 741;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 742;BA.debugLine="Dim placedClassPanels As Int = 0";
_placedclasspanels = (int) (0);
 //BA.debugLineNum = 743;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 8";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)8);
 //BA.debugLineNum = 744;BA.debugLine="Dim classPanelHeight As Int = SVResults.Height /";
_classpanelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 746;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 748;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 749;BA.debugLine="If dayNum = 1 Then";
if (_daynum==1) { 
 //BA.debugLineNum = 750;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSu";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrNationality, trrClass, trrTeam, trdlL1 As L1, trdlL2 As L2, trdlPosition As position, trdlTotal As total, trdlPenalty As penalty, trdlStunt1 As section1, trdlStunt2 As section2, trdlStunt3 As section3, trdlStunt4 As section4, trdlStunt5 As section5, trdlStunt6 As section6, trdlStunt7 As section7, trdlStunt8 As section8, trdlStunt9 As section9, trdlStunt10 As section10, trdlStunt11 As section11, trdlStunt12 As section12, trdlStunt13 As section13, trdlStunt14 As section14, trdlStunt15 As section15 FROM trRiders, trDayLive WHERE trRiders.trrCode = trDayLive.trdlCode ORDER BY trdlIndex ASC")));
 }else {
 //BA.debugLineNum = 752;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSu";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrNationality, trrClass, trrTeam, trdl2L1 As L1, trdl2L2 As L2, trdl2Position As position, trdl2Total As total, trdl2Penalty As penalty, trdl2Stunt1 As section1, trdl2Stunt2 As section2, trdl2Stunt3 As section3, trdl2Stunt4 As section4, trdl2Stunt5 As section5, trdl2Stunt6 As section6, trdl2Stunt7 As section7, trdl2Stunt8 As section8, trdl2Stunt9 As section9, trdl2Stunt10 As section10, trdl2Stunt11 As section11, trdl2Stunt12 As section12, trdl2Stunt13 As section13, trdl2Stunt14 As section14, trdl2Stunt15 As section15 FROM trRiders, trDay2Live WHERE trRiders.trrCode = trDay2Live.trdl2Code ORDER BY trdl2Index ASC")));
 };
 //BA.debugLineNum = 755;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 756;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 760;BA.debugLine="Dim currentClass As String = \"\"";
_currentclass = "";
 //BA.debugLineNum = 761;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 762;BA.debugLine="For i = 0 To n";
{
final int step17 = 1;
final int limit17 = _n;
for (_i = (int) (0) ; (step17 > 0 && _i <= limit17) || (step17 < 0 && _i >= limit17); _i = ((int)(0 + _i + step17)) ) {
 //BA.debugLineNum = 763;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 766;BA.debugLine="If currentClass <> crs.GetString(\"trrClass\") The";
if ((_currentclass).equals(_crs.GetString("trrClass")) == false) { 
 //BA.debugLineNum = 767;BA.debugLine="Dim classPanel As Panel";
_classpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 768;BA.debugLine="classPanel.Initialize(\"classPanel\")";
_classpanel.Initialize(mostCurrent.activityBA,"classPanel");
 //BA.debugLineNum = 769;BA.debugLine="classPanel.Color = Colors.RGB(34, 180, 231)";
_classpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 770;BA.debugLine="pnlResults.AddView(classPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_classpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)+(_classpanelheight*_placedclasspanels)),mostCurrent._svresults.getWidth(),_classpanelheight);
 //BA.debugLineNum = 771;BA.debugLine="placedClassPanels = placedClassPanels + 1";
_placedclasspanels = (int) (_placedclasspanels+1);
 //BA.debugLineNum = 772;BA.debugLine="currentClass = crs.GetString(\"trrClass\")";
_currentclass = _crs.GetString("trrClass");
 //BA.debugLineNum = 775;BA.debugLine="Dim lblClassName As Label";
_lblclassname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 776;BA.debugLine="lblClassName.Initialize(\"lblClassName\")";
_lblclassname.Initialize(mostCurrent.activityBA,"lblClassName");
 //BA.debugLineNum = 777;BA.debugLine="lblClassName.Typeface = Typeface.LoadFromAssets";
_lblclassname.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 778;BA.debugLine="lblClassName.Text = currentClass";
_lblclassname.setText((Object)(_currentclass));
 //BA.debugLineNum = 779;BA.debugLine="lblClassName.TextSize = lblSupport.TextSize";
_lblclassname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 780;BA.debugLine="lblClassName.TextColor = Colors.RGB(255, 255, 2";
_lblclassname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 781;BA.debugLine="lblClassName.Gravity = Gravity.CENTER";
_lblclassname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 782;BA.debugLine="classPanel.AddView(lblClassName, 0, 0, classPan";
_classpanel.AddView((android.view.View)(_lblclassname.getObject()),(int) (0),(int) (0),_classpanel.getWidth(),_classpanel.getHeight());
 };
 //BA.debugLineNum = 786;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 787;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 788;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 789;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 791;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 794;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)+(_classpanelheight*_placedclasspanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 795;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 798;BA.debugLine="Dim lblPosition As Label";
_lblposition = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 799;BA.debugLine="lblPosition.Initialize(\"lblPosition\")";
_lblposition.Initialize(mostCurrent.activityBA,"lblPosition");
 //BA.debugLineNum = 800;BA.debugLine="lblPosition.Typeface = Typeface.LoadFromAssets(\"";
_lblposition.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 801;BA.debugLine="lblPosition.Text = crs.GetString(\"position\")";
_lblposition.setText((Object)(_crs.GetString("position")));
 //BA.debugLineNum = 802;BA.debugLine="lblPosition.TextSize = lblSupport.TextSize-2";
_lblposition.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-2));
 //BA.debugLineNum = 803;BA.debugLine="lblPosition.TextColor = Colors.RGB(34, 180, 231)";
_lblposition.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 804;BA.debugLine="lblPosition.Gravity = Gravity.CENTER";
_lblposition.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 805;BA.debugLine="resultPanel.AddView(lblPosition, lblPos.Left, 5d";
_resultpanel.AddView((android.view.View)(_lblposition.getObject()),mostCurrent._lblpos.getLeft(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),mostCurrent._lblpos.getWidth(),(int) (_resultpanel.getHeight()/(double)2.8));
 //BA.debugLineNum = 808;BA.debugLine="Dim lblNumber As Label";
_lblnumber = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 809;BA.debugLine="lblNumber.Initialize(\"lblNumber\")";
_lblnumber.Initialize(mostCurrent.activityBA,"lblNumber");
 //BA.debugLineNum = 810;BA.debugLine="lblNumber.Typeface = Typeface.LoadFromAssets(\"he";
_lblnumber.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 811;BA.debugLine="lblNumber.Text = crs.GetString(\"trrCode\")";
_lblnumber.setText((Object)(_crs.GetString("trrCode")));
 //BA.debugLineNum = 812;BA.debugLine="lblNumber.TextSize = lblSupport.TextSize-2";
_lblnumber.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-2));
 //BA.debugLineNum = 813;BA.debugLine="lblNumber.TextColor = Colors.RGB(255, 255, 255)";
_lblnumber.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 814;BA.debugLine="lblNumber.Gravity = Gravity.CENTER_VERTICAL";
_lblnumber.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 815;BA.debugLine="resultPanel.AddView(lblNumber, lblNo.Left, 5dip,";
_resultpanel.AddView((android.view.View)(_lblnumber.getObject()),mostCurrent._lblno.getLeft(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),mostCurrent._lblno.getWidth(),(int) (_resultpanel.getHeight()/(double)2.8));
 //BA.debugLineNum = 818;BA.debugLine="Dim lblName As Label";
_lblname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 819;BA.debugLine="lblName.Initialize(\"lblName\")";
_lblname.Initialize(mostCurrent.activityBA,"lblName");
 //BA.debugLineNum = 820;BA.debugLine="lblName.Typeface = Typeface.LoadFromAssets(\"helv";
_lblname.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 821;BA.debugLine="Dim riderName As String = crs.GetString(\"trrSurn";
_ridername = _crs.GetString("trrSurname");
 //BA.debugLineNum = 822;BA.debugLine="If riderName.Length > 11 Then";
if (_ridername.length()>11) { 
 //BA.debugLineNum = 823;BA.debugLine="lblName.Text = riderName.SubString2(0, 11)";
_lblname.setText((Object)(_ridername.substring((int) (0),(int) (11))));
 }else {
 //BA.debugLineNum = 825;BA.debugLine="lblName.Text = riderName";
_lblname.setText((Object)(_ridername));
 };
 //BA.debugLineNum = 827;BA.debugLine="lblName.TextSize = lblSupport.TextSize - 3";
_lblname.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-3));
 //BA.debugLineNum = 828;BA.debugLine="lblName.TextColor = Colors.RGB(255, 255, 255)";
_lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 829;BA.debugLine="lblName.Gravity = Gravity.CENTER_VERTICAL";
_lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 830;BA.debugLine="resultPanel.AddView(lblName, lblRider.Left, 5dip";
_resultpanel.AddView((android.view.View)(_lblname.getObject()),mostCurrent._lblrider.getLeft(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),mostCurrent._lblrider.getWidth(),(int) (_resultpanel.getHeight()/(double)3));
 //BA.debugLineNum = 833;BA.debugLine="Dim lblNationality As Label";
_lblnationality = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 834;BA.debugLine="lblNationality.Initialize(\"lblNationality\")";
_lblnationality.Initialize(mostCurrent.activityBA,"lblNationality");
 //BA.debugLineNum = 835;BA.debugLine="lblNationality.Typeface = Typeface.LoadFromAsset";
_lblnationality.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 836;BA.debugLine="lblNationality.Text = crs.GetString(\"trrNational";
_lblnationality.setText((Object)(_crs.GetString("trrNationality")));
 //BA.debugLineNum = 837;BA.debugLine="lblNationality.TextSize = lblSupport.TextSize -";
_lblnationality.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-5));
 //BA.debugLineNum = 838;BA.debugLine="lblNationality.TextColor = Colors.RGB(255, 255,";
_lblnationality.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 839;BA.debugLine="lblNationality.Gravity = Gravity.CENTER_VERTICAL";
_lblnationality.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 840;BA.debugLine="resultPanel.AddView(lblNationality, lblNo.Left,";
_resultpanel.AddView((android.view.View)(_lblnationality.getObject()),mostCurrent._lblno.getLeft(),(int) ((_resultpanel.getHeight()/(double)3)+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (7))),mostCurrent._lblno.getWidth(),(int) (_resultpanel.getHeight()/(double)3));
 //BA.debugLineNum = 843;BA.debugLine="Dim lblTeam As Label";
_lblteam = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 844;BA.debugLine="lblTeam.Initialize(\"lblTeam\")";
_lblteam.Initialize(mostCurrent.activityBA,"lblTeam");
 //BA.debugLineNum = 845;BA.debugLine="lblTeam.Typeface = Typeface.LoadFromAssets(\"helv";
_lblteam.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.LoadFromAssets("helvetica.ttf"));
 //BA.debugLineNum = 846;BA.debugLine="lblTeam.Text = crs.GetString(\"trrTeam\")";
_lblteam.setText((Object)(_crs.GetString("trrTeam")));
 //BA.debugLineNum = 847;BA.debugLine="lblTeam.TextSize = lblSupport.TextSize - 5";
_lblteam.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-5));
 //BA.debugLineNum = 848;BA.debugLine="lblTeam.TextColor = Colors.RGB(255, 255, 255)";
_lblteam.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 849;BA.debugLine="lblTeam.Gravity = Gravity.CENTER_VERTICAL";
_lblteam.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 850;BA.debugLine="resultPanel.AddView(lblTeam, lblRider.Left, (res";
_resultpanel.AddView((android.view.View)(_lblteam.getObject()),mostCurrent._lblrider.getLeft(),(int) ((_resultpanel.getHeight()/(double)3)+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (7))),mostCurrent._lblrider.getWidth(),(int) (_resultpanel.getHeight()/(double)3));
 //BA.debugLineNum = 853;BA.debugLine="Dim placedSectionLabels As Int = 0";
_placedsectionlabels = (int) (0);
 //BA.debugLineNum = 854;BA.debugLine="Dim sectionLabelWidth As Int = (lblClass.Width -";
_sectionlabelwidth = (int) ((mostCurrent._lblclass.getWidth()-15)/(double)15);
 //BA.debugLineNum = 855;BA.debugLine="For j = 0 To 14";
{
final int step91 = 1;
final int limit91 = (int) (14);
for (_j = (int) (0) ; (step91 > 0 && _j <= limit91) || (step91 < 0 && _j >= limit91); _j = ((int)(0 + _j + step91)) ) {
 //BA.debugLineNum = 856;BA.debugLine="Dim lblIndex As Label";
_lblindex = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 857;BA.debugLine="lblIndex.Initialize(\"lblIndex\")";
_lblindex.Initialize(mostCurrent.activityBA,"lblIndex");
 //BA.debugLineNum = 858;BA.debugLine="lblIndex.Text = j + 1";
_lblindex.setText((Object)(_j+1));
 //BA.debugLineNum = 859;BA.debugLine="lblIndex.TextSize = lblSupport.TextSize - 7";
_lblindex.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-7));
 //BA.debugLineNum = 860;BA.debugLine="lblIndex.TextColor = Colors.RGB(150, 150, 150)";
_lblindex.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 861;BA.debugLine="lblIndex.Gravity = Gravity.CENTER";
_lblindex.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 862;BA.debugLine="resultPanel.AddView(lblIndex, lblClass.Left + (";
_resultpanel.AddView((android.view.View)(_lblindex.getObject()),(int) (mostCurrent._lblclass.getLeft()+(_placedsectionlabels*_sectionlabelwidth)),(int) (0),_sectionlabelwidth,(int) (_resultpanel.getHeight()/(double)3));
 //BA.debugLineNum = 864;BA.debugLine="Dim str() As String";
_str = new String[(int) (0)];
java.util.Arrays.fill(_str,"");
 //BA.debugLineNum = 865;BA.debugLine="Dim lblScore1 As Label";
_lblscore1 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 866;BA.debugLine="Dim lblScore2 As Label";
_lblscore2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 867;BA.debugLine="lblScore1.Initialize(\"lblScore1\")";
_lblscore1.Initialize(mostCurrent.activityBA,"lblScore1");
 //BA.debugLineNum = 868;BA.debugLine="lblScore2.Initialize(\"lblScore2\")";
_lblscore2.Initialize(mostCurrent.activityBA,"lblScore2");
 //BA.debugLineNum = 869;BA.debugLine="lblScore1.Text = \"\"";
_lblscore1.setText((Object)(""));
 //BA.debugLineNum = 870;BA.debugLine="lblScore2.Text = \"\"";
_lblscore2.setText((Object)(""));
 //BA.debugLineNum = 871;BA.debugLine="Select Case j";
switch (_j) {
case 0: {
 //BA.debugLineNum = 873;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section1"));
 break; }
case 1: {
 //BA.debugLineNum = 875;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section2"));
 break; }
case 2: {
 //BA.debugLineNum = 877;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section3"));
 break; }
case 3: {
 //BA.debugLineNum = 879;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section4"));
 break; }
case 4: {
 //BA.debugLineNum = 881;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section5"));
 break; }
case 5: {
 //BA.debugLineNum = 883;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section6"));
 break; }
case 6: {
 //BA.debugLineNum = 885;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section7"));
 break; }
case 7: {
 //BA.debugLineNum = 887;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section8"));
 break; }
case 8: {
 //BA.debugLineNum = 889;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section9"));
 break; }
case 9: {
 //BA.debugLineNum = 891;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section10"));
 break; }
case 10: {
 //BA.debugLineNum = 893;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section11"));
 break; }
case 11: {
 //BA.debugLineNum = 895;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section12"));
 break; }
case 12: {
 //BA.debugLineNum = 897;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section13"));
 break; }
case 13: {
 //BA.debugLineNum = 899;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section14"));
 break; }
case 14: {
 //BA.debugLineNum = 901;BA.debugLine="str = Regex.split(\"@@\", crs.GetString(\"section";
_str = anywheresoftware.b4a.keywords.Common.Regex.Split("@@",_crs.GetString("section15"));
 break; }
}
;
 //BA.debugLineNum = 904;BA.debugLine="If str.Length = 1 Then";
if (_str.length==1) { 
 //BA.debugLineNum = 905;BA.debugLine="If str(0) = \"-\" Then";
if ((_str[(int) (0)]).equals("-")) { 
 //BA.debugLineNum = 906;BA.debugLine="lblScore1.Text = \"\"";
_lblscore1.setText((Object)(""));
 }else {
 //BA.debugLineNum = 908;BA.debugLine="lblScore1.Text = str(0)";
_lblscore1.setText((Object)(_str[(int) (0)]));
 };
 }else if(_str.length==2) { 
 //BA.debugLineNum = 911;BA.debugLine="Dim s1 As String";
_s1 = "";
 //BA.debugLineNum = 912;BA.debugLine="Dim s2 As String";
_s2 = "";
 //BA.debugLineNum = 913;BA.debugLine="If str(0) = \"-\" Then";
if ((_str[(int) (0)]).equals("-")) { 
 //BA.debugLineNum = 914;BA.debugLine="s1 = \"\"";
_s1 = "";
 }else {
 //BA.debugLineNum = 916;BA.debugLine="s1 = str(0)";
_s1 = _str[(int) (0)];
 };
 //BA.debugLineNum = 919;BA.debugLine="If str(1) = \"-\" Then";
if ((_str[(int) (1)]).equals("-")) { 
 //BA.debugLineNum = 920;BA.debugLine="s2 = \"\"";
_s2 = "";
 }else {
 //BA.debugLineNum = 922;BA.debugLine="s2 = str(1)";
_s2 = _str[(int) (1)];
 };
 //BA.debugLineNum = 924;BA.debugLine="lblScore1.Text = s1 & CRLF & s2";
_lblscore1.setText((Object)(_s1+anywheresoftware.b4a.keywords.Common.CRLF+_s2));
 };
 //BA.debugLineNum = 927;BA.debugLine="lblScore1.TextSize = lblSupport.TextSize - 5";
_lblscore1.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-5));
 //BA.debugLineNum = 928;BA.debugLine="lblScore1.TextColor = Colors.RGB(255, 255, 255)";
_lblscore1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 929;BA.debugLine="lblScore1.Gravity = Gravity.CENTER_HORIZONTAL";
_lblscore1.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_HORIZONTAL);
 //BA.debugLineNum = 930;BA.debugLine="resultPanel.AddView(lblScore1, lblClass.Left +";
_resultpanel.AddView((android.view.View)(_lblscore1.getObject()),(int) (mostCurrent._lblclass.getLeft()+(_placedsectionlabels*_sectionlabelwidth)),(int) (_resultpanel.getHeight()/(double)3),_sectionlabelwidth,(int) (2*(_resultpanel.getHeight()/(double)3)));
 //BA.debugLineNum = 932;BA.debugLine="placedSectionLabels = placedSectionLabels + 1";
_placedsectionlabels = (int) (_placedsectionlabels+1);
 }
};
 //BA.debugLineNum = 936;BA.debugLine="Dim lblL1 As Label";
_lbll1 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 937;BA.debugLine="lblL1.Initialize(\"lblL1\")";
_lbll1.Initialize(mostCurrent.activityBA,"lblL1");
 //BA.debugLineNum = 938;BA.debugLine="lblL1.Text = \"L1\" & CRLF & \"L2\"";
_lbll1.setText((Object)("L1"+anywheresoftware.b4a.keywords.Common.CRLF+"L2"));
 //BA.debugLineNum = 939;BA.debugLine="lblL1.TextSize = lblSupport.TextSize - 4";
_lbll1.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-4));
 //BA.debugLineNum = 940;BA.debugLine="lblL1.TextColor = Colors.RGB(150, 150, 150)";
_lbll1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 941;BA.debugLine="lblL1.Gravity = Gravity.CENTER_HORIZONTAL";
_lbll1.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_HORIZONTAL);
 //BA.debugLineNum = 943;BA.debugLine="resultPanel.AddView(lblL1, lblTime.Left - sectio";
_resultpanel.AddView((android.view.View)(_lbll1.getObject()),(int) (mostCurrent._lbltime.getLeft()-_sectionlabelwidth-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (8))),(int) (_resultpanel.getHeight()/(double)3),(int) (_sectionlabelwidth*1.3),(int) (2*(_resultpanel.getHeight()/(double)3)));
 //BA.debugLineNum = 945;BA.debugLine="Dim lblL1Result As Label";
_lbll1result = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 946;BA.debugLine="lblL1Result.Initialize(\"lblL1Result\")";
_lbll1result.Initialize(mostCurrent.activityBA,"lblL1Result");
 //BA.debugLineNum = 947;BA.debugLine="lblL1Result.Text = crs.GetString(\"L1\") & CRLF &";
_lbll1result.setText((Object)(_crs.GetString("L1")+anywheresoftware.b4a.keywords.Common.CRLF+_crs.GetString("L2")));
 //BA.debugLineNum = 948;BA.debugLine="lblL1Result.TextSize = lblSupport.TextSize - 4";
_lbll1result.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-4));
 //BA.debugLineNum = 949;BA.debugLine="lblL1Result.TextColor = Colors.RGB(255, 255, 255";
_lbll1result.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 950;BA.debugLine="lblL1Result.Gravity = Gravity.CENTER_HORIZONTAL";
_lbll1result.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_HORIZONTAL);
 //BA.debugLineNum = 951;BA.debugLine="resultPanel.AddView(lblL1Result, lblL1.Left + lb";
_resultpanel.AddView((android.view.View)(_lbll1result.getObject()),(int) (_lbll1.getLeft()+_lbll1.getWidth()),(int) (_resultpanel.getHeight()/(double)3),(int) (_sectionlabelwidth*1.3),(int) (2*(_resultpanel.getHeight()/(double)3)));
 //BA.debugLineNum = 954;BA.debugLine="Dim lblRiderPenalty As Label";
_lblriderpenalty = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 955;BA.debugLine="lblRiderPenalty.Initialize(\"lblTime\")";
_lblriderpenalty.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 956;BA.debugLine="lblRiderPenalty.Text = crs.GetString(\"penalty\")";
_lblriderpenalty.setText((Object)(_crs.GetString("penalty")));
 //BA.debugLineNum = 957;BA.debugLine="lblRiderPenalty.TextSize = lblSupport.TextSize -";
_lblriderpenalty.setTextSize((float) (mostCurrent._lblsupport.getTextSize()-2));
 //BA.debugLineNum = 958;BA.debugLine="lblRiderPenalty.TextColor = Colors.RGB(255, 255,";
_lblriderpenalty.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 959;BA.debugLine="lblRiderPenalty.Gravity = Gravity.CENTER";
_lblriderpenalty.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 960;BA.debugLine="resultPanel.AddView(lblRiderPenalty, lblTime.Lef";
_resultpanel.AddView((android.view.View)(_lblriderpenalty.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 963;BA.debugLine="Dim lblRiderTotal As Label";
_lblridertotal = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 964;BA.debugLine="lblRiderTotal.Initialize(\"lblTime\")";
_lblridertotal.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 965;BA.debugLine="lblRiderTotal.Text = crs.GetString(\"total\")";
_lblridertotal.setText((Object)(_crs.GetString("total")));
 //BA.debugLineNum = 966;BA.debugLine="lblRiderTotal.TextSize = lblSupport.TextSize";
_lblridertotal.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 967;BA.debugLine="lblRiderTotal.TextColor = Colors.RGB(34, 180, 23";
_lblridertotal.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 968;BA.debugLine="lblRiderTotal.Gravity = Gravity.CENTER";
_lblridertotal.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 969;BA.debugLine="resultPanel.AddView(lblRiderTotal, lblTotal.Left";
_resultpanel.AddView((android.view.View)(_lblridertotal.getObject()),mostCurrent._lbltotal.getLeft(),(int) (0),mostCurrent._lbltotal.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 971;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)+(_placedclasspanels*_classpanelheight)));
 //BA.debugLineNum = 973;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 974;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 975;BA.debugLine="End Sub";
return "";
}
public static String  _displaydayonestart() throws Exception{
int _placedpanels = 0;
int _panelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblindex = null;
anywheresoftware.b4a.objects.LabelWrapper _lblnumber = null;
anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
String _ridername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblriderclass = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridertime = null;
 //BA.debugLineNum = 977;BA.debugLine="Sub DisplayDayOneStart";
 //BA.debugLineNum = 978;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 979;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 14";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 981;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 983;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 984;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSur";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trdsTime FROM trRiders, trDayStart WHERE trRiders.trrCode = trDayStart.trdsCode ORDER BY trdsPosition ASC")));
 //BA.debugLineNum = 986;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 987;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 991;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 992;BA.debugLine="For i = 0 To n";
{
final int step10 = 1;
final int limit10 = _n;
for (_i = (int) (0) ; (step10 > 0 && _i <= limit10) || (step10 < 0 && _i >= limit10); _i = ((int)(0 + _i + step10)) ) {
 //BA.debugLineNum = 993;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 996;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 997;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 998;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 999;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 1001;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 1004;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1005;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1008;BA.debugLine="Dim lblIndex As Label";
_lblindex = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1009;BA.debugLine="lblIndex.Initialize(\"lblIndex\")";
_lblindex.Initialize(mostCurrent.activityBA,"lblIndex");
 //BA.debugLineNum = 1010;BA.debugLine="lblIndex.Text = i + 1";
_lblindex.setText((Object)(_i+1));
 //BA.debugLineNum = 1011;BA.debugLine="lblIndex.TextSize = lblSupport.TextSize";
_lblindex.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1012;BA.debugLine="lblIndex.TextColor = Colors.RGB(150, 150, 150)";
_lblindex.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1013;BA.debugLine="lblIndex.Gravity = Gravity.CENTER_VERTICAL";
_lblindex.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1014;BA.debugLine="resultPanel.AddView(lblIndex, 5dip, 0, resultPan";
_resultpanel.AddView((android.view.View)(_lblindex.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),(int) (0),(int) (_resultpanel.getWidth()/(double)10),_resultpanel.getHeight());
 //BA.debugLineNum = 1017;BA.debugLine="Dim lblNumber As Label";
_lblnumber = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1018;BA.debugLine="lblNumber.Initialize(\"lblNumber\")";
_lblnumber.Initialize(mostCurrent.activityBA,"lblNumber");
 //BA.debugLineNum = 1019;BA.debugLine="lblNumber.Text = crs.GetString(\"trrCode\")";
_lblnumber.setText((Object)(_crs.GetString("trrCode")));
 //BA.debugLineNum = 1020;BA.debugLine="lblNumber.TextSize = lblSupport.TextSize";
_lblnumber.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1021;BA.debugLine="lblNumber.TextColor = Colors.RGB(255, 255, 255)";
_lblnumber.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1022;BA.debugLine="lblNumber.Gravity = Gravity.CENTER_VERTICAL";
_lblnumber.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1023;BA.debugLine="resultPanel.AddView(lblNumber, lblNo.Left, 0, lb";
_resultpanel.AddView((android.view.View)(_lblnumber.getObject()),mostCurrent._lblno.getLeft(),(int) (0),mostCurrent._lblno.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1026;BA.debugLine="Dim lblName As Label";
_lblname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1027;BA.debugLine="lblName.Initialize(\"lblName\")";
_lblname.Initialize(mostCurrent.activityBA,"lblName");
 //BA.debugLineNum = 1028;BA.debugLine="Dim riderName As String = crs.GetString(\"trrSurn";
_ridername = _crs.GetString("trrSurname");
 //BA.debugLineNum = 1029;BA.debugLine="If riderName.Length > 12 Then";
if (_ridername.length()>12) { 
 //BA.debugLineNum = 1030;BA.debugLine="lblName.Text = riderName.SubString2(0, 12)";
_lblname.setText((Object)(_ridername.substring((int) (0),(int) (12))));
 }else {
 //BA.debugLineNum = 1032;BA.debugLine="lblName.Text = riderName";
_lblname.setText((Object)(_ridername));
 };
 //BA.debugLineNum = 1034;BA.debugLine="lblName.TextSize = lblSupport.TextSize";
_lblname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1035;BA.debugLine="lblName.TextColor = Colors.RGB(255, 255, 255)";
_lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1036;BA.debugLine="lblName.Gravity = Gravity.CENTER_VERTICAL";
_lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1037;BA.debugLine="resultPanel.AddView(lblName, lblRider.Left, 0, l";
_resultpanel.AddView((android.view.View)(_lblname.getObject()),mostCurrent._lblrider.getLeft(),(int) (0),mostCurrent._lblrider.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1040;BA.debugLine="Dim lblRiderClass As Label";
_lblriderclass = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1041;BA.debugLine="lblRiderClass.Initialize(\"lblClass\")";
_lblriderclass.Initialize(mostCurrent.activityBA,"lblClass");
 //BA.debugLineNum = 1042;BA.debugLine="lblRiderClass.Text = crs.GetString(\"trrClass\")";
_lblriderclass.setText((Object)(_crs.GetString("trrClass")));
 //BA.debugLineNum = 1043;BA.debugLine="lblRiderClass.TextSize = lblSupport.TextSize";
_lblriderclass.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1044;BA.debugLine="lblRiderClass.TextColor = Colors.RGB(150, 150, 1";
_lblriderclass.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1045;BA.debugLine="lblRiderClass.Gravity = Gravity.CENTER_VERTICAL";
_lblriderclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1046;BA.debugLine="resultPanel.AddView(lblRiderClass, lblClass.Left";
_resultpanel.AddView((android.view.View)(_lblriderclass.getObject()),mostCurrent._lblclass.getLeft(),(int) (0),(int) (mostCurrent._lblclass.getLeft()-mostCurrent._lbltime.getLeft()),_resultpanel.getHeight());
 //BA.debugLineNum = 1049;BA.debugLine="Dim lblRiderTime As Label";
_lblridertime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1050;BA.debugLine="lblRiderTime.Initialize(\"lblTime\")";
_lblridertime.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1051;BA.debugLine="lblRiderTime.Text = crs.GetString(\"trdsTime\")";
_lblridertime.setText((Object)(_crs.GetString("trdsTime")));
 //BA.debugLineNum = 1052;BA.debugLine="lblRiderTime.TextSize = lblSupport.TextSize";
_lblridertime.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1053;BA.debugLine="lblRiderTime.TextColor = Colors.RGB(34, 180, 231";
_lblridertime.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1054;BA.debugLine="lblRiderTime.Gravity = Gravity.CENTER_VERTICAL";
_lblridertime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1055;BA.debugLine="resultPanel.AddView(lblRiderTime, lblTime.Left,";
_resultpanel.AddView((android.view.View)(_lblridertime.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 1057;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)));
 //BA.debugLineNum = 1059;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1060;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1061;BA.debugLine="End Sub";
return "";
}
public static String  _displaydaytwostart() throws Exception{
int _placedpanels = 0;
int _panelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblindex = null;
anywheresoftware.b4a.objects.LabelWrapper _lblnumber = null;
anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
String _ridername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblriderclass = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridertime = null;
 //BA.debugLineNum = 1063;BA.debugLine="Sub DisplayDayTwoStart";
 //BA.debugLineNum = 1064;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 1065;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 14";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 1067;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 1069;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 1070;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSur";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trds2Time FROM trRiders, trDay2Start WHERE trRiders.trrCode = trDay2Start.trds2Code ORDER BY trds2Position ASC")));
 //BA.debugLineNum = 1072;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 1073;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 1077;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 1078;BA.debugLine="For i = 0 To n";
{
final int step10 = 1;
final int limit10 = _n;
for (_i = (int) (0) ; (step10 > 0 && _i <= limit10) || (step10 < 0 && _i >= limit10); _i = ((int)(0 + _i + step10)) ) {
 //BA.debugLineNum = 1079;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 1082;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1083;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 1084;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 1085;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 1087;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 1090;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1091;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1094;BA.debugLine="Dim lblIndex As Label";
_lblindex = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1095;BA.debugLine="lblIndex.Initialize(\"lblIndex\")";
_lblindex.Initialize(mostCurrent.activityBA,"lblIndex");
 //BA.debugLineNum = 1096;BA.debugLine="lblIndex.Text = i + 1";
_lblindex.setText((Object)(_i+1));
 //BA.debugLineNum = 1097;BA.debugLine="lblIndex.TextSize = lblSupport.TextSize";
_lblindex.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1098;BA.debugLine="lblIndex.TextColor = Colors.RGB(150, 150, 150)";
_lblindex.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1099;BA.debugLine="lblIndex.Gravity = Gravity.CENTER_VERTICAL";
_lblindex.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1100;BA.debugLine="resultPanel.AddView(lblIndex, 5dip, 0, resultPan";
_resultpanel.AddView((android.view.View)(_lblindex.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),(int) (0),(int) (_resultpanel.getWidth()/(double)10),_resultpanel.getHeight());
 //BA.debugLineNum = 1103;BA.debugLine="Dim lblNumber As Label";
_lblnumber = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1104;BA.debugLine="lblNumber.Initialize(\"lblNumber\")";
_lblnumber.Initialize(mostCurrent.activityBA,"lblNumber");
 //BA.debugLineNum = 1105;BA.debugLine="lblNumber.Text = crs.GetString(\"trrCode\")";
_lblnumber.setText((Object)(_crs.GetString("trrCode")));
 //BA.debugLineNum = 1106;BA.debugLine="lblNumber.TextSize = lblSupport.TextSize";
_lblnumber.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1107;BA.debugLine="lblNumber.TextColor = Colors.RGB(255, 255, 255)";
_lblnumber.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1108;BA.debugLine="lblNumber.Gravity = Gravity.CENTER_VERTICAL";
_lblnumber.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1109;BA.debugLine="resultPanel.AddView(lblNumber, lblNo.Left, 0, lb";
_resultpanel.AddView((android.view.View)(_lblnumber.getObject()),mostCurrent._lblno.getLeft(),(int) (0),mostCurrent._lblno.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1112;BA.debugLine="Dim lblName As Label";
_lblname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1113;BA.debugLine="lblName.Initialize(\"lblName\")";
_lblname.Initialize(mostCurrent.activityBA,"lblName");
 //BA.debugLineNum = 1114;BA.debugLine="Dim riderName As String = crs.GetString(\"trrSurn";
_ridername = _crs.GetString("trrSurname");
 //BA.debugLineNum = 1115;BA.debugLine="If riderName.Length > 12 Then";
if (_ridername.length()>12) { 
 //BA.debugLineNum = 1116;BA.debugLine="lblName.Text = riderName.SubString2(0, 12)";
_lblname.setText((Object)(_ridername.substring((int) (0),(int) (12))));
 }else {
 //BA.debugLineNum = 1118;BA.debugLine="lblName.Text = riderName";
_lblname.setText((Object)(_ridername));
 };
 //BA.debugLineNum = 1120;BA.debugLine="lblName.TextSize = lblSupport.TextSize";
_lblname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1121;BA.debugLine="lblName.TextColor = Colors.RGB(255, 255, 255)";
_lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1122;BA.debugLine="lblName.Gravity = Gravity.CENTER_VERTICAL";
_lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1123;BA.debugLine="resultPanel.AddView(lblName, lblRider.Left, 0, l";
_resultpanel.AddView((android.view.View)(_lblname.getObject()),mostCurrent._lblrider.getLeft(),(int) (0),mostCurrent._lblrider.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1126;BA.debugLine="Dim lblRiderClass As Label";
_lblriderclass = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1127;BA.debugLine="lblRiderClass.Initialize(\"lblClass\")";
_lblriderclass.Initialize(mostCurrent.activityBA,"lblClass");
 //BA.debugLineNum = 1128;BA.debugLine="lblRiderClass.Text = crs.GetString(\"trrClass\")";
_lblriderclass.setText((Object)(_crs.GetString("trrClass")));
 //BA.debugLineNum = 1129;BA.debugLine="lblRiderClass.TextSize = lblSupport.TextSize";
_lblriderclass.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1130;BA.debugLine="lblRiderClass.TextColor = Colors.RGB(150, 150, 1";
_lblriderclass.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1131;BA.debugLine="lblRiderClass.Gravity = Gravity.CENTER_VERTICAL";
_lblriderclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1132;BA.debugLine="resultPanel.AddView(lblRiderClass, lblClass.Left";
_resultpanel.AddView((android.view.View)(_lblriderclass.getObject()),mostCurrent._lblclass.getLeft(),(int) (0),(int) (mostCurrent._lblclass.getLeft()-mostCurrent._lbltime.getLeft()),_resultpanel.getHeight());
 //BA.debugLineNum = 1135;BA.debugLine="Dim lblRiderTime As Label";
_lblridertime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1136;BA.debugLine="lblRiderTime.Initialize(\"lblTime\")";
_lblridertime.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1137;BA.debugLine="lblRiderTime.Text = crs.GetString(\"trds2Time\")";
_lblridertime.setText((Object)(_crs.GetString("trds2Time")));
 //BA.debugLineNum = 1138;BA.debugLine="lblRiderTime.TextSize = lblSupport.TextSize";
_lblridertime.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1139;BA.debugLine="lblRiderTime.TextColor = Colors.RGB(34, 180, 231";
_lblridertime.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1140;BA.debugLine="lblRiderTime.Gravity = Gravity.CENTER_VERTICAL";
_lblridertime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1141;BA.debugLine="resultPanel.AddView(lblRiderTime, lblTime.Left,";
_resultpanel.AddView((android.view.View)(_lblridertime.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 1143;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)));
 //BA.debugLineNum = 1145;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1146;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1147;BA.debugLine="End Sub";
return "";
}
public static String  _displayqualificationlive() throws Exception{
int _placedpanels = 0;
int _panelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
String _currentclass = "";
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _classpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblclassname = null;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblposition = null;
anywheresoftware.b4a.objects.LabelWrapper _lblcode = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridername = null;
anywheresoftware.b4a.objects.LabelWrapper _lblscore = null;
anywheresoftware.b4a.objects.LabelWrapper _lblresulttime = null;
 //BA.debugLineNum = 1149;BA.debugLine="Sub DisplayQualificationLive";
 //BA.debugLineNum = 1150;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 1151;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 14";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 1153;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 1155;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 1156;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trqlPosition, t";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trqlPosition, trqlIndex, trrCode, trrSurname, trrClass, trqlScore, trqlTime FROM trRiders, trQuLive WHERE trRiders.trrCode = trQuLive.trqlCode ORDER BY trqlPosition ASC")));
 //BA.debugLineNum = 1158;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 1159;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 1163;BA.debugLine="Dim currentClass As String = \"\"";
_currentclass = "";
 //BA.debugLineNum = 1164;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 1165;BA.debugLine="For i = 0 To n";
{
final int step11 = 1;
final int limit11 = _n;
for (_i = (int) (0) ; (step11 > 0 && _i <= limit11) || (step11 < 0 && _i >= limit11); _i = ((int)(0 + _i + step11)) ) {
 //BA.debugLineNum = 1166;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 1169;BA.debugLine="If currentClass <> crs.GetString(\"trrClass\") The";
if ((_currentclass).equals(_crs.GetString("trrClass")) == false) { 
 //BA.debugLineNum = 1170;BA.debugLine="Dim classPanel As Panel";
_classpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1171;BA.debugLine="classPanel.Initialize(\"classPanel\")";
_classpanel.Initialize(mostCurrent.activityBA,"classPanel");
 //BA.debugLineNum = 1172;BA.debugLine="classPanel.Color = Colors.RGB(34, 180, 231)";
_classpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1173;BA.debugLine="pnlResults.AddView(classPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_classpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1174;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1175;BA.debugLine="currentClass = crs.GetString(\"trrClass\")";
_currentclass = _crs.GetString("trrClass");
 //BA.debugLineNum = 1178;BA.debugLine="Dim lblClassName As Label";
_lblclassname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1179;BA.debugLine="lblClassName.Initialize(\"lblClassName\")";
_lblclassname.Initialize(mostCurrent.activityBA,"lblClassName");
 //BA.debugLineNum = 1180;BA.debugLine="lblClassName.Text = currentClass";
_lblclassname.setText((Object)(_currentclass));
 //BA.debugLineNum = 1181;BA.debugLine="lblClassName.TextSize = lblSupport.TextSize";
_lblclassname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1182;BA.debugLine="lblClassName.TextColor = Colors.RGB(255, 255, 2";
_lblclassname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1183;BA.debugLine="lblClassName.Gravity = Gravity.CENTER";
_lblclassname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 1184;BA.debugLine="classPanel.AddView(lblClassName, 0, 0, classPan";
_classpanel.AddView((android.view.View)(_lblclassname.getObject()),(int) (0),(int) (0),_classpanel.getWidth(),_classpanel.getHeight());
 };
 //BA.debugLineNum = 1188;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1189;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 1190;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 1191;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 1193;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 1196;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1197;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1200;BA.debugLine="Dim lblPosition As Label";
_lblposition = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1201;BA.debugLine="lblPosition.Initialize(\"lblPosition\")";
_lblposition.Initialize(mostCurrent.activityBA,"lblPosition");
 //BA.debugLineNum = 1202;BA.debugLine="lblPosition.Text = crs.GetString(\"trqlIndex\")";
_lblposition.setText((Object)(_crs.GetString("trqlIndex")));
 //BA.debugLineNum = 1203;BA.debugLine="lblPosition.TextSize = lblSupport.TextSize";
_lblposition.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1204;BA.debugLine="lblPosition.TextColor = Colors.RGB(34, 180, 231)";
_lblposition.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1205;BA.debugLine="lblPosition.Gravity = Gravity.CENTER_VERTICAL";
_lblposition.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1206;BA.debugLine="resultPanel.AddView(lblPosition, lblPos.Left, 0,";
_resultpanel.AddView((android.view.View)(_lblposition.getObject()),mostCurrent._lblpos.getLeft(),(int) (0),mostCurrent._lblpos.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1209;BA.debugLine="Dim lblCode As Label";
_lblcode = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1210;BA.debugLine="lblCode.Initialize(\"lblCode\")";
_lblcode.Initialize(mostCurrent.activityBA,"lblCode");
 //BA.debugLineNum = 1211;BA.debugLine="lblCode.Text = crs.GetString(\"trrCode\")";
_lblcode.setText((Object)(_crs.GetString("trrCode")));
 //BA.debugLineNum = 1212;BA.debugLine="lblCode.TextSize = lblSupport.TextSize";
_lblcode.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1213;BA.debugLine="lblCode.TextColor = Colors.RGB(255, 255, 255)";
_lblcode.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1214;BA.debugLine="lblCode.Gravity = Gravity.CENTER_VERTICAL";
_lblcode.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1215;BA.debugLine="resultPanel.AddView(lblCode, lblNo.Left, 0, lblN";
_resultpanel.AddView((android.view.View)(_lblcode.getObject()),mostCurrent._lblno.getLeft(),(int) (0),mostCurrent._lblno.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1218;BA.debugLine="Dim lblRiderName As Label";
_lblridername = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1219;BA.debugLine="lblRiderName.Initialize(\"lblRiderName\")";
_lblridername.Initialize(mostCurrent.activityBA,"lblRiderName");
 //BA.debugLineNum = 1220;BA.debugLine="lblRiderName.Text = crs.GetString(\"trrSurname\")";
_lblridername.setText((Object)(_crs.GetString("trrSurname")));
 //BA.debugLineNum = 1221;BA.debugLine="lblRiderName.TextSize = lblSupport.TextSize";
_lblridername.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1222;BA.debugLine="lblRiderName.TextColor = Colors.RGB(255, 255, 25";
_lblridername.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1223;BA.debugLine="lblRiderName.Gravity = Gravity.CENTER_VERTICAL";
_lblridername.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1224;BA.debugLine="resultPanel.AddView(lblRiderName, lblRider.Left,";
_resultpanel.AddView((android.view.View)(_lblridername.getObject()),mostCurrent._lblrider.getLeft(),(int) (0),mostCurrent._lblrider.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1227;BA.debugLine="Dim lblScore As Label";
_lblscore = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1228;BA.debugLine="lblScore.Initialize(\"lblScore\")";
_lblscore.Initialize(mostCurrent.activityBA,"lblScore");
 //BA.debugLineNum = 1229;BA.debugLine="lblScore.Text = crs.GetInt(\"trqlScore\")";
_lblscore.setText((Object)(_crs.GetInt("trqlScore")));
 //BA.debugLineNum = 1230;BA.debugLine="lblScore.TextSize = lblSupport.TextSize";
_lblscore.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1231;BA.debugLine="lblScore.TextColor = Colors.RGB(255, 255, 255)";
_lblscore.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1232;BA.debugLine="lblScore.Gravity = Gravity.CENTER_VERTICAL";
_lblscore.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1233;BA.debugLine="resultPanel.AddView(lblScore, lblClass.Left, 0,";
_resultpanel.AddView((android.view.View)(_lblscore.getObject()),mostCurrent._lblclass.getLeft(),(int) (0),mostCurrent._lblclass.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1236;BA.debugLine="Dim lblResultTime As Label";
_lblresulttime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1237;BA.debugLine="lblResultTime.Initialize(\"lblTime\")";
_lblresulttime.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1238;BA.debugLine="lblResultTime.Text = crs.GetString(\"trqlTime\")";
_lblresulttime.setText((Object)(_crs.GetString("trqlTime")));
 //BA.debugLineNum = 1239;BA.debugLine="lblResultTime.TextSize = lblSupport.TextSize";
_lblresulttime.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1240;BA.debugLine="lblResultTime.TextColor = Colors.RGB(34, 180, 23";
_lblresulttime.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1241;BA.debugLine="lblResultTime.Gravity = Gravity.CENTER_VERTICAL";
_lblresulttime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1242;BA.debugLine="resultPanel.AddView(lblResultTime, lblTime.Left,";
_resultpanel.AddView((android.view.View)(_lblresulttime.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 1245;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)));
 //BA.debugLineNum = 1247;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1248;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1249;BA.debugLine="End Sub";
return "";
}
public static String  _displayqualificationstart() throws Exception{
int _placedpanels = 0;
int _panelheight = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.PanelWrapper _resultpanel = null;
anywheresoftware.b4a.objects.LabelWrapper _lblindex = null;
anywheresoftware.b4a.objects.LabelWrapper _lblnumber = null;
anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
String _ridername = "";
anywheresoftware.b4a.objects.LabelWrapper _lblriderclass = null;
anywheresoftware.b4a.objects.LabelWrapper _lblridertime = null;
 //BA.debugLineNum = 1275;BA.debugLine="Sub DisplayQualificationStart";
 //BA.debugLineNum = 1276;BA.debugLine="Dim placedPanels As Int = 0";
_placedpanels = (int) (0);
 //BA.debugLineNum = 1277;BA.debugLine="Dim panelHeight As Int = SVResults.Height / 14";
_panelheight = (int) (mostCurrent._svresults.getHeight()/(double)14);
 //BA.debugLineNum = 1279;BA.debugLine="ClearScreen";
_clearscreen();
 //BA.debugLineNum = 1281;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 1282;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trrCode, trrSur";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trqsTime FROM trRiders, trQuStart WHERE trRiders.trrCode = trQuStart.trqsCode ORDER BY trqsPosition ASC")));
 //BA.debugLineNum = 1284;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 1285;BA.debugLine="lblNoData.Visible = False";
mostCurrent._lblnodata.setVisible(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 1289;BA.debugLine="Dim n As Int = crs.RowCount - 1";
_n = (int) (_crs.getRowCount()-1);
 //BA.debugLineNum = 1290;BA.debugLine="For i = 0 To n";
{
final int step10 = 1;
final int limit10 = _n;
for (_i = (int) (0) ; (step10 > 0 && _i <= limit10) || (step10 < 0 && _i >= limit10); _i = ((int)(0 + _i + step10)) ) {
 //BA.debugLineNum = 1291;BA.debugLine="crs.Position = i";
_crs.setPosition(_i);
 //BA.debugLineNum = 1294;BA.debugLine="Dim resultPanel As Panel";
_resultpanel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 1295;BA.debugLine="resultPanel.Initialize(\"resultPanel\")";
_resultpanel.Initialize(mostCurrent.activityBA,"resultPanel");
 //BA.debugLineNum = 1296;BA.debugLine="If i Mod 2 = 0 Then";
if (_i%2==0) { 
 //BA.debugLineNum = 1297;BA.debugLine="resultPanel.Color = Colors.RGB(60, 60, 60)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (60),(int) (60),(int) (60)));
 }else {
 //BA.debugLineNum = 1299;BA.debugLine="resultPanel.Color = Colors.RGB(0, 0, 0)";
_resultpanel.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (0),(int) (0),(int) (0)));
 };
 //BA.debugLineNum = 1302;BA.debugLine="pnlResults.AddView(resultPanel, 0, (panelHeight";
mostCurrent._pnlresults.AddView((android.view.View)(_resultpanel.getObject()),(int) (0),(int) ((_panelheight*_placedpanels)),mostCurrent._svresults.getWidth(),_panelheight);
 //BA.debugLineNum = 1303;BA.debugLine="placedPanels = placedPanels + 1";
_placedpanels = (int) (_placedpanels+1);
 //BA.debugLineNum = 1306;BA.debugLine="Dim lblIndex As Label";
_lblindex = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1307;BA.debugLine="lblIndex.Initialize(\"lblIndex\")";
_lblindex.Initialize(mostCurrent.activityBA,"lblIndex");
 //BA.debugLineNum = 1308;BA.debugLine="lblIndex.Text = i + 1";
_lblindex.setText((Object)(_i+1));
 //BA.debugLineNum = 1309;BA.debugLine="lblIndex.TextSize = lblSupport.TextSize";
_lblindex.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1310;BA.debugLine="lblIndex.TextColor = Colors.RGB(150, 150, 150)";
_lblindex.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1311;BA.debugLine="lblIndex.Gravity = Gravity.CENTER_VERTICAL";
_lblindex.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1312;BA.debugLine="resultPanel.AddView(lblIndex, 5dip, 0, resultPan";
_resultpanel.AddView((android.view.View)(_lblindex.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5)),(int) (0),(int) (_resultpanel.getWidth()/(double)10),_resultpanel.getHeight());
 //BA.debugLineNum = 1315;BA.debugLine="Dim lblNumber As Label";
_lblnumber = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1316;BA.debugLine="lblNumber.Initialize(\"lblNumber\")";
_lblnumber.Initialize(mostCurrent.activityBA,"lblNumber");
 //BA.debugLineNum = 1317;BA.debugLine="lblNumber.Text = crs.GetString(\"trrCode\")";
_lblnumber.setText((Object)(_crs.GetString("trrCode")));
 //BA.debugLineNum = 1318;BA.debugLine="lblNumber.TextSize = lblSupport.TextSize";
_lblnumber.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1319;BA.debugLine="lblNumber.TextColor = Colors.RGB(255, 255, 255)";
_lblnumber.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1320;BA.debugLine="lblNumber.Gravity = Gravity.CENTER_VERTICAL";
_lblnumber.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1321;BA.debugLine="resultPanel.AddView(lblNumber, lblNo.Left, 0, lb";
_resultpanel.AddView((android.view.View)(_lblnumber.getObject()),mostCurrent._lblno.getLeft(),(int) (0),mostCurrent._lblno.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1324;BA.debugLine="Dim lblName As Label";
_lblname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1325;BA.debugLine="lblName.Initialize(\"lblName\")";
_lblname.Initialize(mostCurrent.activityBA,"lblName");
 //BA.debugLineNum = 1326;BA.debugLine="Dim riderName As String = crs.GetString(\"trrSurn";
_ridername = _crs.GetString("trrSurname");
 //BA.debugLineNum = 1327;BA.debugLine="If riderName.Length > 12 Then";
if (_ridername.length()>12) { 
 //BA.debugLineNum = 1328;BA.debugLine="lblName.Text = riderName.SubString2(0, 12)";
_lblname.setText((Object)(_ridername.substring((int) (0),(int) (12))));
 }else {
 //BA.debugLineNum = 1330;BA.debugLine="lblName.Text = riderName";
_lblname.setText((Object)(_ridername));
 };
 //BA.debugLineNum = 1332;BA.debugLine="lblName.TextSize = lblSupport.TextSize";
_lblname.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1333;BA.debugLine="lblName.TextColor = Colors.RGB(255, 255, 255)";
_lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 1334;BA.debugLine="lblName.Gravity = Gravity.CENTER_VERTICAL";
_lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1335;BA.debugLine="resultPanel.AddView(lblName, lblRider.Left, 0, l";
_resultpanel.AddView((android.view.View)(_lblname.getObject()),mostCurrent._lblrider.getLeft(),(int) (0),mostCurrent._lblrider.getWidth(),_resultpanel.getHeight());
 //BA.debugLineNum = 1338;BA.debugLine="Dim lblRiderClass As Label";
_lblriderclass = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1339;BA.debugLine="lblRiderClass.Initialize(\"lblClass\")";
_lblriderclass.Initialize(mostCurrent.activityBA,"lblClass");
 //BA.debugLineNum = 1340;BA.debugLine="lblRiderClass.Text = crs.GetString(\"trrClass\")";
_lblriderclass.setText((Object)(_crs.GetString("trrClass")));
 //BA.debugLineNum = 1341;BA.debugLine="lblRiderClass.TextSize = lblSupport.TextSize";
_lblriderclass.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1342;BA.debugLine="lblRiderClass.TextColor = Colors.RGB(150, 150, 1";
_lblriderclass.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (150),(int) (150),(int) (150)));
 //BA.debugLineNum = 1343;BA.debugLine="lblRiderClass.Gravity = Gravity.CENTER_VERTICAL";
_lblriderclass.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1344;BA.debugLine="resultPanel.AddView(lblRiderClass, lblClass.Left";
_resultpanel.AddView((android.view.View)(_lblriderclass.getObject()),mostCurrent._lblclass.getLeft(),(int) (0),(int) (mostCurrent._lblrider.getLeft()+mostCurrent._lblrider.getWidth()),_resultpanel.getHeight());
 //BA.debugLineNum = 1347;BA.debugLine="Dim lblRiderTime As Label";
_lblridertime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 1348;BA.debugLine="lblRiderTime.Initialize(\"lblTime\")";
_lblridertime.Initialize(mostCurrent.activityBA,"lblTime");
 //BA.debugLineNum = 1349;BA.debugLine="lblRiderTime.Text = crs.GetString(\"trqsTime\")";
_lblridertime.setText((Object)(_crs.GetString("trqsTime")));
 //BA.debugLineNum = 1350;BA.debugLine="lblRiderTime.TextSize = lblSupport.TextSize";
_lblridertime.setTextSize(mostCurrent._lblsupport.getTextSize());
 //BA.debugLineNum = 1351;BA.debugLine="lblRiderTime.TextColor = Colors.RGB(34, 180, 231";
_lblridertime.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 1352;BA.debugLine="lblRiderTime.Gravity = Gravity.CENTER_VERTICAL";
_lblridertime.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_VERTICAL);
 //BA.debugLineNum = 1353;BA.debugLine="resultPanel.AddView(lblRiderTime, lblTime.Left,";
_resultpanel.AddView((android.view.View)(_lblridertime.getObject()),mostCurrent._lbltime.getLeft(),(int) (0),mostCurrent._lbltime.getWidth(),_resultpanel.getHeight());
 }
};
 //BA.debugLineNum = 1355;BA.debugLine="pnlResults.Height = (placedPanels * panelHeight)";
mostCurrent._pnlresults.setHeight((int) ((_placedpanels*_panelheight)));
 //BA.debugLineNum = 1357;BA.debugLine="SVResults.ScrollPosition = currentScroll";
mostCurrent._svresults.setScrollPosition(_currentscroll);
 //BA.debugLineNum = 1358;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1359;BA.debugLine="End Sub";
return "";
}
public static String  _getrefreshinterval() throws Exception{
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
 //BA.debugLineNum = 121;BA.debugLine="Sub GetRefreshInterval";
 //BA.debugLineNum = 122;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 123;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trgUrl12 FROM t";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trgUrl12 FROM trGeneral")));
 //BA.debugLineNum = 125;BA.debugLine="If crs.RowCount = 0 Then";
if (_crs.getRowCount()==0) { 
 //BA.debugLineNum = 126;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 129;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 131;BA.debugLine="If crs.GetInt(\"trgUrl12\") > 0 Then";
if (_crs.GetInt("trgUrl12")>0) { 
 //BA.debugLineNum = 132;BA.debugLine="refreshTimer.Initialize(\"refreshTimer\", crs.GetI";
_refreshtimer.Initialize(processBA,"refreshTimer",(long) (_crs.GetInt("trgUrl12")*1000));
 //BA.debugLineNum = 133;BA.debugLine="refreshTimer.Enabled = True";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 135;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 12;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 15;BA.debugLine="Private pnlFooter As Panel";
mostCurrent._pnlfooter = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 16;BA.debugLine="Private pnlHeader As Panel";
mostCurrent._pnlheader = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 17;BA.debugLine="Private pnlTabs As Panel";
mostCurrent._pnltabs = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Private pnlTabs2 As Panel";
mostCurrent._pnltabs2 = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 19;BA.debugLine="Private pnlQualificationBar As Panel";
mostCurrent._pnlqualificationbar = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Private pnlDay1Bar As Panel";
mostCurrent._pnlday1bar = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private pnlDay2Bar As Panel";
mostCurrent._pnlday2bar = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private pnlHeadersStart As Panel";
mostCurrent._pnlheadersstart = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 24;BA.debugLine="Private imgMainLogo As ImageView";
mostCurrent._imgmainlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Private imgLeftLogo As ImageView";
mostCurrent._imgleftlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 26;BA.debugLine="Private imgRightLogo As ImageView";
mostCurrent._imgrightlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 27;BA.debugLine="Private imgHome As ImageView";
mostCurrent._imghome = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 28;BA.debugLine="Private imgArrowStart As ImageView";
mostCurrent._imgarrowstart = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 29;BA.debugLine="Private imgArrowLive As ImageView";
mostCurrent._imgarrowlive = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 31;BA.debugLine="Private lblEventName As Label";
mostCurrent._lbleventname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 32;BA.debugLine="Private lblQualification As Label";
mostCurrent._lblqualification = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 33;BA.debugLine="Private lblDay1 As Label";
mostCurrent._lblday1 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 34;BA.debugLine="Private lblDay2 As Label";
mostCurrent._lblday2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 35;BA.debugLine="Private lblStart As Label";
mostCurrent._lblstart = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 36;BA.debugLine="Private lblLive As Label";
mostCurrent._lbllive = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 37;BA.debugLine="Private lblSupport As Label";
mostCurrent._lblsupport = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Private lblNo As Label";
mostCurrent._lblno = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 40;BA.debugLine="Private lblRider As Label";
mostCurrent._lblrider = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 41;BA.debugLine="Private lblClass As Label";
mostCurrent._lblclass = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 42;BA.debugLine="Private lblTime As Label";
mostCurrent._lbltime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 43;BA.debugLine="Private lblPos As Label";
mostCurrent._lblpos = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 44;BA.debugLine="Private lblTotal As Label";
mostCurrent._lbltotal = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 45;BA.debugLine="Private lblNoData As Label";
mostCurrent._lblnodata = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 47;BA.debugLine="Private SVResults As ScrollView";
mostCurrent._svresults = new anywheresoftware.b4a.objects.ScrollViewWrapper();
 //BA.debugLineNum = 48;BA.debugLine="Dim pnlResults As Panel";
mostCurrent._pnlresults = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 50;BA.debugLine="Dim isQualification As Boolean = False";
_isqualification = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 51;BA.debugLine="Dim isDayOne As Boolean = False";
_isdayone = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 52;BA.debugLine="Dim isStart As Boolean = False";
_isstart = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 54;BA.debugLine="Dim whiteArrow As BitmapDrawable";
mostCurrent._whitearrow = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 55;BA.debugLine="Dim greyArrow As BitmapDrawable";
mostCurrent._greyarrow = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 57;BA.debugLine="Dim queryType As Int '0 = Qualification Live, 1 =";
_querytype = 0;
 //BA.debugLineNum = 60;BA.debugLine="Dim currentScroll As Int";
_currentscroll = 0;
 //BA.debugLineNum = 61;BA.debugLine="End Sub";
return "";
}
public static String  _imgarrowlive_click() throws Exception{
 //BA.debugLineNum = 1397;BA.debugLine="Sub imgArrowLive_Click";
 //BA.debugLineNum = 1398;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1399;BA.debugLine="SetActiveTab(isQualification, isDayOne, False)";
_setactivetab(_isqualification,_isdayone,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 1400;BA.debugLine="End Sub";
return "";
}
public static String  _imgarrowstart_click() throws Exception{
 //BA.debugLineNum = 1392;BA.debugLine="Sub imgArrowStart_Click";
 //BA.debugLineNum = 1393;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1394;BA.debugLine="SetActiveTab(isQualification, isDayOne, True)";
_setactivetab(_isqualification,_isdayone,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1395;BA.debugLine="End Sub";
return "";
}
public static String  _imghome_click() throws Exception{
 //BA.debugLineNum = 1402;BA.debugLine="Sub imgHome_Click";
 //BA.debugLineNum = 1403;BA.debugLine="StartActivity(Main)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._main.getObject()));
 //BA.debugLineNum = 1404;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 1405;BA.debugLine="End Sub";
return "";
}
public static String  _jobdone(trialGP.live.httpjob _job) throws Exception{
 //BA.debugLineNum = 397;BA.debugLine="Sub JobDone (Job As HttpJob)";
 //BA.debugLineNum = 399;BA.debugLine="If Job.Success = False Then";
if (_job._success==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 401;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"Job0","Job1","Job2","Job3","Job4","Job5")) {
case 0: {
 //BA.debugLineNum = 403;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 break; }
case 1: {
 //BA.debugLineNum = 405;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 break; }
case 2: {
 //BA.debugLineNum = 407;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 break; }
case 3: {
 //BA.debugLineNum = 409;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 break; }
case 4: {
 //BA.debugLineNum = 411;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 break; }
case 5: {
 //BA.debugLineNum = 413;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 break; }
}
;
 }else {
 //BA.debugLineNum = 417;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"Job0","Job1","Job2","Job3","Job4","Job5")) {
case 0: {
 //BA.debugLineNum = 419;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 420;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 }else {
 //BA.debugLineNum = 422;BA.debugLine="ParseJSONReplies(\"qualificationLive\", Job.GetS";
_parsejsonreplies("qualificationLive",_job._getstring());
 };
 break; }
case 1: {
 //BA.debugLineNum = 425;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 426;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 }else {
 //BA.debugLineNum = 428;BA.debugLine="ParseJSONReplies(\"dayOneStart\", Job.GetString)";
_parsejsonreplies("dayOneStart",_job._getstring());
 };
 break; }
case 2: {
 //BA.debugLineNum = 432;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 433;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 }else {
 //BA.debugLineNum = 435;BA.debugLine="ParseJSONReplies(\"dayOneLive\", Job.GetString)";
_parsejsonreplies("dayOneLive",_job._getstring());
 };
 break; }
case 3: {
 //BA.debugLineNum = 438;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 439;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 }else {
 //BA.debugLineNum = 441;BA.debugLine="ParseJSONReplies(\"dayTwoStart\", Job.GetString)";
_parsejsonreplies("dayTwoStart",_job._getstring());
 };
 break; }
case 4: {
 //BA.debugLineNum = 445;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 446;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 }else {
 //BA.debugLineNum = 448;BA.debugLine="ParseJSONReplies(\"dayTwoLive\", Job.GetString)";
_parsejsonreplies("dayTwoLive",_job._getstring());
 };
 break; }
case 5: {
 //BA.debugLineNum = 452;BA.debugLine="If Job.GetString = \"\" Then";
if ((_job._getstring()).equals("")) { 
 //BA.debugLineNum = 453;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 }else {
 //BA.debugLineNum = 455;BA.debugLine="ParseJSONReplies(\"qualificationStart\", Job.Get";
_parsejsonreplies("qualificationStart",_job._getstring());
 };
 break; }
}
;
 };
 //BA.debugLineNum = 460;BA.debugLine="Job.Release";
_job._release();
 //BA.debugLineNum = 461;BA.debugLine="End Sub";
return "";
}
public static String  _lblday1_click() throws Exception{
 //BA.debugLineNum = 1367;BA.debugLine="Sub lblDay1_Click";
 //BA.debugLineNum = 1368;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1369;BA.debugLine="SetActiveTab(False, True, isStart)";
_setactivetab(anywheresoftware.b4a.keywords.Common.False,anywheresoftware.b4a.keywords.Common.True,_isstart);
 //BA.debugLineNum = 1370;BA.debugLine="End Sub";
return "";
}
public static String  _lblday2_click() throws Exception{
 //BA.debugLineNum = 1372;BA.debugLine="Sub lblDay2_Click";
 //BA.debugLineNum = 1373;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1374;BA.debugLine="SetActiveTab(False, False, isStart)";
_setactivetab(anywheresoftware.b4a.keywords.Common.False,anywheresoftware.b4a.keywords.Common.False,_isstart);
 //BA.debugLineNum = 1375;BA.debugLine="End Sub";
return "";
}
public static String  _lbllive_click() throws Exception{
 //BA.debugLineNum = 1387;BA.debugLine="Sub lblLive_Click";
 //BA.debugLineNum = 1388;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1389;BA.debugLine="SetActiveTab(isQualification, isDayOne, False)";
_setactivetab(_isqualification,_isdayone,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 1390;BA.debugLine="End Sub";
return "";
}
public static String  _lblqualification_click() throws Exception{
 //BA.debugLineNum = 1377;BA.debugLine="Sub lblQualification_Click";
 //BA.debugLineNum = 1378;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1379;BA.debugLine="SetActiveTab(True, False, isStart)";
_setactivetab(anywheresoftware.b4a.keywords.Common.True,anywheresoftware.b4a.keywords.Common.False,_isstart);
 //BA.debugLineNum = 1380;BA.debugLine="End Sub";
return "";
}
public static String  _lblstart_click() throws Exception{
 //BA.debugLineNum = 1382;BA.debugLine="Sub lblStart_Click";
 //BA.debugLineNum = 1383;BA.debugLine="currentScroll = 0";
_currentscroll = (int) (0);
 //BA.debugLineNum = 1384;BA.debugLine="SetActiveTab(isQualification, isDayOne, True)";
_setactivetab(_isqualification,_isdayone,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1385;BA.debugLine="End Sub";
return "";
}
public static String  _parsejsonreplies(String _mode,String _reply) throws Exception{
boolean _success = false;
anywheresoftware.b4a.objects.collections.JSONParser _jpar = null;
anywheresoftware.b4a.objects.collections.Map _m = null;
String _updatedon = "";
int _n = 0;
int _i = 0;
anywheresoftware.b4a.objects.collections.Map _tmpmap = null;
String[] _stuntsone = null;
String[] _stuntstwo = null;
anywheresoftware.b4a.objects.collections.Map _stuntsmap = null;
int _k = 0;
int _pos = 0;
 //BA.debugLineNum = 463;BA.debugLine="Sub ParseJSONReplies(mode As String, reply As Stri";
 //BA.debugLineNum = 464;BA.debugLine="Dim success As Boolean = False";
_success = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 466;BA.debugLine="If reply = \"\" Then";
if ((_reply).equals("")) { 
 //BA.debugLineNum = 467;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 468;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 471;BA.debugLine="Dim jPar As JSONParser";
_jpar = new anywheresoftware.b4a.objects.collections.JSONParser();
 //BA.debugLineNum = 472;BA.debugLine="Dim m As Map";
_m = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 473;BA.debugLine="jPar.Initialize(reply)";
_jpar.Initialize(_reply);
 //BA.debugLineNum = 474;BA.debugLine="m = jPar.NextObject";
_m = _jpar.NextObject();
 //BA.debugLineNum = 476;BA.debugLine="Log(\"RESULT: \" & m.Get(\"Result\"))";
anywheresoftware.b4a.keywords.Common.Log("RESULT: "+BA.ObjectToString(_m.Get((Object)("Result"))));
 //BA.debugLineNum = 478;BA.debugLine="If m.Get(\"Result\") <> \"0\" Then";
if ((_m.Get((Object)("Result"))).equals((Object)("0")) == false) { 
 //BA.debugLineNum = 479;BA.debugLine="If m.Get(\"Result\") = \"2\" Then";
if ((_m.Get((Object)("Result"))).equals((Object)("2"))) { 
 //BA.debugLineNum = 480;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 482;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trQuStart";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trQuStart");
 //BA.debugLineNum = 483;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 break; }
case 1: {
 //BA.debugLineNum = 485;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trQuLive\"";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trQuLive");
 //BA.debugLineNum = 486;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 break; }
case 2: {
 //BA.debugLineNum = 488;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDayStar";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDayStart");
 //BA.debugLineNum = 489;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 break; }
case 3: {
 //BA.debugLineNum = 491;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDayLive";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDayLive");
 //BA.debugLineNum = 492;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 break; }
case 4: {
 //BA.debugLineNum = 494;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDay2Sta";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDay2Start");
 //BA.debugLineNum = 495;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 break; }
case 5: {
 //BA.debugLineNum = 497;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDay2Liv";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDay2Live");
 //BA.debugLineNum = 498;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 break; }
}
;
 }else {
 //BA.debugLineNum = 501;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 503;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 break; }
case 1: {
 //BA.debugLineNum = 505;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 break; }
case 2: {
 //BA.debugLineNum = 507;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 break; }
case 3: {
 //BA.debugLineNum = 509;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 break; }
case 4: {
 //BA.debugLineNum = 511;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 break; }
case 5: {
 //BA.debugLineNum = 513;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 break; }
}
;
 };
 //BA.debugLineNum = 516;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 517;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 520;BA.debugLine="Dim updatedOn As String";
_updatedon = "";
 //BA.debugLineNum = 521;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 525;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 526;BA.debugLine="For i = 0 To n";
{
final int step56 = 1;
final int limit56 = _n;
for (_i = (int) (0) ; (step56 > 0 && _i <= limit56) || (step56 < 0 && _i >= limit56); _i = ((int)(0 + _i + step56)) ) {
 //BA.debugLineNum = 528;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 529;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 530;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 533;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 535;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 536;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 537;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trRi";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trRiders (trrCode, trrSurname, trrClass, trrTeam, trrName, trrNationality) VALUES (?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("surname")),_tmpmap.Get((Object)("motoClass")),_tmpmap.Get((Object)("team")),_tmpmap.Get((Object)("name")),_tmpmap.Get((Object)("nationality"))}));
 //BA.debugLineNum = 539;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trQu";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trQuStart (trqsCode, trqsPosition, trqstime) VALUES (?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time"))}));
 };
 }
};
 break; }
case 1: {
 //BA.debugLineNum = 545;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 546;BA.debugLine="For i = 0 To n";
{
final int step72 = 1;
final int limit72 = _n;
for (_i = (int) (0) ; (step72 > 0 && _i <= limit72) || (step72 < 0 && _i >= limit72); _i = ((int)(0 + _i + step72)) ) {
 //BA.debugLineNum = 548;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 549;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 550;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 553;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 556;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 557;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 558;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trQ";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trQuLive (trqlCode, trqlPosition, trqlTime, trqlScore, trqlIndex) VALUES (?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time")),_tmpmap.Get((Object)("score")),_tmpmap.Get((Object)("index"))}));
 };
 }
};
 break; }
case 2: {
 //BA.debugLineNum = 564;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 565;BA.debugLine="For i = 0 To n";
{
final int step87 = 1;
final int limit87 = _n;
for (_i = (int) (0) ; (step87 > 0 && _i <= limit87) || (step87 < 0 && _i >= limit87); _i = ((int)(0 + _i + step87)) ) {
 //BA.debugLineNum = 567;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 568;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 569;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 572;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 575;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 576;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 577;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trD";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trDayStart (trdsCode, trdsPosition, trdsTime) VALUES (?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time"))}));
 };
 }
};
 break; }
case 3: {
 //BA.debugLineNum = 583;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 584;BA.debugLine="For i = 0 To n";
{
final int step102 = 1;
final int limit102 = _n;
for (_i = (int) (0) ; (step102 > 0 && _i <= limit102) || (step102 < 0 && _i >= limit102); _i = ((int)(0 + _i + step102)) ) {
 //BA.debugLineNum = 586;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 587;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 588;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 591;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 594;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 595;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 597;BA.debugLine="Dim stuntsOne(15) As String";
_stuntsone = new String[(int) (15)];
java.util.Arrays.fill(_stuntsone,"");
 //BA.debugLineNum = 598;BA.debugLine="Dim stuntsTwo(15) As String";
_stuntstwo = new String[(int) (15)];
java.util.Arrays.fill(_stuntstwo,"");
 //BA.debugLineNum = 599;BA.debugLine="If tmpMap.ContainsKey(\"one\") Then";
if (_tmpmap.ContainsKey((Object)("one"))) { 
 //BA.debugLineNum = 600;BA.debugLine="Dim stuntsMap As Map";
_stuntsmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 601;BA.debugLine="stuntsMap = tmpMap.Get(\"one\")";
_stuntsmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_tmpmap.Get((Object)("one"))));
 //BA.debugLineNum = 602;BA.debugLine="For k = 0 To stuntsMap.Size - 1";
{
final int step117 = 1;
final int limit117 = (int) (_stuntsmap.getSize()-1);
for (_k = (int) (0) ; (step117 > 0 && _k <= limit117) || (step117 < 0 && _k >= limit117); _k = ((int)(0 + _k + step117)) ) {
 //BA.debugLineNum = 603;BA.debugLine="Dim pos As Int = stuntsMap.GetKeyAt(k)";
_pos = (int)(BA.ObjectToNumber(_stuntsmap.GetKeyAt(_k)));
 //BA.debugLineNum = 604;BA.debugLine="pos = pos -1";
_pos = (int) (_pos-1);
 //BA.debugLineNum = 605;BA.debugLine="stuntsOne(pos) = stuntsMap.GetValueAt(k)";
_stuntsone[_pos] = BA.ObjectToString(_stuntsmap.GetValueAt(_k));
 }
};
 };
 //BA.debugLineNum = 609;BA.debugLine="If tmpMap.ContainsKey(\"two\") Then";
if (_tmpmap.ContainsKey((Object)("two"))) { 
 //BA.debugLineNum = 610;BA.debugLine="Dim stuntsMap As Map";
_stuntsmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 611;BA.debugLine="stuntsMap = tmpMap.Get(\"two\")";
_stuntsmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_tmpmap.Get((Object)("two"))));
 //BA.debugLineNum = 612;BA.debugLine="For k = 0 To stuntsMap.Size - 1";
{
final int step126 = 1;
final int limit126 = (int) (_stuntsmap.getSize()-1);
for (_k = (int) (0) ; (step126 > 0 && _k <= limit126) || (step126 < 0 && _k >= limit126); _k = ((int)(0 + _k + step126)) ) {
 //BA.debugLineNum = 613;BA.debugLine="Dim pos As Int = stuntsMap.GetKeyAt(k)";
_pos = (int)(BA.ObjectToNumber(_stuntsmap.GetKeyAt(_k)));
 //BA.debugLineNum = 614;BA.debugLine="pos = pos -1";
_pos = (int) (_pos-1);
 //BA.debugLineNum = 615;BA.debugLine="stuntsTwo(pos) = stuntsMap.GetValueAt(k)";
_stuntstwo[_pos] = BA.ObjectToString(_stuntsmap.GetValueAt(_k));
 }
};
 };
 //BA.debugLineNum = 619;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trD";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trDayLive (trdlCode, trdlL1, trdlL2, trdlPosition, trdlIndex, trdlPenalty, trdlTotal, trdlStunt1, trdlStunt2, trdlStunt3, trdlStunt4, trdlStunt5, trdlStunt6, trdlStunt7, trdlStunt8, trdlStunt9, trdlStunt10, trdlStunt11, trdlStunt12, trdlStunt13, trdlStunt14, trdlStunt15) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("l1")),_tmpmap.Get((Object)("l2")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("index")),_tmpmap.Get((Object)("penalty")),_tmpmap.Get((Object)("total")),(Object)(_stuntsone[(int) (0)]+"@@"+_stuntstwo[(int) (0)]),(Object)(_stuntsone[(int) (1)]+"@@"+_stuntstwo[(int) (1)]),(Object)(_stuntsone[(int) (2)]+"@@"+_stuntstwo[(int) (2)]),(Object)(_stuntsone[(int) (3)]+"@@"+_stuntstwo[(int) (3)]),(Object)(_stuntsone[(int) (4)]+"@@"+_stuntstwo[(int) (4)]),(Object)(_stuntsone[(int) (5)]+"@@"+_stuntstwo[(int) (5)]),(Object)(_stuntsone[(int) (6)]+"@@"+_stuntstwo[(int) (6)]),(Object)(_stuntsone[(int) (7)]+"@@"+_stuntstwo[(int) (7)]),(Object)(_stuntsone[(int) (8)]+"@@"+_stuntstwo[(int) (8)]),(Object)(_stuntsone[(int) (9)]+"@@"+_stuntstwo[(int) (9)]),(Object)(_stuntsone[(int) (10)]+"@@"+_stuntstwo[(int) (10)]),(Object)(_stuntsone[(int) (11)]+"@@"+_stuntstwo[(int) (11)]),(Object)(_stuntsone[(int) (12)]+"@@"+_stuntstwo[(int) (12)]),(Object)(_stuntsone[(int) (13)]+"@@"+_stuntstwo[(int) (13)]),(Object)(_stuntsone[(int) (14)]+"@@"+_stuntstwo[(int) (14)])}));
 };
 }
};
 break; }
case 4: {
 //BA.debugLineNum = 625;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 626;BA.debugLine="For i = 0 To n";
{
final int step137 = 1;
final int limit137 = _n;
for (_i = (int) (0) ; (step137 > 0 && _i <= limit137) || (step137 < 0 && _i >= limit137); _i = ((int)(0 + _i + step137)) ) {
 //BA.debugLineNum = 628;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 629;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 630;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 633;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 636;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 637;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 638;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trD";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trDay2Start (trds2Code, trds2Position, trds2Time) VALUES (?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("time"))}));
 };
 }
};
 break; }
case 5: {
 //BA.debugLineNum = 644;BA.debugLine="Dim n As Int = m.Size - 1";
_n = (int) (_m.getSize()-1);
 //BA.debugLineNum = 645;BA.debugLine="For i = 0 To n";
{
final int step152 = 1;
final int limit152 = _n;
for (_i = (int) (0) ; (step152 > 0 && _i <= limit152) || (step152 < 0 && _i >= limit152); _i = ((int)(0 + _i + step152)) ) {
 //BA.debugLineNum = 647;BA.debugLine="If m.GetKeyAt(i) = \"Result\" Then";
if ((_m.GetKeyAt(_i)).equals((Object)("Result"))) { 
 //BA.debugLineNum = 648;BA.debugLine="If m.GetValueAt(i) = \"0\" Then";
if ((_m.GetValueAt(_i)).equals((Object)("0"))) { 
 //BA.debugLineNum = 649;BA.debugLine="success = True";
_success = anywheresoftware.b4a.keywords.Common.True;
 };
 }else if((_m.GetKeyAt(_i)).equals((Object)("mtime"))) { 
 //BA.debugLineNum = 652;BA.debugLine="updatedOn = m.GetValueAt(i)";
_updatedon = BA.ObjectToString(_m.GetValueAt(_i));
 }else {
 //BA.debugLineNum = 655;BA.debugLine="Dim tmpMap As Map";
_tmpmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 656;BA.debugLine="tmpMap = m.GetValueAt(i)";
_tmpmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_m.GetValueAt(_i)));
 //BA.debugLineNum = 658;BA.debugLine="Dim stuntsOne(15) As String";
_stuntsone = new String[(int) (15)];
java.util.Arrays.fill(_stuntsone,"");
 //BA.debugLineNum = 659;BA.debugLine="Dim stuntsTwo(15) As String";
_stuntstwo = new String[(int) (15)];
java.util.Arrays.fill(_stuntstwo,"");
 //BA.debugLineNum = 660;BA.debugLine="If tmpMap.ContainsKey(\"one\") Then";
if (_tmpmap.ContainsKey((Object)("one"))) { 
 //BA.debugLineNum = 661;BA.debugLine="Dim stuntsMap As Map";
_stuntsmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 662;BA.debugLine="stuntsMap = tmpMap.Get(\"one\")";
_stuntsmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_tmpmap.Get((Object)("one"))));
 //BA.debugLineNum = 663;BA.debugLine="For k = 0 To stuntsMap.Size - 1";
{
final int step167 = 1;
final int limit167 = (int) (_stuntsmap.getSize()-1);
for (_k = (int) (0) ; (step167 > 0 && _k <= limit167) || (step167 < 0 && _k >= limit167); _k = ((int)(0 + _k + step167)) ) {
 //BA.debugLineNum = 664;BA.debugLine="Dim pos As Int = stuntsMap.GetKeyAt(k)";
_pos = (int)(BA.ObjectToNumber(_stuntsmap.GetKeyAt(_k)));
 //BA.debugLineNum = 665;BA.debugLine="pos = pos -1";
_pos = (int) (_pos-1);
 //BA.debugLineNum = 666;BA.debugLine="stuntsOne(pos) = stuntsMap.GetValueAt(k)";
_stuntsone[_pos] = BA.ObjectToString(_stuntsmap.GetValueAt(_k));
 }
};
 };
 //BA.debugLineNum = 670;BA.debugLine="If tmpMap.ContainsKey(\"two\") Then";
if (_tmpmap.ContainsKey((Object)("two"))) { 
 //BA.debugLineNum = 671;BA.debugLine="Dim stuntsMap As Map";
_stuntsmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 672;BA.debugLine="stuntsMap = tmpMap.Get(\"two\")";
_stuntsmap.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(_tmpmap.Get((Object)("two"))));
 //BA.debugLineNum = 673;BA.debugLine="For k = 0 To stuntsMap.Size - 1";
{
final int step176 = 1;
final int limit176 = (int) (_stuntsmap.getSize()-1);
for (_k = (int) (0) ; (step176 > 0 && _k <= limit176) || (step176 < 0 && _k >= limit176); _k = ((int)(0 + _k + step176)) ) {
 //BA.debugLineNum = 674;BA.debugLine="Dim pos As Int = stuntsMap.GetKeyAt(k)";
_pos = (int)(BA.ObjectToNumber(_stuntsmap.GetKeyAt(_k)));
 //BA.debugLineNum = 675;BA.debugLine="pos = pos -1";
_pos = (int) (_pos-1);
 //BA.debugLineNum = 676;BA.debugLine="stuntsTwo(pos) = stuntsMap.GetValueAt(k)";
_stuntstwo[_pos] = BA.ObjectToString(_stuntsmap.GetValueAt(_k));
 }
};
 };
 //BA.debugLineNum = 680;BA.debugLine="Main.SQL1.AddNonQueryToBatch(\"INSERT INTO trD";
mostCurrent._main._sql1.AddNonQueryToBatch("INSERT INTO trDay2Live (trdl2Code, trdl2L1, trdl2L2, trdl2Position, trdl2Index, trdl2Penalty, trdl2Total, trdl2Stunt1, trdl2Stunt2, trdl2Stunt3, trdl2Stunt4, trdl2Stunt5, trdl2Stunt6, trdl2Stunt7, trdl2Stunt8, trdl2Stunt9, trdl2Stunt10, trdl2Stunt11, trdl2Stunt12, trdl2Stunt13, trdl2Stunt14, trdl2Stunt15) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{_tmpmap.Get((Object)("code")),_tmpmap.Get((Object)("l1")),_tmpmap.Get((Object)("l2")),_tmpmap.Get((Object)("position")),_tmpmap.Get((Object)("index")),_tmpmap.Get((Object)("penalty")),_tmpmap.Get((Object)("total")),(Object)(_stuntsone[(int) (0)]+"@@"+_stuntstwo[(int) (0)]),(Object)(_stuntsone[(int) (1)]+"@@"+_stuntstwo[(int) (1)]),(Object)(_stuntsone[(int) (2)]+"@@"+_stuntstwo[(int) (2)]),(Object)(_stuntsone[(int) (3)]+"@@"+_stuntstwo[(int) (3)]),(Object)(_stuntsone[(int) (4)]+"@@"+_stuntstwo[(int) (4)]),(Object)(_stuntsone[(int) (5)]+"@@"+_stuntstwo[(int) (5)]),(Object)(_stuntsone[(int) (6)]+"@@"+_stuntstwo[(int) (6)]),(Object)(_stuntsone[(int) (7)]+"@@"+_stuntstwo[(int) (7)]),(Object)(_stuntsone[(int) (8)]+"@@"+_stuntstwo[(int) (8)]),(Object)(_stuntsone[(int) (9)]+"@@"+_stuntstwo[(int) (9)]),(Object)(_stuntsone[(int) (10)]+"@@"+_stuntstwo[(int) (10)]),(Object)(_stuntsone[(int) (11)]+"@@"+_stuntstwo[(int) (11)]),(Object)(_stuntsone[(int) (12)]+"@@"+_stuntstwo[(int) (12)]),(Object)(_stuntsone[(int) (13)]+"@@"+_stuntstwo[(int) (13)]),(Object)(_stuntsone[(int) (14)]+"@@"+_stuntstwo[(int) (14)])}));
 };
 }
};
 break; }
}
;
 //BA.debugLineNum = 686;BA.debugLine="If success Then";
if (_success) { 
 //BA.debugLineNum = 687;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 689;BA.debugLine="queryType = 5";
_querytype = (int) (5);
 //BA.debugLineNum = 690;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trQuStart\"";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trQuStart");
 //BA.debugLineNum = 692;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trRiders\")";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trRiders");
 //BA.debugLineNum = 694;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 696;BA.debugLine="Log(\"UPDATED ON: \" & updatedOn)";
anywheresoftware.b4a.keywords.Common.Log("UPDATED ON: "+_updatedon);
 //BA.debugLineNum = 697;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trQuStart SET t";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trQuStart SET trqsUpdatedOn = '"+_updatedon+"'");
 break; }
case 1: {
 //BA.debugLineNum = 699;BA.debugLine="queryType = 0";
_querytype = (int) (0);
 //BA.debugLineNum = 700;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trQuLive\")";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trQuLive");
 //BA.debugLineNum = 701;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 703;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trQuLive SET tr";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trQuLive SET trqlUpdatedOn = '"+_updatedon+"'");
 break; }
case 2: {
 //BA.debugLineNum = 705;BA.debugLine="queryType = 1";
_querytype = (int) (1);
 //BA.debugLineNum = 706;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDayStart";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDayStart");
 //BA.debugLineNum = 707;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 709;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trDayStart SET";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trDayStart SET trdsUpdatedOn = '"+_updatedon+"'");
 break; }
case 3: {
 //BA.debugLineNum = 711;BA.debugLine="queryType = 2";
_querytype = (int) (2);
 //BA.debugLineNum = 712;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDayLive\"";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDayLive");
 //BA.debugLineNum = 713;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 715;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trDayLive SET t";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trDayLive SET trdlUpdatedOn = '"+_updatedon+"'");
 break; }
case 4: {
 //BA.debugLineNum = 717;BA.debugLine="queryType = 3";
_querytype = (int) (3);
 //BA.debugLineNum = 718;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDay2Star";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDay2Start");
 //BA.debugLineNum = 719;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 721;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trDay2Start SET";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trDay2Start SET trds2UpdatedOn = '"+_updatedon+"'");
 break; }
case 5: {
 //BA.debugLineNum = 723;BA.debugLine="queryType = 4";
_querytype = (int) (4);
 //BA.debugLineNum = 724;BA.debugLine="Main.SQL1.ExecNonQuery(\"DELETE FROM trDay2Live";
mostCurrent._main._sql1.ExecNonQuery("DELETE FROM trDay2Live");
 //BA.debugLineNum = 725;BA.debugLine="Main.SQL1.ExecNonQueryBatch(\"SQL\")";
mostCurrent._main._sql1.ExecNonQueryBatch(processBA,"SQL");
 //BA.debugLineNum = 727;BA.debugLine="Main.SQL1.ExecNonQuery(\"UPDATE trDay2Live SET";
mostCurrent._main._sql1.ExecNonQuery("UPDATE trDay2Live SET trdl2UpdatedOn = '"+_updatedon+"'");
 break; }
}
;
 }else {
 //BA.debugLineNum = 731;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 732;BA.debugLine="Msgbox(\"Error while updating db\", \"Error!\")";
anywheresoftware.b4a.keywords.Common.Msgbox("Error while updating db","Error!",mostCurrent.activityBA);
 //BA.debugLineNum = 733;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 735;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim refreshTimer As Timer";
_refreshtimer = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 10;BA.debugLine="End Sub";
return "";
}
public static String  _refreshtimer_tick() throws Exception{
 //BA.debugLineNum = 1407;BA.debugLine="Sub refreshTimer_tick";
 //BA.debugLineNum = 1408;BA.debugLine="currentScroll = SVResults.ScrollPosition";
_currentscroll = mostCurrent._svresults.getScrollPosition();
 //BA.debugLineNum = 1409;BA.debugLine="If isStart = False Then";
if (_isstart==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 1410;BA.debugLine="SetActiveTab(isQualification, isDayOne, isStart)";
_setactivetab(_isqualification,_isdayone,_isstart);
 };
 //BA.debugLineNum = 1412;BA.debugLine="End Sub";
return "";
}
public static String  _requestresults(String _mode) throws Exception{
String _datestring = "";
anywheresoftware.b4a.sql.SQL.CursorWrapper _crs = null;
trialGP.live.httpjob _job = null;
 //BA.debugLineNum = 263;BA.debugLine="Sub RequestResults(mode As String)";
 //BA.debugLineNum = 264;BA.debugLine="Dim dateString As String";
_datestring = "";
 //BA.debugLineNum = 265;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 //BA.debugLineNum = 267;BA.debugLine="Select Case mode";
switch (BA.switchObjectToInt(_mode,"qualificationStart","qualificationLive","dayOneStart","dayOneLive","dayTwoStart","dayTwoLive")) {
case 0: {
 //BA.debugLineNum = 270;BA.debugLine="ProgressDialogShow2(\"Retrieving Qualification S";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Qualification Start List...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 273;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 274;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trqsUpdatedOn";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trqsUpdatedOn FROM trQuStart")));
 //BA.debugLineNum = 275;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 276;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 277;BA.debugLine="dateString = crs.GetString(\"trqsUpdatedOn\")";
_datestring = _crs.GetString("trqsUpdatedOn");
 //BA.debugLineNum = 279;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 280;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 285;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 286;BA.debugLine="Job.Initialize(\"Job5\", Me)";
_job._initialize(processBA,"Job5",twodaysevent.getObject());
 //BA.debugLineNum = 287;BA.debugLine="Job.PostString(Main.server & \"/QualifiersStartL";
_job._poststring(mostCurrent._main._server+"/QualifiersStartList?date="+_datestring,"");
 //BA.debugLineNum = 288;BA.debugLine="Log(\"Date Qualification Start:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Qualification Start:"+_datestring);
 break; }
case 1: {
 //BA.debugLineNum = 291;BA.debugLine="ProgressDialogShow2(\"Retrieving Qualification L";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Qualification Live Results...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 294;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 295;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trqlUpdatedOn";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trqlUpdatedOn FROM trQuLive")));
 //BA.debugLineNum = 296;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 297;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 298;BA.debugLine="dateString = crs.GetString(\"trqlUpdatedOn\")";
_datestring = _crs.GetString("trqlUpdatedOn");
 //BA.debugLineNum = 300;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 301;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 306;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 307;BA.debugLine="Job.Initialize(\"Job0\", Me)";
_job._initialize(processBA,"Job0",twodaysevent.getObject());
 //BA.debugLineNum = 308;BA.debugLine="Job.PostString(Main.server & \"/QualifiersResult";
_job._poststring(mostCurrent._main._server+"/QualifiersResults?date="+_datestring,"");
 //BA.debugLineNum = 309;BA.debugLine="Log(\"Date Qualification Live:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Qualification Live:"+_datestring);
 break; }
case 2: {
 //BA.debugLineNum = 312;BA.debugLine="ProgressDialogShow2(\"Retrieving Day1 Start List";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Day1 Start List...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 315;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 316;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trdsUpdatedOn";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trdsUpdatedOn FROM trDayStart")));
 //BA.debugLineNum = 317;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 318;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 319;BA.debugLine="dateString = crs.GetString(\"trdsUpdatedOn\")";
_datestring = _crs.GetString("trdsUpdatedOn");
 //BA.debugLineNum = 321;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 322;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 327;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 328;BA.debugLine="Job.Initialize(\"Job1\", Me)";
_job._initialize(processBA,"Job1",twodaysevent.getObject());
 //BA.debugLineNum = 329;BA.debugLine="Job.PostString(Main.server & \"/DayOneStartList?";
_job._poststring(mostCurrent._main._server+"/DayOneStartList?date="+_datestring,"");
 //BA.debugLineNum = 330;BA.debugLine="Log(\"Date Day One Start:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Day One Start:"+_datestring);
 break; }
case 3: {
 //BA.debugLineNum = 333;BA.debugLine="ProgressDialogShow2(\"Retrieving Day1 Live Resul";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Day1 Live Results...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 336;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 337;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trdlUpdatedOn";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trdlUpdatedOn FROM trDayLive")));
 //BA.debugLineNum = 338;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 339;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 340;BA.debugLine="dateString = crs.GetString(\"trdlUpdatedOn\")";
_datestring = _crs.GetString("trdlUpdatedOn");
 //BA.debugLineNum = 342;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 343;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 348;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 349;BA.debugLine="Job.Initialize(\"Job2\", Me)";
_job._initialize(processBA,"Job2",twodaysevent.getObject());
 //BA.debugLineNum = 350;BA.debugLine="Job.PostString(Main.server & \"/DayOneResults?da";
_job._poststring(mostCurrent._main._server+"/DayOneResults?date="+_datestring,"");
 //BA.debugLineNum = 351;BA.debugLine="Log(\"Date Day One Live:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Day One Live:"+_datestring);
 break; }
case 4: {
 //BA.debugLineNum = 354;BA.debugLine="ProgressDialogShow2(\"Retrieving Day2 Start List";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Day2 Start List...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 357;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 358;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trds2UpdatedO";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trds2UpdatedOn FROM trDay2Start")));
 //BA.debugLineNum = 359;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 360;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 361;BA.debugLine="dateString = crs.GetString(\"trds2UpdatedOn\")";
_datestring = _crs.GetString("trds2UpdatedOn");
 //BA.debugLineNum = 363;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 364;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 369;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 370;BA.debugLine="Job.Initialize(\"Job3\", Me)";
_job._initialize(processBA,"Job3",twodaysevent.getObject());
 //BA.debugLineNum = 371;BA.debugLine="Job.PostString(Main.server & \"/DayTwoStartList?";
_job._poststring(mostCurrent._main._server+"/DayTwoStartList?date="+_datestring,"");
 //BA.debugLineNum = 372;BA.debugLine="Log(\"Date Day 2 Start:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Day 2 Start:"+_datestring);
 break; }
case 5: {
 //BA.debugLineNum = 375;BA.debugLine="ProgressDialogShow2(\"Retrieving Day2 Live Resul";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow2(mostCurrent.activityBA,"Retrieving Day2 Live Results...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 378;BA.debugLine="Dim crs As Cursor";
_crs = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 379;BA.debugLine="crs = Main.SQL1.ExecQuery(\"SELECT trdl2UpdatedO";
_crs.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("SELECT trdl2UpdatedOn FROM trDay2Live")));
 //BA.debugLineNum = 380;BA.debugLine="If crs.RowCount > 0 Then";
if (_crs.getRowCount()>0) { 
 //BA.debugLineNum = 381;BA.debugLine="crs.Position = 0";
_crs.setPosition((int) (0));
 //BA.debugLineNum = 382;BA.debugLine="dateString = crs.GetString(\"trdl2UpdatedOn\")";
_datestring = _crs.GetString("trdl2UpdatedOn");
 //BA.debugLineNum = 384;BA.debugLine="If dateString = Null Then";
if (_datestring== null) { 
 //BA.debugLineNum = 385;BA.debugLine="dateString = \"1900-01-01T00:00:00\"";
_datestring = "1900-01-01T00:00:00";
 };
 };
 //BA.debugLineNum = 390;BA.debugLine="Dim Job As HttpJob";
_job = new trialGP.live.httpjob();
 //BA.debugLineNum = 391;BA.debugLine="Job.Initialize(\"Job4\", Me)";
_job._initialize(processBA,"Job4",twodaysevent.getObject());
 //BA.debugLineNum = 392;BA.debugLine="Job.PostString(Main.server & \"/DayTwoResults?da";
_job._poststring(mostCurrent._main._server+"/DayTwoResults?date="+_datestring,"");
 //BA.debugLineNum = 393;BA.debugLine="Log(\"Date Day 2 Live:\" & dateString)";
anywheresoftware.b4a.keywords.Common.Log("Date Day 2 Live:"+_datestring);
 break; }
}
;
 //BA.debugLineNum = 395;BA.debugLine="End Sub";
return "";
}
public static String  _setactivetab(boolean _isqual,boolean _isday1,boolean _isst) throws Exception{
 //BA.debugLineNum = 137;BA.debugLine="Sub SetActiveTab(isQual As Boolean, isDay1 As Bool";
 //BA.debugLineNum = 138;BA.debugLine="refreshTimer.Enabled = False";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 139;BA.debugLine="If refreshTimer.Interval > 0 Then";
if (_refreshtimer.getInterval()>0) { 
 //BA.debugLineNum = 140;BA.debugLine="refreshTimer.Enabled = True";
_refreshtimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 143;BA.debugLine="isQualification = isQual";
_isqualification = _isqual;
 //BA.debugLineNum = 144;BA.debugLine="isStart = isSt";
_isstart = _isst;
 //BA.debugLineNum = 145;BA.debugLine="isDayOne = isDay1";
_isdayone = _isday1;
 //BA.debugLineNum = 147;BA.debugLine="SetupLabels";
_setuplabels();
 //BA.debugLineNum = 150;BA.debugLine="If isQual Then";
if (_isqual) { 
 //BA.debugLineNum = 151;BA.debugLine="lblQualification.TextColor = Colors.RGB(34, 180,";
mostCurrent._lblqualification.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 152;BA.debugLine="pnlQualificationBar.Color = Colors.RGB(34, 180,";
mostCurrent._pnlqualificationbar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 153;BA.debugLine="lblDay1.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblday1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 154;BA.debugLine="lblDay2.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblday2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 155;BA.debugLine="pnlDay1Bar.Color = Colors.RGB(106, 107, 108)";
mostCurrent._pnlday1bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 156;BA.debugLine="pnlDay2Bar.Color = Colors.RGB(106, 107, 108)";
mostCurrent._pnlday2bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 }else if(_isday1) { 
 //BA.debugLineNum = 158;BA.debugLine="lblQualification.TextColor = Colors.RGB(106, 107";
mostCurrent._lblqualification.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 159;BA.debugLine="pnlQualificationBar.Color = Colors.RGB(106, 107,";
mostCurrent._pnlqualificationbar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 160;BA.debugLine="lblDay1.TextColor = Colors.RGB(34, 180, 231)";
mostCurrent._lblday1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 161;BA.debugLine="lblDay2.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblday2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 162;BA.debugLine="pnlDay1Bar.Color = Colors.RGB(34, 180, 231)";
mostCurrent._pnlday1bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 163;BA.debugLine="pnlDay2Bar.Color = Colors.RGB(106, 107, 108)";
mostCurrent._pnlday2bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 }else {
 //BA.debugLineNum = 165;BA.debugLine="lblQualification.TextColor = Colors.RGB(106, 107";
mostCurrent._lblqualification.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 166;BA.debugLine="pnlQualificationBar.Color = Colors.RGB(106, 107,";
mostCurrent._pnlqualificationbar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 167;BA.debugLine="lblDay1.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblday1.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 168;BA.debugLine="lblDay2.TextColor = Colors.RGB(34, 180, 231)";
mostCurrent._lblday2.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 //BA.debugLineNum = 169;BA.debugLine="pnlDay1Bar.Color = Colors.RGB(106, 107, 108)";
mostCurrent._pnlday1bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 170;BA.debugLine="pnlDay2Bar.Color = Colors.RGB(34, 180, 231)";
mostCurrent._pnlday2bar.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (34),(int) (180),(int) (231)));
 };
 //BA.debugLineNum = 173;BA.debugLine="If isSt Then";
if (_isst) { 
 //BA.debugLineNum = 174;BA.debugLine="lblStart.TextColor = Colors.RGB(255, 255, 255)";
mostCurrent._lblstart.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 175;BA.debugLine="lblLive.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lbllive.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 176;BA.debugLine="imgArrowStart.Background = whiteArrow";
mostCurrent._imgarrowstart.setBackground((android.graphics.drawable.Drawable)(mostCurrent._whitearrow.getObject()));
 //BA.debugLineNum = 177;BA.debugLine="imgArrowLive.Background = greyArrow";
mostCurrent._imgarrowlive.setBackground((android.graphics.drawable.Drawable)(mostCurrent._greyarrow.getObject()));
 }else {
 //BA.debugLineNum = 179;BA.debugLine="lblStart.TextColor = Colors.RGB(106, 107, 108)";
mostCurrent._lblstart.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (106),(int) (107),(int) (108)));
 //BA.debugLineNum = 180;BA.debugLine="lblLive.TextColor = Colors.RGB(255, 255, 255)";
mostCurrent._lbllive.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 181;BA.debugLine="imgArrowStart.Background = greyArrow";
mostCurrent._imgarrowstart.setBackground((android.graphics.drawable.Drawable)(mostCurrent._greyarrow.getObject()));
 //BA.debugLineNum = 182;BA.debugLine="imgArrowLive.Background = whiteArrow";
mostCurrent._imgarrowlive.setBackground((android.graphics.drawable.Drawable)(mostCurrent._whitearrow.getObject()));
 };
 //BA.debugLineNum = 185;BA.debugLine="If isQual Then";
if (_isqual) { 
 //BA.debugLineNum = 186;BA.debugLine="If isSt Then";
if (_isst) { 
 //BA.debugLineNum = 188;BA.debugLine="RequestResults(\"qualificationStart\")";
_requestresults("qualificationStart");
 }else {
 //BA.debugLineNum = 190;BA.debugLine="RequestResults(\"qualificationLive\")";
_requestresults("qualificationLive");
 };
 }else if(_isdayone) { 
 //BA.debugLineNum = 193;BA.debugLine="If isSt Then";
if (_isst) { 
 //BA.debugLineNum = 194;BA.debugLine="RequestResults(\"dayOneStart\")";
_requestresults("dayOneStart");
 }else {
 //BA.debugLineNum = 196;BA.debugLine="RequestResults(\"dayOneLive\")";
_requestresults("dayOneLive");
 //BA.debugLineNum = 197;BA.debugLine="lblClass.Text = \"SECTION SCORES\"";
mostCurrent._lblclass.setText((Object)("SECTION SCORES"));
 };
 }else {
 //BA.debugLineNum = 200;BA.debugLine="If isSt Then";
if (_isst) { 
 //BA.debugLineNum = 201;BA.debugLine="RequestResults(\"dayTwoStart\")";
_requestresults("dayTwoStart");
 }else {
 //BA.debugLineNum = 203;BA.debugLine="RequestResults(\"dayTwoLive\")";
_requestresults("dayTwoLive");
 //BA.debugLineNum = 204;BA.debugLine="lblClass.Text = \"SECTION SCORES\"";
mostCurrent._lblclass.setText((Object)("SECTION SCORES"));
 };
 };
 //BA.debugLineNum = 207;BA.debugLine="End Sub";
return "";
}
public static String  _setuplabels() throws Exception{
int _labelwidth = 0;
 //BA.debugLineNum = 209;BA.debugLine="Sub SetupLabels";
 //BA.debugLineNum = 210;BA.debugLine="Dim labelWidth As Int";
_labelwidth = 0;
 //BA.debugLineNum = 211;BA.debugLine="labelWidth = pnlHeadersStart.Width / 6";
_labelwidth = (int) (mostCurrent._pnlheadersstart.getWidth()/(double)6);
 //BA.debugLineNum = 213;BA.debugLine="lblPos.Width = labelWidth / 2";
mostCurrent._lblpos.setWidth((int) (_labelwidth/(double)2));
 //BA.debugLineNum = 214;BA.debugLine="lblNo.Width = labelWidth / 2";
mostCurrent._lblno.setWidth((int) (_labelwidth/(double)2));
 //BA.debugLineNum = 215;BA.debugLine="lblRider.Width = labelWidth * 2";
mostCurrent._lblrider.setWidth((int) (_labelwidth*2));
 //BA.debugLineNum = 216;BA.debugLine="lblClass.Width = labelWidth * 2";
mostCurrent._lblclass.setWidth((int) (_labelwidth*2));
 //BA.debugLineNum = 217;BA.debugLine="lblTime.Width = labelWidth * 1.5";
mostCurrent._lbltime.setWidth((int) (_labelwidth*1.5));
 //BA.debugLineNum = 218;BA.debugLine="lblTotal.Width = labelWidth";
mostCurrent._lbltotal.setWidth(_labelwidth);
 //BA.debugLineNum = 220;BA.debugLine="lblNo.Left = lblPos.Left + lblPos.Width";
mostCurrent._lblno.setLeft((int) (mostCurrent._lblpos.getLeft()+mostCurrent._lblpos.getWidth()));
 //BA.debugLineNum = 221;BA.debugLine="lblRider.Left = lblNo.Left + lblNo.Width";
mostCurrent._lblrider.setLeft((int) (mostCurrent._lblno.getLeft()+mostCurrent._lblno.getWidth()));
 //BA.debugLineNum = 222;BA.debugLine="lblClass.Left = lblRider.Left + lblRider.Width";
mostCurrent._lblclass.setLeft((int) (mostCurrent._lblrider.getLeft()+mostCurrent._lblrider.getWidth()));
 //BA.debugLineNum = 223;BA.debugLine="lblTime.Left = lblClass.Left + lblClass.Width";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lblclass.getLeft()+mostCurrent._lblclass.getWidth()));
 //BA.debugLineNum = 225;BA.debugLine="If isQualification Then";
if (_isqualification) { 
 //BA.debugLineNum = 226;BA.debugLine="If isStart Then";
if (_isstart) { 
 //BA.debugLineNum = 227;BA.debugLine="lblPos.Visible = False";
mostCurrent._lblpos.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 228;BA.debugLine="lblTotal.Visible = False";
mostCurrent._lbltotal.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 229;BA.debugLine="lblClass.Text = \"CLASS\"";
mostCurrent._lblclass.setText((Object)("CLASS"));
 //BA.debugLineNum = 230;BA.debugLine="lblTime.Text = \"TIME\"";
mostCurrent._lbltime.setText((Object)("TIME"));
 }else {
 //BA.debugLineNum = 232;BA.debugLine="lblPos.Visible = True";
mostCurrent._lblpos.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 233;BA.debugLine="lblTotal.Visible = False";
mostCurrent._lbltotal.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 234;BA.debugLine="lblClass.Text = \"SCORE\"";
mostCurrent._lblclass.setText((Object)("SCORE"));
 //BA.debugLineNum = 235;BA.debugLine="lblTime.Text = \"TIME\"";
mostCurrent._lbltime.setText((Object)("TIME"));
 //BA.debugLineNum = 236;BA.debugLine="lblRider.Width = labelWidth * 3";
mostCurrent._lblrider.setWidth((int) (_labelwidth*3));
 //BA.debugLineNum = 237;BA.debugLine="lblClass.Width = labelWidth";
mostCurrent._lblclass.setWidth(_labelwidth);
 //BA.debugLineNum = 238;BA.debugLine="lblClass.Left = lblRider.Left + lblRider.Width";
mostCurrent._lblclass.setLeft((int) (mostCurrent._lblrider.getLeft()+mostCurrent._lblrider.getWidth()));
 //BA.debugLineNum = 239;BA.debugLine="lblTime.Left = lblClass.Left + lblClass.Width";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lblclass.getLeft()+mostCurrent._lblclass.getWidth()));
 };
 }else {
 //BA.debugLineNum = 242;BA.debugLine="If isStart Then";
if (_isstart) { 
 //BA.debugLineNum = 243;BA.debugLine="lblPos.Visible = False";
mostCurrent._lblpos.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 244;BA.debugLine="lblTotal.Visible = False";
mostCurrent._lbltotal.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 245;BA.debugLine="lblClass.Text = \"CLASS\"";
mostCurrent._lblclass.setText((Object)("CLASS"));
 //BA.debugLineNum = 246;BA.debugLine="lblTime.Text = \"TIME\"";
mostCurrent._lbltime.setText((Object)("TIME"));
 }else {
 //BA.debugLineNum = 248;BA.debugLine="lblPos.Visible = True";
mostCurrent._lblpos.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 249;BA.debugLine="lblTotal.Visible = True";
mostCurrent._lbltotal.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 250;BA.debugLine="lblClass.Text = \"SECTION SCORES\"";
mostCurrent._lblclass.setText((Object)("SECTION SCORES"));
 //BA.debugLineNum = 251;BA.debugLine="lblTime.Text = \"PEN\"";
mostCurrent._lbltime.setText((Object)("PEN"));
 //BA.debugLineNum = 252;BA.debugLine="lblRider.Width = labelWidth * 1.10";
mostCurrent._lblrider.setWidth((int) (_labelwidth*1.10));
 //BA.debugLineNum = 253;BA.debugLine="lblClass.Width = labelWidth * 2.75";
mostCurrent._lblclass.setWidth((int) (_labelwidth*2.75));
 //BA.debugLineNum = 254;BA.debugLine="lblClass.Left = lblRider.Left + lblRider.Width";
mostCurrent._lblclass.setLeft((int) (mostCurrent._lblrider.getLeft()+mostCurrent._lblrider.getWidth()));
 //BA.debugLineNum = 255;BA.debugLine="lblTime.Width = labelWidth / 2";
mostCurrent._lbltime.setWidth((int) (_labelwidth/(double)2));
 //BA.debugLineNum = 256;BA.debugLine="lblTotal.Width = labelWidth / 2";
mostCurrent._lbltotal.setWidth((int) (_labelwidth/(double)2));
 //BA.debugLineNum = 257;BA.debugLine="lblTotal.Left = pnlHeadersStart.Width - lblTota";
mostCurrent._lbltotal.setLeft((int) (mostCurrent._pnlheadersstart.getWidth()-mostCurrent._lbltotal.getWidth()));
 //BA.debugLineNum = 258;BA.debugLine="lblTime.Left = lblTotal.Left - lblTime.Width +";
mostCurrent._lbltime.setLeft((int) (mostCurrent._lbltotal.getLeft()-mostCurrent._lbltime.getWidth()+anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5))));
 };
 };
 //BA.debugLineNum = 261;BA.debugLine="End Sub";
return "";
}
public static String  _sql_nonquerycomplete(boolean _success) throws Exception{
 //BA.debugLineNum = 1251;BA.debugLine="Sub SQL_NonQueryComplete (Success As Boolean)";
 //BA.debugLineNum = 1253;BA.debugLine="If Success = False Then";
if (_success==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 1254;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 1255;BA.debugLine="Msgbox(LastException, \"Error!\")";
anywheresoftware.b4a.keywords.Common.Msgbox(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)),"Error!",mostCurrent.activityBA);
 //BA.debugLineNum = 1256;BA.debugLine="Return";
if (true) return "";
 };
 //BA.debugLineNum = 1259;BA.debugLine="Select Case queryType";
switch (_querytype) {
case 0: {
 //BA.debugLineNum = 1261;BA.debugLine="DisplayQualificationLive";
_displayqualificationlive();
 break; }
case 1: {
 //BA.debugLineNum = 1263;BA.debugLine="DisplayDayOneStart";
_displaydayonestart();
 break; }
case 2: {
 //BA.debugLineNum = 1265;BA.debugLine="DisplayDayLive(1)";
_displaydaylive((int) (1));
 break; }
case 3: {
 //BA.debugLineNum = 1267;BA.debugLine="DisplayDayTwoStart";
_displaydaytwostart();
 break; }
case 4: {
 //BA.debugLineNum = 1269;BA.debugLine="DisplayDayLive(2)";
_displaydaylive((int) (2));
 break; }
case 5: {
 //BA.debugLineNum = 1271;BA.debugLine="DisplayQualificationStart";
_displayqualificationstart();
 break; }
}
;
 //BA.debugLineNum = 1273;BA.debugLine="End Sub";
return "";
}
}
