package trialGP.live;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class misc {
private static misc mostCurrent = new misc();
public static Object getObject() {
    throw new RuntimeException("Code module does not support this method.");
}
 public anywheresoftware.b4a.keywords.Common __c = null;
public trialGP.live.main _main = null;
public trialGP.live.starter _starter = null;
public trialGP.live.onedayevent _onedayevent = null;
public trialGP.live.twodaysevent _twodaysevent = null;
public trialGP.live.httputils2service _httputils2service = null;
public static String  _horizontalcenterview(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,anywheresoftware.b4a.objects.ConcreteViewWrapper _parent) throws Exception{
 //BA.debugLineNum = 5;BA.debugLine="Sub HorizontalCenterView(v As View, parent As View";
 //BA.debugLineNum = 6;BA.debugLine="v.Left = parent.Width / 2 - v.Width / 2";
_v.setLeft((int) (_parent.getWidth()/(double)2-_v.getWidth()/(double)2));
 //BA.debugLineNum = 7;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 1;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 3;BA.debugLine="End Sub";
return "";
}
public static String  _settextshadow(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _pview,float _pradius,float _pdx,float _pdy,int _pcolor) throws Exception{
anywheresoftware.b4a.agraham.reflection.Reflection _ref = null;
 //BA.debugLineNum = 13;BA.debugLine="Sub SetTextShadow(pView As View, pRadius As Float,";
 //BA.debugLineNum = 14;BA.debugLine="Dim ref As Reflector";
_ref = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 16;BA.debugLine="ref.Target = pView";
_ref.Target = (Object)(_pview.getObject());
 //BA.debugLineNum = 17;BA.debugLine="ref.RunMethod4(\"setShadowLayer\", Array As Objec";
_ref.RunMethod4("setShadowLayer",new Object[]{(Object)(_pradius),(Object)(_pdx),(Object)(_pdy),(Object)(_pcolor)},new String[]{"java.lang.float","java.lang.float","java.lang.float","java.lang.int"});
 //BA.debugLineNum = 18;BA.debugLine="End Sub";
return "";
}
public static String  _verticalcenterview(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,anywheresoftware.b4a.objects.ConcreteViewWrapper _parent) throws Exception{
 //BA.debugLineNum = 9;BA.debugLine="Sub VerticalCenterView(v As View, parent As View)";
 //BA.debugLineNum = 10;BA.debugLine="v.Top = parent.Height / 2 - v.Height / 2";
_v.setTop((int) (_parent.getHeight()/(double)2-_v.getHeight()/(double)2));
 //BA.debugLineNum = 11;BA.debugLine="End Sub";
return "";
}
}
