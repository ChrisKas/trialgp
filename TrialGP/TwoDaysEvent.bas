﻿Type=Activity
Version=6.31
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim refreshTimer As Timer
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Private pnlFooter As Panel
	Private pnlHeader As Panel
	Private pnlTabs As Panel
	Private pnlTabs2 As Panel
	Private pnlQualificationBar As Panel
	Private pnlDay1Bar As Panel
	Private pnlDay2Bar As Panel
	Private pnlHeadersStart As Panel
	
	Private imgMainLogo As ImageView
	Private imgLeftLogo As ImageView
	Private imgRightLogo As ImageView
	Private imgHome As ImageView
	Private imgArrowStart As ImageView
	Private imgArrowLive As ImageView
	
	Private lblEventName As Label
	Private lblQualification As Label
	Private lblDay1 As Label
	Private lblDay2 As Label
	Private lblStart As Label
	Private lblLive As Label
	Private lblSupport As Label
	
	Private lblNo As Label
	Private lblRider As Label
	Private lblClass As Label
	Private lblTime As Label
	Private lblPos As Label
	Private lblTotal As Label
	Private lblNoData As Label
	
	Private SVResults As ScrollView
	Dim pnlResults As Panel
	
	Dim isQualification As Boolean = False
	Dim isDayOne As Boolean = False
	Dim isStart As Boolean = False
	
	Dim whiteArrow As BitmapDrawable
	Dim greyArrow As BitmapDrawable
	
	Dim queryType As Int '0 = Qualification Live, 1 = Trial Start, 2 = Trial Live, 3=, 4=
	
	
	Dim currentScroll As Int
End Sub

Sub Activity_Create(FirstTime As Boolean)
	If Main.SQL1.IsInitialized = False Then
		Activity.Finish
		Return
	End If
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("TwoDaysEventLayout")

	Misc.HorizontalCenterView(imgMainLogo, pnlHeader)
	
	Misc.VerticalCenterView(imgLeftLogo, pnlHeader)
	Misc.VerticalCenterView(imgRightLogo, pnlHeader)
	Misc.VerticalCenterView(imgMainLogo, pnlHeader)
	Misc.VerticalCenterView(imgHome, pnlFooter)
	
	lblQualification.Width = (pnlTabs.Width / 2) - 4dip
	lblDay1.Width = (pnlTabs.Width / 4) - 2dip
	lblDay2.Width = (pnlTabs.Width / 4) - 2dip
	pnlQualificationBar.Width = (pnlTabs.Width / 2) - 4dip
	pnlDay1Bar.Width = (pnlTabs.Width / 4) - 2dip
	pnlDay2Bar.Width = (pnlTabs.Width / 4) - 2dip
	lblDay1.Left = lblQualification.Left + lblQualification.Width
	lblDay2.Left = lblDay1.Left + lblDay1.Width
	pnlDay1Bar.Left = pnlQualificationBar.Left + pnlQualificationBar.Width
	pnlDay2Bar.Left = pnlDay1Bar.Left + pnlDay1Bar.Width
	
	lblStart.Width = (pnlTabs2.Width / 2) - 51dip
	lblLive.Width = (pnlTabs2.Width / 2) - 51dip
	lblStart.Left = imgArrowStart.Left + imgArrowStart.Width + 10dip
	imgArrowLive.Left = pnlTabs2.Width / 2 + 15dip
	lblLive.Left = imgArrowLive.Left + imgArrowLive.Width + 10dip
	
	whiteArrow.Initialize(LoadBitmap(File.DirAssets, "white-arrow.png"))
	greyArrow.Initialize(LoadBitmap(File.DirAssets, "grey-arrow.png"))

	pnlResults = SVResults.Panel
	lblEventName.Text = Main.eventName
	
	GetRefreshInterval
	
	SetActiveTab(True, False, True)
End Sub

Sub Activity_Resume
	If Main.SQL1.IsInitialized = False Then
		Activity.Finish
		Return
	End If
	
	If refreshTimer.Interval > 0 Then
		refreshTimer.Enabled = True
	End If
End Sub

Sub Activity_Pause (UserClosed As Boolean)
	refreshTimer.Enabled = False
End Sub

Sub GetRefreshInterval
	Dim crs As Cursor
	crs = Main.SQL1.ExecQuery("SELECT trgUrl12 FROM trGeneral")
	
	If crs.RowCount = 0 Then
		Return
	End If
	
	crs.Position = 0
	
	If crs.GetInt("trgUrl12") > 0 Then
		refreshTimer.Initialize("refreshTimer", crs.GetInt("trgUrl12") * 1000)
		refreshTimer.Enabled = True
	End If
End Sub

Sub SetActiveTab(isQual As Boolean, isDay1 As Boolean, isSt As Boolean)
	refreshTimer.Enabled = False
	If refreshTimer.Interval > 0 Then
		refreshTimer.Enabled = True
	End If
	
	isQualification = isQual
	isStart = isSt
	isDayOne = isDay1
	
	SetupLabels
	
	'Determine which of the following was selected: Qualification or Trial
	If isQual Then
		lblQualification.TextColor = Colors.RGB(34, 180, 231)
		pnlQualificationBar.Color = Colors.RGB(34, 180, 231)
		lblDay1.TextColor = Colors.RGB(106, 107, 108)
		lblDay2.TextColor = Colors.RGB(106, 107, 108)
		pnlDay1Bar.Color = Colors.RGB(106, 107, 108)
		pnlDay2Bar.Color = Colors.RGB(106, 107, 108)
	Else If isDay1 Then
		lblQualification.TextColor = Colors.RGB(106, 107, 108)
		pnlQualificationBar.Color = Colors.RGB(106, 107, 108)
		lblDay1.TextColor = Colors.RGB(34, 180, 231)
		lblDay2.TextColor = Colors.RGB(106, 107, 108)
		pnlDay1Bar.Color = Colors.RGB(34, 180, 231)
		pnlDay2Bar.Color = Colors.RGB(106, 107, 108)
	Else
		lblQualification.TextColor = Colors.RGB(106, 107, 108)
		pnlQualificationBar.Color = Colors.RGB(106, 107, 108)
		lblDay1.TextColor = Colors.RGB(106, 107, 108)
		lblDay2.TextColor = Colors.RGB(34, 180, 231)
		pnlDay1Bar.Color = Colors.RGB(106, 107, 108)
		pnlDay2Bar.Color = Colors.RGB(34, 180, 231)
	End If
	'Determine which of the following was selected: Start or Live
	If isSt Then
		lblStart.TextColor = Colors.RGB(255, 255, 255)
		lblLive.TextColor = Colors.RGB(106, 107, 108)
		imgArrowStart.Background = whiteArrow
		imgArrowLive.Background = greyArrow
	Else
		lblStart.TextColor = Colors.RGB(106, 107, 108)
		lblLive.TextColor = Colors.RGB(255, 255, 255)
		imgArrowStart.Background = greyArrow
		imgArrowLive.Background = whiteArrow
	End If
	
	If isQual Then
		If isSt Then
			'DisplayQualificationStart
			RequestResults("qualificationStart")
		Else
			RequestResults("qualificationLive")
		End If
	Else If isDayOne Then
		If isSt Then
			RequestResults("dayOneStart")
		Else
			RequestResults("dayOneLive")
			lblClass.Text = "SECTION SCORES"
		End If
	Else
		If isSt Then
			RequestResults("dayTwoStart")
		Else
			RequestResults("dayTwoLive")
			lblClass.Text = "SECTION SCORES"
		End If
	End If
End Sub

Sub SetupLabels
	Dim labelWidth As Int
	labelWidth = pnlHeadersStart.Width / 6
	
	lblPos.Width = labelWidth / 2
	lblNo.Width = labelWidth / 2
	lblRider.Width = labelWidth * 2
	lblClass.Width = labelWidth * 2
	lblTime.Width = labelWidth * 1.5
	lblTotal.Width = labelWidth
		
	lblNo.Left = lblPos.Left + lblPos.Width
	lblRider.Left = lblNo.Left + lblNo.Width
	lblClass.Left = lblRider.Left + lblRider.Width
	lblTime.Left = lblClass.Left + lblClass.Width
	
	If isQualification Then
		If isStart Then
			lblPos.Visible = False
			lblTotal.Visible = False
			lblClass.Text = "CLASS"
			lblTime.Text = "TIME"
		Else
			lblPos.Visible = True
			lblTotal.Visible = False
			lblClass.Text = "SCORE"
			lblTime.Text = "TIME"
			lblRider.Width = labelWidth * 3
			lblClass.Width = labelWidth
			lblClass.Left = lblRider.Left + lblRider.Width
			lblTime.Left = lblClass.Left + lblClass.Width
		End If
	Else
		If isStart Then
			lblPos.Visible = False
			lblTotal.Visible = False
			lblClass.Text = "CLASS"
			lblTime.Text = "TIME"
		Else
			lblPos.Visible = True
			lblTotal.Visible = True
			lblClass.Text = "SECTION SCORES"
			lblTime.Text = "PEN"
			lblRider.Width = labelWidth * 1.10
			lblClass.Width = labelWidth * 2.75
			lblClass.Left = lblRider.Left + lblRider.Width
			lblTime.Width = labelWidth / 2
			lblTotal.Width = labelWidth / 2
			lblTotal.Left = pnlHeadersStart.Width - lblTotal.Width
			lblTime.Left = lblTotal.Left - lblTime.Width + 5dip
		End If
	End If
End Sub

Sub RequestResults(mode As String)
	Dim dateString As String
	dateString = "1900-01-01T00:00:00"
	
	Select Case mode
		Case "qualificationStart"
			'Display progress dialog
			ProgressDialogShow2("Retrieving Qualification Start List...", False)
			
			'Get last update date&time from database
			Dim crs As Cursor
			crs = Main.SQL1.ExecQuery("SELECT trqsUpdatedOn FROM trQuStart")
			If crs.RowCount > 0 Then
				crs.Position = 0
				dateString = crs.GetString("trqsUpdatedOn")
				
				If dateString = Null Then
					dateString = "1900-01-01T00:00:00"
				End If
			End If
			
			'Send POST request to fetch qualifiers start list
			Dim Job As HttpJob
			Job.Initialize("Job5", Me)
			Job.PostString(Main.server & "/QualifiersStartList?date=" & dateString, "")
			Log("Date Qualification Start:" & dateString)
		Case "qualificationLive"
			'Display progress dialog
			ProgressDialogShow2("Retrieving Qualification Live Results...", False)
			
			'Get stored info from database
			Dim crs As Cursor
			crs = Main.SQL1.ExecQuery("SELECT trqlUpdatedOn FROM trQuLive")
			If crs.RowCount > 0 Then
				crs.Position = 0
				dateString = crs.GetString("trqlUpdatedOn")
				
				If dateString = Null Then
					dateString = "1900-01-01T00:00:00"
				End If
			End If
			
			'Send POST request to fetch qualification live results
			Dim Job As HttpJob
			Job.Initialize("Job0", Me)
			Job.PostString(Main.server & "/QualifiersResults?date=" & dateString, "")
			Log("Date Qualification Live:" & dateString)
		Case "dayOneStart"
			'Display progress dialog
			ProgressDialogShow2("Retrieving Day1 Start List...", False)
			
			'Get stored info from database
			Dim crs As Cursor
			crs = Main.SQL1.ExecQuery("SELECT trdsUpdatedOn FROM trDayStart")
			If crs.RowCount > 0 Then
				crs.Position = 0
				dateString = crs.GetString("trdsUpdatedOn")
				
				If dateString = Null Then
					dateString = "1900-01-01T00:00:00"
				End If
			End If
			
			'Send POST request to fetch trial starting list
			Dim Job As HttpJob
			Job.Initialize("Job1", Me)
			Job.PostString(Main.server & "/DayOneStartList?date=" & dateString, "")
			Log("Date Day One Start:" & dateString)
		Case "dayOneLive"
			'Display progress dialog
			ProgressDialogShow2("Retrieving Day1 Live Results...", False)
			
			'Get stored info from database
			Dim crs As Cursor
			crs = Main.SQL1.ExecQuery("SELECT trdlUpdatedOn FROM trDayLive")
			If crs.RowCount > 0 Then
				crs.Position = 0
				dateString = crs.GetString("trdlUpdatedOn")
				
				If dateString = Null Then
					dateString = "1900-01-01T00:00:00"
				End If
			End If
			
			'Send POST request to fetch order info
			Dim Job As HttpJob
			Job.Initialize("Job2", Me)
			Job.PostString(Main.server & "/DayOneResults?date=" & dateString, "")
			Log("Date Day One Live:" & dateString)
		Case "dayTwoStart"
			'Display progress dialog
			ProgressDialogShow2("Retrieving Day2 Start List...", False)
			
			'Get stored info from database
			Dim crs As Cursor
			crs = Main.SQL1.ExecQuery("SELECT trds2UpdatedOn FROM trDay2Start")
			If crs.RowCount > 0 Then
				crs.Position = 0
				dateString = crs.GetString("trds2UpdatedOn")
				
				If dateString = Null Then
					dateString = "1900-01-01T00:00:00"
				End If
			End If
			
			'Send POST request to fetch trial starting list
			Dim Job As HttpJob
			Job.Initialize("Job3", Me)
			Job.PostString(Main.server & "/DayTwoStartList?date=" & dateString, "")
			Log("Date Day 2 Start:" & dateString)
		Case "dayTwoLive"
			'Display progress dialog
			ProgressDialogShow2("Retrieving Day2 Live Results...", False)
			
			'Get stored info from database
			Dim crs As Cursor
			crs = Main.SQL1.ExecQuery("SELECT trdl2UpdatedOn FROM trDay2Live")
			If crs.RowCount > 0 Then
				crs.Position = 0
				dateString = crs.GetString("trdl2UpdatedOn")
				
				If dateString = Null Then
					dateString = "1900-01-01T00:00:00"
				End If
			End If
			
			'Send POST request to fetch trial starting list
			Dim Job As HttpJob
			Job.Initialize("Job4", Me)
			Job.PostString(Main.server & "/DayTwoResults?date=" & dateString, "")
			Log("Date Day 2 Live:" & dateString)
	End Select
End Sub

Sub JobDone (Job As HttpJob)
	'Check if job was succesful
	If Job.Success = False Then
		'Determine which job was NOT completed succesfully and act accordingly
		Select Case Job.JobName
		Case "Job0"
			DisplayQualificationLive
		Case "Job1"
			DisplayDayOneStart
		Case "Job2"
			DisplayDayLive(1)
		Case "Job3"
			DisplayDayTwoStart
		Case "Job4"
			DisplayDayLive(2)
		Case "Job5"
			DisplayQualificationStart
		End Select
	Else
		'Determine which job was completed succesfully and act accordingly
		Select Case Job.JobName
		Case "Job0"
			If Job.GetString = "" Then
				DisplayQualificationLive
			Else
				ParseJSONReplies("qualificationLive", Job.GetString)
			End If
		Case "Job1"
			If Job.GetString = "" Then
				DisplayDayOneStart
			Else
				ParseJSONReplies("dayOneStart", Job.GetString)
			End If
			
		Case "Job2"
			If Job.GetString = "" Then
				DisplayDayLive(1)
			Else
				ParseJSONReplies("dayOneLive", Job.GetString)
			End If
		Case "Job3"
			If Job.GetString = "" Then
				DisplayDayTwoStart
			Else
				ParseJSONReplies("dayTwoStart", Job.GetString)
			End If
			
		Case "Job4"
			If Job.GetString = "" Then
				DisplayDayLive(2)
			Else
				ParseJSONReplies("dayTwoLive", Job.GetString)
			End If
			
		Case "Job5"
			If Job.GetString = "" Then
				DisplayQualificationStart
			Else
				ParseJSONReplies("qualificationStart", Job.GetString)
			End If
		End Select
	End If
	
  	Job.Release
End Sub

Sub ParseJSONReplies(mode As String, reply As String)
	Dim success As Boolean = False
	
	If reply = "" Then
		ProgressDialogHide
		Return
	End If
	'Parse JSON string
	Dim jPar As JSONParser
	Dim m As Map
	jPar.Initialize(reply)
	m = jPar.NextObject
	
	Log("RESULT: " & m.Get("Result"))
	'Check result
	If m.Get("Result") <> "0" Then
		If m.Get("Result") = "2" Then
			Select Case mode
				Case "qualificationStart"
					Main.SQL1.ExecNonQuery("DELETE FROM trQuStart")
					DisplayQualificationStart
				Case "qualificationLive"
					Main.SQL1.ExecNonQuery("DELETE FROM trQuLive")
					DisplayQualificationLive
				Case "dayOneStart"
					Main.SQL1.ExecNonQuery("DELETE FROM trDayStart")
					DisplayDayOneStart
				Case "dayOneLive"
					Main.SQL1.ExecNonQuery("DELETE FROM trDayLive")
					DisplayDayLive(1)
				Case "dayTwoStart"
					Main.SQL1.ExecNonQuery("DELETE FROM trDay2Start")
					DisplayDayTwoStart
				Case "dayTwoLive"
					Main.SQL1.ExecNonQuery("DELETE FROM trDay2Live")
					DisplayDayLive(2)
			End Select
		Else
			Select Case mode
				Case "qualificationStart"
					DisplayQualificationStart
				Case "qualificationLive"
					DisplayQualificationLive
				Case "dayOneStart"
					DisplayDayOneStart
				Case "dayOneLive"
					DisplayDayLive(1)
				Case "dayTwoStart"
					DisplayDayTwoStart
				Case "dayTwoLive"
					DisplayDayLive(2)
			End Select
		End If
		ProgressDialogHide
		Return
	End If
	
	Dim updatedOn As String
	Select Case mode
		Case "qualificationStart"
					
		'Iterate through m objects
		Dim n As Int = m.Size - 1
		For i = 0 To n
			'Check if current object is the string indicating the result of server reply
			If m.GetKeyAt(i) = "Result" Then
				If m.GetValueAt(i) = "0" Then
					success = True
				End If
			Else If m.GetKeyAt(i) = "mtime" Then
				updatedOn = m.GetValueAt(i)
			Else
				Dim tmpMap As Map
				tmpMap = m.GetValueAt(i)
				Main.SQL1.AddNonQueryToBatch("INSERT INTO trRiders (trrCode, trrSurname, trrClass, trrTeam, trrName, trrNationality) VALUES (?, ?, ?, ?, ?, ?)", Array As Object(tmpMap.Get("code"), tmpMap.Get("surname"), tmpMap.Get("motoClass"), tmpMap.Get("team"), tmpMap.Get("name"), tmpMap.Get("nationality")))
			
				Main.SQL1.AddNonQueryToBatch("INSERT INTO trQuStart (trqsCode, trqsPosition, trqstime) VALUES (?, ?, ?)", Array As Object(tmpMap.Get("code"), tmpMap.Get("position"), tmpMap.Get("time")))
			End If
		Next
	
		Case "qualificationLive"
			'Iterate through m objects
			Dim n As Int = m.Size - 1
			For i = 0 To n
				'Check if current object is the string indicating the result of server reply
				If m.GetKeyAt(i) = "Result" Then
					If m.GetValueAt(i) = "0" Then
						success = True
					End If
				Else If m.GetKeyAt(i) = "mtime" Then
					updatedOn = m.GetValueAt(i)
					
				Else
					Dim tmpMap As Map
					tmpMap = m.GetValueAt(i)
					Main.SQL1.AddNonQueryToBatch("INSERT INTO trQuLive (trqlCode, trqlPosition, trqlTime, trqlScore, trqlIndex) VALUES (?, ?, ?, ?, ?)", Array As Object(tmpMap.Get("code"), tmpMap.Get("position"), tmpMap.Get("time"), tmpMap.Get("score"), tmpMap.Get("index")))
				End If
			Next
			
		Case "dayOneStart"
			'Iterate through m objects
			Dim n As Int = m.Size - 1
			For i = 0 To n
				'Check if current object is the string indicating the result of server reply
				If m.GetKeyAt(i) = "Result" Then
					If m.GetValueAt(i) = "0" Then
						success = True
					End If
				Else If m.GetKeyAt(i) = "mtime" Then
					updatedOn = m.GetValueAt(i)
					
				Else
					Dim tmpMap As Map
					tmpMap = m.GetValueAt(i)
					Main.SQL1.AddNonQueryToBatch("INSERT INTO trDayStart (trdsCode, trdsPosition, trdsTime) VALUES (?, ?, ?)", Array As Object(tmpMap.Get("code"), tmpMap.Get("position"), tmpMap.Get("time")))
				End If
			Next
			
		Case "dayOneLive"
			'Iterate through m objects
			Dim n As Int = m.Size - 1
			For i = 0 To n
				'Check if current object is the string indicating the result of server reply
				If m.GetKeyAt(i) = "Result" Then
					If m.GetValueAt(i) = "0" Then
						success = True
					End If
				Else If m.GetKeyAt(i) = "mtime" Then
					updatedOn = m.GetValueAt(i)
					
				Else
					Dim tmpMap As Map
					tmpMap = m.GetValueAt(i)
				
					Dim stuntsOne(15) As String
					Dim stuntsTwo(15) As String
					If tmpMap.ContainsKey("one") Then
						Dim stuntsMap As Map
						stuntsMap = tmpMap.Get("one")
						For k = 0 To stuntsMap.Size - 1
							Dim pos As Int = stuntsMap.GetKeyAt(k)
							pos = pos -1
							stuntsOne(pos) = stuntsMap.GetValueAt(k)
						Next
					End If
			
					If tmpMap.ContainsKey("two") Then
						Dim stuntsMap As Map
						stuntsMap = tmpMap.Get("two")
						For k = 0 To stuntsMap.Size - 1
							Dim pos As Int = stuntsMap.GetKeyAt(k)
							pos = pos -1
							stuntsTwo(pos) = stuntsMap.GetValueAt(k)
						Next
					End If
			
					Main.SQL1.AddNonQueryToBatch("INSERT INTO trDayLive (trdlCode, trdlL1, trdlL2, trdlPosition, trdlIndex, trdlPenalty, trdlTotal, trdlStunt1, trdlStunt2, trdlStunt3, trdlStunt4, trdlStunt5, trdlStunt6, trdlStunt7, trdlStunt8, trdlStunt9, trdlStunt10, trdlStunt11, trdlStunt12, trdlStunt13, trdlStunt14, trdlStunt15) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Array As Object(tmpMap.Get("code"), tmpMap.Get("l1"), tmpMap.Get("l2"), tmpMap.Get("position"), tmpMap.Get("index"), tmpMap.Get("penalty"), tmpMap.Get("total"), stuntsOne(0) & "@@" & stuntsTwo(0), stuntsOne(1) & "@@" & stuntsTwo(1), stuntsOne(2) & "@@" & stuntsTwo(2), stuntsOne(3) & "@@" & stuntsTwo(3), stuntsOne(4) & "@@" & stuntsTwo(4), stuntsOne(5) & "@@" & stuntsTwo(5), stuntsOne(6) & "@@" & stuntsTwo(6), stuntsOne(7) & "@@" & stuntsTwo(7), stuntsOne(8) & "@@" & stuntsTwo(8), stuntsOne(9) & "@@" & stuntsTwo(9), stuntsOne(10) & "@@" & stuntsTwo(10), stuntsOne(11) & "@@" & stuntsTwo(11), stuntsOne(12) & "@@" & stuntsTwo(12), stuntsOne(13) & "@@" & stuntsTwo(13), stuntsOne(14) & "@@" & stuntsTwo(14)))
				End If
			Next
			
		Case "dayTwoStart"
			'Iterate through m objects
			Dim n As Int = m.Size - 1
			For i = 0 To n
				'Check if current object is the string indicating the result of server reply
				If m.GetKeyAt(i) = "Result" Then
					If m.GetValueAt(i) = "0" Then
						success = True
					End If
				Else If m.GetKeyAt(i) = "mtime" Then
					updatedOn = m.GetValueAt(i)
					
				Else
					Dim tmpMap As Map
					tmpMap = m.GetValueAt(i)
					Main.SQL1.AddNonQueryToBatch("INSERT INTO trDay2Start (trds2Code, trds2Position, trds2Time) VALUES (?, ?, ?)", Array As Object(tmpMap.Get("code"), tmpMap.Get("position"), tmpMap.Get("time")))
				End If
			Next
			
		Case "dayTwoLive"
			'Iterate through m objects
			Dim n As Int = m.Size - 1
			For i = 0 To n
				'Check if current object is the string indicating the result of server reply
				If m.GetKeyAt(i) = "Result" Then
					If m.GetValueAt(i) = "0" Then
						success = True
					End If
				Else If m.GetKeyAt(i) = "mtime" Then
					updatedOn = m.GetValueAt(i)
					
				Else
					Dim tmpMap As Map
					tmpMap = m.GetValueAt(i)
				
					Dim stuntsOne(15) As String
					Dim stuntsTwo(15) As String
					If tmpMap.ContainsKey("one") Then
						Dim stuntsMap As Map
						stuntsMap = tmpMap.Get("one")
						For k = 0 To stuntsMap.Size - 1
							Dim pos As Int = stuntsMap.GetKeyAt(k)
							pos = pos -1
							stuntsOne(pos) = stuntsMap.GetValueAt(k)
						Next
					End If
			
					If tmpMap.ContainsKey("two") Then
						Dim stuntsMap As Map
						stuntsMap = tmpMap.Get("two")
						For k = 0 To stuntsMap.Size - 1
							Dim pos As Int = stuntsMap.GetKeyAt(k)
							pos = pos -1
							stuntsTwo(pos) = stuntsMap.GetValueAt(k)
						Next
					End If
			
					Main.SQL1.AddNonQueryToBatch("INSERT INTO trDay2Live (trdl2Code, trdl2L1, trdl2L2, trdl2Position, trdl2Index, trdl2Penalty, trdl2Total, trdl2Stunt1, trdl2Stunt2, trdl2Stunt3, trdl2Stunt4, trdl2Stunt5, trdl2Stunt6, trdl2Stunt7, trdl2Stunt8, trdl2Stunt9, trdl2Stunt10, trdl2Stunt11, trdl2Stunt12, trdl2Stunt13, trdl2Stunt14, trdl2Stunt15) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Array As Object(tmpMap.Get("code"), tmpMap.Get("l1"), tmpMap.Get("l2"), tmpMap.Get("position"), tmpMap.Get("index"), tmpMap.Get("penalty"), tmpMap.Get("total"), stuntsOne(0) & "@@" & stuntsTwo(0), stuntsOne(1) & "@@" & stuntsTwo(1), stuntsOne(2) & "@@" & stuntsTwo(2), stuntsOne(3) & "@@" & stuntsTwo(3), stuntsOne(4) & "@@" & stuntsTwo(4), stuntsOne(5) & "@@" & stuntsTwo(5), stuntsOne(6) & "@@" & stuntsTwo(6), stuntsOne(7) & "@@" & stuntsTwo(7), stuntsOne(8) & "@@" & stuntsTwo(8), stuntsOne(9) & "@@" & stuntsTwo(9), stuntsOne(10) & "@@" & stuntsTwo(10), stuntsOne(11) & "@@" & stuntsTwo(11), stuntsOne(12) & "@@" & stuntsTwo(12), stuntsOne(13) & "@@" & stuntsTwo(13), stuntsOne(14) & "@@" & stuntsTwo(14)))
				End If
			Next
	End Select
	
	'If reply result Is "successful", execute the batched queries
	If success Then
		Select Case mode
			Case "qualificationStart"
				queryType = 5
				Main.SQL1.ExecNonQuery("DELETE FROM trQuStart")
				
				Main.SQL1.ExecNonQuery("DELETE FROM trRiders")
				
				Main.SQL1.ExecNonQueryBatch("SQL")
				
				Log("UPDATED ON: " & updatedOn)
				Main.SQL1.ExecNonQuery("UPDATE trQuStart SET trqsUpdatedOn = '" & updatedOn & "'")
			Case "qualificationLive"
				queryType = 0
				Main.SQL1.ExecNonQuery("DELETE FROM trQuLive")
				Main.SQL1.ExecNonQueryBatch("SQL")
				
				Main.SQL1.ExecNonQuery("UPDATE trQuLive SET trqlUpdatedOn = '" & updatedOn & "'")
			Case "dayOneStart"
				queryType = 1
				Main.SQL1.ExecNonQuery("DELETE FROM trDayStart")
				Main.SQL1.ExecNonQueryBatch("SQL")
				
				Main.SQL1.ExecNonQuery("UPDATE trDayStart SET trdsUpdatedOn = '" & updatedOn & "'")
			Case "dayOneLive"
				queryType = 2
				Main.SQL1.ExecNonQuery("DELETE FROM trDayLive")
				Main.SQL1.ExecNonQueryBatch("SQL")
				
				Main.SQL1.ExecNonQuery("UPDATE trDayLive SET trdlUpdatedOn = '" & updatedOn & "'")
			Case "dayTwoStart"
				queryType = 3
				Main.SQL1.ExecNonQuery("DELETE FROM trDay2Start")
				Main.SQL1.ExecNonQueryBatch("SQL")
				
				Main.SQL1.ExecNonQuery("UPDATE trDay2Start SET trds2UpdatedOn = '" & updatedOn & "'")
			Case "dayTwoLive"
				queryType = 4
				Main.SQL1.ExecNonQuery("DELETE FROM trDay2Live")
				Main.SQL1.ExecNonQueryBatch("SQL")
				
				Main.SQL1.ExecNonQuery("UPDATE trDay2Live SET trdl2UpdatedOn = '" & updatedOn & "'")
		End Select
	
	Else
		ProgressDialogHide
		Msgbox("Error while updating db", "Error!")
		Return
	End If
End Sub

'--------------------------------------------------------------------------
'-------------------------- DISPLAY METHODS -------------------------------
'--------------------------------------------------------------------------
Sub DisplayDayLive(dayNum As Int)
	Dim placedPanels As Int = 0
	Dim placedClassPanels As Int = 0
	Dim panelHeight As Int = SVResults.Height / 8
	Dim classPanelHeight As Int = SVResults.Height / 14 
	
	ClearScreen
	
	Dim crs As Cursor
	If dayNum = 1 Then
		crs = Main.SQL1.ExecQuery("SELECT trrCode, trrSurname, trrNationality, trrClass, trrTeam, trdlL1 As L1, trdlL2 As L2, trdlPosition As position, trdlTotal As total, trdlPenalty As penalty, trdlStunt1 As section1, trdlStunt2 As section2, trdlStunt3 As section3, trdlStunt4 As section4, trdlStunt5 As section5, trdlStunt6 As section6, trdlStunt7 As section7, trdlStunt8 As section8, trdlStunt9 As section9, trdlStunt10 As section10, trdlStunt11 As section11, trdlStunt12 As section12, trdlStunt13 As section13, trdlStunt14 As section14, trdlStunt15 As section15 FROM trRiders, trDayLive WHERE trRiders.trrCode = trDayLive.trdlCode ORDER BY trdlIndex ASC")
	Else
		crs = Main.SQL1.ExecQuery("SELECT trrCode, trrSurname, trrNationality, trrClass, trrTeam, trdl2L1 As L1, trdl2L2 As L2, trdl2Position As position, trdl2Total As total, trdl2Penalty As penalty, trdl2Stunt1 As section1, trdl2Stunt2 As section2, trdl2Stunt3 As section3, trdl2Stunt4 As section4, trdl2Stunt5 As section5, trdl2Stunt6 As section6, trdl2Stunt7 As section7, trdl2Stunt8 As section8, trdl2Stunt9 As section9, trdl2Stunt10 As section10, trdl2Stunt11 As section11, trdl2Stunt12 As section12, trdl2Stunt13 As section13, trdl2Stunt14 As section14, trdl2Stunt15 As section15 FROM trRiders, trDay2Live WHERE trRiders.trrCode = trDay2Live.trdl2Code ORDER BY trdl2Index ASC")
	End If

	If crs.RowCount > 0 Then
		lblNoData.Visible = False
	End If
	
	'Display Qualification Start Results
	Dim currentClass As String = ""
	Dim n As Int = crs.RowCount - 1
	For i = 0 To n
		crs.Position = i
		'Log(crs.GetString("trrCode") & "#" & crs.GetString("section1") & "#" & crs.GetString("section2") & "#" & crs.GetString("section3") & "#" & crs.GetString("section4") & "#" & crs.GetString("section5") & "#" & crs.GetString("section6") & "#" & crs.GetString("section7") & "#" & crs.GetString("section8") & "#" & crs.GetString("section9") & "#" & crs.GetString("section10") & "#" & crs.GetString("section11") & "#" & crs.GetString("section12") & "#" & crs.GetString("section13") & "#" & crs.GetString("section14") & "#" & crs.GetString("section15"))		
		'Check If a row displaying current class should be added
		If currentClass <> crs.GetString("trrClass") Then
			Dim classPanel As Panel
			classPanel.Initialize("classPanel")
			classPanel.Color = Colors.RGB(34, 180, 231)
			pnlResults.AddView(classPanel, 0, (panelHeight * placedPanels) + (classPanelHeight * placedClassPanels), SVResults.Width, classPanelHeight)
			placedClassPanels = placedClassPanels + 1
			currentClass = crs.GetString("trrClass")
			
			'Add label displaying current class name
			Dim lblClassName As Label
			lblClassName.Initialize("lblClassName")
			lblClassName.Typeface = Typeface.LoadFromAssets("helvetica.ttf")
			lblClassName.Text = currentClass
			lblClassName.TextSize = lblSupport.TextSize
			lblClassName.TextColor = Colors.RGB(255, 255, 255)
			lblClassName.Gravity = Gravity.CENTER
			classPanel.AddView(lblClassName, 0, 0, classPanel.Width, classPanel.Height)	
		End If	
		
		'Create And place a new panel For current result
		Dim resultPanel As Panel
		resultPanel.Initialize("resultPanel")
		If i Mod 2 = 0 Then
			resultPanel.Color = Colors.RGB(60, 60, 60)
		Else
			resultPanel.Color = Colors.RGB(0, 0, 0)
		End If
		
		pnlResults.AddView(resultPanel, 0, (panelHeight * placedPanels) + (classPanelHeight * placedClassPanels), SVResults.Width, panelHeight)
		placedPanels = placedPanels + 1	
				
		'Add label displaying current result's position
		Dim lblPosition As Label
		lblPosition.Initialize("lblPosition")
		lblPosition.Typeface = Typeface.LoadFromAssets("helvetica.ttf")
		lblPosition.Text = crs.GetString("position")
		lblPosition.TextSize = lblSupport.TextSize-2
		lblPosition.TextColor = Colors.RGB(34, 180, 231)
		lblPosition.Gravity = Gravity.CENTER
		resultPanel.AddView(lblPosition, lblPos.Left, 5dip, lblPos.Width, resultPanel.Height / 2.8)	
		 
		'Add label displaying current rider number
		Dim lblNumber As Label
		lblNumber.Initialize("lblNumber")
		lblNumber.Typeface = Typeface.LoadFromAssets("helvetica.ttf")
		lblNumber.Text = crs.GetString("trrCode")
		lblNumber.TextSize = lblSupport.TextSize-2
		lblNumber.TextColor = Colors.RGB(255, 255, 255)
		lblNumber.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblNumber, lblNo.Left, 5dip, lblNo.Width, resultPanel.Height / 2.8)	
		
		'Add label displaying current rider name
		Dim lblName As Label
		lblName.Initialize("lblName")
		lblName.Typeface = Typeface.LoadFromAssets("helvetica.ttf")
		Dim riderName As String = crs.GetString("trrSurname")
		If riderName.Length > 11 Then
			lblName.Text = riderName.SubString2(0, 11)
		Else
			lblName.Text = riderName
		End If
		lblName.TextSize = lblSupport.TextSize - 3
		lblName.TextColor = Colors.RGB(255, 255, 255)
		lblName.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblName, lblRider.Left, 5dip, lblRider.Width, resultPanel.Height / 3)
		
		'Add label displaying current rider nationality
		Dim lblNationality As Label
		lblNationality.Initialize("lblNationality")
		lblNationality.Typeface = Typeface.LoadFromAssets("helvetica.ttf")
		lblNationality.Text = crs.GetString("trrNationality")
		lblNationality.TextSize = lblSupport.TextSize - 5
		lblNationality.TextColor = Colors.RGB(255, 255, 255)
		lblNationality.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblNationality, lblNo.Left, (resultPanel.Height / 3) + 7dip, lblNo.Width, resultPanel.Height / 3)
		
		'Add label displaying current rider team
		Dim lblTeam As Label
		lblTeam.Initialize("lblTeam")
		lblTeam.Typeface = Typeface.LoadFromAssets("helvetica.ttf")
		lblTeam.Text = crs.GetString("trrTeam")
		lblTeam.TextSize = lblSupport.TextSize - 5
		lblTeam.TextColor = Colors.RGB(255, 255, 255)
		lblTeam.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblTeam, lblRider.Left, (resultPanel.Height / 3) + 7dip, lblRider.Width, resultPanel.Height / 3)
		
		'Add labels displaying section scores
		Dim placedSectionLabels As Int = 0
		Dim sectionLabelWidth As Int = (lblClass.Width - 15) / 15
		For j = 0 To 14
			Dim lblIndex As Label
			lblIndex.Initialize("lblIndex")
			lblIndex.Text = j + 1
			lblIndex.TextSize = lblSupport.TextSize - 7
			lblIndex.TextColor = Colors.RGB(150, 150, 150)
			lblIndex.Gravity = Gravity.CENTER
			resultPanel.AddView(lblIndex, lblClass.Left + (placedSectionLabels * sectionLabelWidth), 0, sectionLabelWidth, resultPanel.Height / 3)
			
			Dim str() As String
			Dim lblScore1 As Label
			Dim lblScore2 As Label
			lblScore1.Initialize("lblScore1")
			lblScore2.Initialize("lblScore2")
			lblScore1.Text = ""
			lblScore2.Text = ""
			Select Case j
			Case 0
				str = Regex.split("@@", crs.GetString("section1"))
			Case 1
				str = Regex.split("@@", crs.GetString("section2"))
			Case 2
				str = Regex.split("@@", crs.GetString("section3"))
			Case 3
				str = Regex.split("@@", crs.GetString("section4"))
			Case 4
				str = Regex.split("@@", crs.GetString("section5"))
			Case 5
				str = Regex.split("@@", crs.GetString("section6"))
			Case 6
				str = Regex.split("@@", crs.GetString("section7"))
			Case 7
				str = Regex.split("@@", crs.GetString("section8"))
			Case 8
				str = Regex.split("@@", crs.GetString("section9"))
			Case 9
				str = Regex.split("@@", crs.GetString("section10"))
			Case 10
				str = Regex.split("@@", crs.GetString("section11"))
			Case 11
				str = Regex.split("@@", crs.GetString("section12"))
			Case 12
				str = Regex.split("@@", crs.GetString("section13"))
			Case 13
				str = Regex.split("@@", crs.GetString("section14"))
			Case 14
				str = Regex.split("@@", crs.GetString("section15"))
			End Select
			
			If str.Length = 1 Then
				If str(0) = "-" Then
					lblScore1.Text = ""
				Else
					lblScore1.Text = str(0)
				End If
			Else If str.Length = 2 Then
				Dim s1 As String
				Dim s2 As String
				If str(0) = "-" Then
					s1 = ""
				Else
					s1 = str(0)
				End If
				
				If str(1) = "-" Then
					s2 = ""
				Else
					s2 = str(1)
				End If
				lblScore1.Text = s1 & CRLF & s2
			End If
				
			lblScore1.TextSize = lblSupport.TextSize - 5
			lblScore1.TextColor = Colors.RGB(255, 255, 255)
			lblScore1.Gravity = Gravity.CENTER_HORIZONTAL
			resultPanel.AddView(lblScore1, lblClass.Left + (placedSectionLabels * sectionLabelWidth), resultPanel.Height / 3, sectionLabelWidth, 2 * (resultPanel.Height / 3))
		
			placedSectionLabels = placedSectionLabels + 1
		Next
		
		'Add label L1
		Dim lblL1 As Label
		lblL1.Initialize("lblL1")
		lblL1.Text = "L1" & CRLF & "L2"
		lblL1.TextSize = lblSupport.TextSize - 4
		lblL1.TextColor = Colors.RGB(150, 150, 150)
		lblL1.Gravity = Gravity.CENTER_HORIZONTAL
		'resultPanel.AddView(lblL1, lblTime.Left - sectionLabelWidth - 7dip, resultPanel.Height / 3, sectionLabelWidth * 1.2, 2 * (resultPanel.Height / 3))
		resultPanel.AddView(lblL1, lblTime.Left - sectionLabelWidth - 8dip, resultPanel.Height / 3,sectionLabelWidth * 1.3, 2 * (resultPanel.Height / 3))
		'Add label displaying result of L1
		Dim lblL1Result As Label
		lblL1Result.Initialize("lblL1Result")
		lblL1Result.Text = crs.GetString("L1") & CRLF & crs.GetString("L2")
		lblL1Result.TextSize = lblSupport.TextSize - 4
		lblL1Result.TextColor = Colors.RGB(255, 255, 255)
		lblL1Result.Gravity = Gravity.CENTER_HORIZONTAL
		resultPanel.AddView(lblL1Result, lblL1.Left + lblL1.Width, resultPanel.Height / 3, sectionLabelWidth * 1.3, 2 * (resultPanel.Height / 3))
		
		'Add label displaying current rider's penalty
		Dim lblRiderPenalty As Label
		lblRiderPenalty.Initialize("lblTime")
		lblRiderPenalty.Text = crs.GetString("penalty")
		lblRiderPenalty.TextSize = lblSupport.TextSize - 2
		lblRiderPenalty.TextColor = Colors.RGB(255, 255, 255)
		lblRiderPenalty.Gravity = Gravity.CENTER
		resultPanel.AddView(lblRiderPenalty, lblTime.Left, 0, lblTime.Width, resultPanel.Height)	

		'Add label displaying current rider's total
		Dim lblRiderTotal As Label
		lblRiderTotal.Initialize("lblTime")
		lblRiderTotal.Text = crs.GetString("total")
		lblRiderTotal.TextSize = lblSupport.TextSize
		lblRiderTotal.TextColor = Colors.RGB(34, 180, 231)
		lblRiderTotal.Gravity = Gravity.CENTER
		resultPanel.AddView(lblRiderTotal, lblTotal.Left, 0, lblTotal.Width, resultPanel.Height)	
	Next
 	pnlResults.Height = (placedPanels * panelHeight) + (placedClassPanels * classPanelHeight)
	
	SVResults.ScrollPosition = currentScroll
	ProgressDialogHide
End Sub

Sub DisplayDayOneStart
	Dim placedPanels As Int = 0
	Dim panelHeight As Int = SVResults.Height / 14
	
	ClearScreen
	
	Dim crs As Cursor
	crs = Main.SQL1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trdsTime FROM trRiders, trDayStart WHERE trRiders.trrCode = trDayStart.trdsCode ORDER BY trdsPosition ASC")
	
	If crs.RowCount > 0 Then
		lblNoData.Visible = False
	End If
	
	'Display Qualification Start Results
	Dim n As Int = crs.RowCount - 1
	For i = 0 To n
		crs.Position = i
				
		'Create And place a new panel For current result
		Dim resultPanel As Panel
		resultPanel.Initialize("resultPanel")
		If i Mod 2 = 0 Then
			resultPanel.Color = Colors.RGB(60, 60, 60)
		Else
			resultPanel.Color = Colors.RGB(0, 0, 0)
		End If
		
		pnlResults.AddView(resultPanel, 0, (panelHeight * placedPanels), SVResults.Width, panelHeight)
		placedPanels = placedPanels + 1	
				
		'Add label displaying current result index
		Dim lblIndex As Label
		lblIndex.Initialize("lblIndex")
		lblIndex.Text = i + 1
		lblIndex.TextSize = lblSupport.TextSize
		lblIndex.TextColor = Colors.RGB(150, 150, 150)
		lblIndex.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblIndex, 5dip, 0, resultPanel.Width / 10, resultPanel.Height)	
		
		'Add label displaying current rider number
		Dim lblNumber As Label
		lblNumber.Initialize("lblNumber")
		lblNumber.Text = crs.GetString("trrCode")
		lblNumber.TextSize = lblSupport.TextSize
		lblNumber.TextColor = Colors.RGB(255, 255, 255)
		lblNumber.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblNumber, lblNo.Left, 0, lblNo.Width, resultPanel.Height)	
		
		'Add label displaying current rider name
		Dim lblName As Label
		lblName.Initialize("lblName")
		Dim riderName As String = crs.GetString("trrSurname")
		If riderName.Length > 12 Then
			lblName.Text = riderName.SubString2(0, 12)
		Else
			lblName.Text = riderName
		End If
		lblName.TextSize = lblSupport.TextSize
		lblName.TextColor = Colors.RGB(255, 255, 255)
		lblName.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblName, lblRider.Left, 0, lblRider.Width, resultPanel.Height)
		
		'Add label displaying current rider class (or score, depending on what results are displayed)
		Dim lblRiderClass As Label
		lblRiderClass.Initialize("lblClass")
		lblRiderClass.Text = crs.GetString("trrClass")
		lblRiderClass.TextSize = lblSupport.TextSize
		lblRiderClass.TextColor = Colors.RGB(150, 150, 150)
		lblRiderClass.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblRiderClass, lblClass.Left, 0, lblClass.Left - lblTime.Left, resultPanel.Height)
		
		'Add label displaying current rider's time
		Dim lblRiderTime As Label
		lblRiderTime.Initialize("lblTime")
		lblRiderTime.Text = crs.GetString("trdsTime")
		lblRiderTime.TextSize = lblSupport.TextSize
		lblRiderTime.TextColor = Colors.RGB(34, 180, 231)
		lblRiderTime.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblRiderTime, lblTime.Left, 0, lblTime.Width, resultPanel.Height)	
	Next
 	pnlResults.Height = (placedPanels * panelHeight)
	
	SVResults.ScrollPosition = currentScroll
	ProgressDialogHide
End Sub

Sub DisplayDayTwoStart
	Dim placedPanels As Int = 0
	Dim panelHeight As Int = SVResults.Height / 14
	
	ClearScreen
	
	Dim crs As Cursor
	crs = Main.SQL1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trds2Time FROM trRiders, trDay2Start WHERE trRiders.trrCode = trDay2Start.trds2Code ORDER BY trds2Position ASC")
	
	If crs.RowCount > 0 Then
		lblNoData.Visible = False
	End If
	
	'Display Qualification Start Results
	Dim n As Int = crs.RowCount - 1
	For i = 0 To n
		crs.Position = i
				
		'Create And place a new panel For current result
		Dim resultPanel As Panel
		resultPanel.Initialize("resultPanel")
		If i Mod 2 = 0 Then
			resultPanel.Color = Colors.RGB(60, 60, 60)
		Else
			resultPanel.Color = Colors.RGB(0, 0, 0)
		End If
		
		pnlResults.AddView(resultPanel, 0, (panelHeight * placedPanels), SVResults.Width, panelHeight)
		placedPanels = placedPanels + 1	
				
		'Add label displaying current result index
		Dim lblIndex As Label
		lblIndex.Initialize("lblIndex")
		lblIndex.Text = i + 1
		lblIndex.TextSize = lblSupport.TextSize
		lblIndex.TextColor = Colors.RGB(150, 150, 150)
		lblIndex.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblIndex, 5dip, 0, resultPanel.Width / 10, resultPanel.Height)	
		
		'Add label displaying current rider number
		Dim lblNumber As Label
		lblNumber.Initialize("lblNumber")
		lblNumber.Text = crs.GetString("trrCode")
		lblNumber.TextSize = lblSupport.TextSize
		lblNumber.TextColor = Colors.RGB(255, 255, 255)
		lblNumber.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblNumber, lblNo.Left, 0, lblNo.Width, resultPanel.Height)	
		
		'Add label displaying current rider name
		Dim lblName As Label
		lblName.Initialize("lblName")
		Dim riderName As String = crs.GetString("trrSurname")
		If riderName.Length > 12 Then
			lblName.Text = riderName.SubString2(0, 12)
		Else
			lblName.Text = riderName
		End If
		lblName.TextSize = lblSupport.TextSize
		lblName.TextColor = Colors.RGB(255, 255, 255)
		lblName.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblName, lblRider.Left, 0, lblRider.Width, resultPanel.Height)
		
		'Add label displaying current rider class (or score, depending on what results are displayed)
		Dim lblRiderClass As Label
		lblRiderClass.Initialize("lblClass")
		lblRiderClass.Text = crs.GetString("trrClass")
		lblRiderClass.TextSize = lblSupport.TextSize
		lblRiderClass.TextColor = Colors.RGB(150, 150, 150)
		lblRiderClass.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblRiderClass, lblClass.Left, 0, lblClass.Left - lblTime.Left, resultPanel.Height)
		
		'Add label displaying current rider's time
		Dim lblRiderTime As Label
		lblRiderTime.Initialize("lblTime")
		lblRiderTime.Text = crs.GetString("trds2Time")
		lblRiderTime.TextSize = lblSupport.TextSize
		lblRiderTime.TextColor = Colors.RGB(34, 180, 231)
		lblRiderTime.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblRiderTime, lblTime.Left, 0, lblTime.Width, resultPanel.Height)	
	Next
 	pnlResults.Height = (placedPanels * panelHeight)
	
	SVResults.ScrollPosition = currentScroll
	ProgressDialogHide
End Sub

Sub DisplayQualificationLive
	Dim placedPanels As Int = 0
	Dim panelHeight As Int = SVResults.Height / 14

	ClearScreen
	
	Dim crs As Cursor
	crs = Main.SQL1.ExecQuery("SELECT trqlPosition, trqlIndex, trrCode, trrSurname, trrClass, trqlScore, trqlTime FROM trRiders, trQuLive WHERE trRiders.trrCode = trQuLive.trqlCode ORDER BY trqlPosition ASC")
	
	If crs.RowCount > 0 Then
		lblNoData.Visible = False
	End If
	
	'Display Qualification Live Results
	Dim currentClass As String = ""
	Dim n As Int = crs.RowCount - 1
	For i = 0 To n
		crs.Position = i
						
		'Check If a row displaying current class should be added
		If currentClass <> crs.GetString("trrClass") Then
			Dim classPanel As Panel
			classPanel.Initialize("classPanel")
			classPanel.Color = Colors.RGB(34, 180, 231)
			pnlResults.AddView(classPanel, 0, (panelHeight * placedPanels), SVResults.Width, panelHeight)
			placedPanels = placedPanels + 1
			currentClass = crs.GetString("trrClass")
			
			'Add label displaying current class name
			Dim lblClassName As Label
			lblClassName.Initialize("lblClassName")
			lblClassName.Text = currentClass
			lblClassName.TextSize = lblSupport.TextSize
			lblClassName.TextColor = Colors.RGB(255, 255, 255)
			lblClassName.Gravity = Gravity.CENTER
			classPanel.AddView(lblClassName, 0, 0, classPanel.Width, classPanel.Height)	
		End If		
				
		'Create and place a new panel for current result
		Dim resultPanel As Panel
		resultPanel.Initialize("resultPanel")
		If i Mod 2 = 0 Then
			resultPanel.Color = Colors.RGB(60, 60, 60)
		Else
			resultPanel.Color = Colors.RGB(0, 0, 0)
		End If
		
		pnlResults.AddView(resultPanel, 0, (panelHeight * placedPanels), SVResults.Width, panelHeight)
		placedPanels = placedPanels + 1	
		
		'Add label displaying current result's position
		Dim lblPosition As Label
		lblPosition.Initialize("lblPosition")
		lblPosition.Text = crs.GetString("trqlIndex")
		lblPosition.TextSize = lblSupport.TextSize
		lblPosition.TextColor = Colors.RGB(34, 180, 231)
		lblPosition.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblPosition, lblPos.Left, 0, lblPos.Width, resultPanel.Height)	
		
		'Add label displaying current result's rider code
		Dim lblCode As Label
		lblCode.Initialize("lblCode")
		lblCode.Text = crs.GetString("trrCode")
		lblCode.TextSize = lblSupport.TextSize
		lblCode.TextColor = Colors.RGB(255, 255, 255)
		lblCode.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblCode, lblNo.Left, 0, lblNo.Width, resultPanel.Height)
		
		'Add label displaying current result's rider name
		Dim lblRiderName As Label
		lblRiderName.Initialize("lblRiderName")
		lblRiderName.Text = crs.GetString("trrSurname")
		lblRiderName.TextSize = lblSupport.TextSize
		lblRiderName.TextColor = Colors.RGB(255, 255, 255)
		lblRiderName.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblRiderName, lblRider.Left, 0, lblRider.Width, resultPanel.Height)
		
		'Add label displaying current result's score
		Dim lblScore As Label
		lblScore.Initialize("lblScore")
		lblScore.Text = crs.GetInt("trqlScore")
		lblScore.TextSize = lblSupport.TextSize
		lblScore.TextColor = Colors.RGB(255, 255, 255)
		lblScore.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblScore, lblClass.Left, 0, lblClass.Width, resultPanel.Height)
		
		'Add label displaying current result's time
		Dim lblResultTime As Label
		lblResultTime.Initialize("lblTime")
		lblResultTime.Text = crs.GetString("trqlTime")
		lblResultTime.TextSize = lblSupport.TextSize
		lblResultTime.TextColor = Colors.RGB(34, 180, 231)
		lblResultTime.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblResultTime, lblTime.Left, 0, lblTime.Width, resultPanel.Height)
		
	Next
 	pnlResults.Height = (placedPanels * panelHeight)
	
	SVResults.ScrollPosition = currentScroll
	ProgressDialogHide
End Sub

Sub SQL_NonQueryComplete (Success As Boolean)
	'Check if query was succesful
	If Success = False Then
		ProgressDialogHide
		Msgbox(LastException, "Error!")
		Return
	End If

	Select Case queryType
		Case 0
			DisplayQualificationLive
		Case 1
			DisplayDayOneStart
		Case 2
			DisplayDayLive(1)
		Case 3
			DisplayDayTwoStart
		Case 4
			DisplayDayLive(2)
		Case 5
			DisplayQualificationStart
	End Select		
End Sub

Sub DisplayQualificationStart
	Dim placedPanels As Int = 0
	Dim panelHeight As Int = SVResults.Height / 14
	
	ClearScreen
	
	Dim crs As Cursor
	crs = Main.SQL1.ExecQuery("SELECT trrCode, trrSurname, trrClass, trqsTime FROM trRiders, trQuStart WHERE trRiders.trrCode = trQuStart.trqsCode ORDER BY trqsPosition ASC")
	
	If crs.RowCount > 0 Then
		lblNoData.Visible = False
	End If
	
	'Display Qualification Start Results
	Dim n As Int = crs.RowCount - 1
	For i = 0 To n
		crs.Position = i
				
		'Create And place a new panel For current result
		Dim resultPanel As Panel
		resultPanel.Initialize("resultPanel")
		If i Mod 2 = 0 Then
			resultPanel.Color = Colors.RGB(60, 60, 60)
		Else
			resultPanel.Color = Colors.RGB(0, 0, 0)
		End If
		
		pnlResults.AddView(resultPanel, 0, (panelHeight * placedPanels), SVResults.Width, panelHeight)
		placedPanels = placedPanels + 1	
				
		'Add label displaying current result index
		Dim lblIndex As Label
		lblIndex.Initialize("lblIndex")
		lblIndex.Text = i + 1
		lblIndex.TextSize = lblSupport.TextSize
		lblIndex.TextColor = Colors.RGB(150, 150, 150)
		lblIndex.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblIndex, 5dip, 0, resultPanel.Width / 10, resultPanel.Height)	
		
		'Add label displaying current rider number
		Dim lblNumber As Label
		lblNumber.Initialize("lblNumber")
		lblNumber.Text = crs.GetString("trrCode")
		lblNumber.TextSize = lblSupport.TextSize
		lblNumber.TextColor = Colors.RGB(255, 255, 255)
		lblNumber.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblNumber, lblNo.Left, 0, lblNo.Width, resultPanel.Height)	
		
		'Add label displaying current rider name
		Dim lblName As Label
		lblName.Initialize("lblName")
		Dim riderName As String = crs.GetString("trrSurname")
		If riderName.Length > 12 Then
			lblName.Text = riderName.SubString2(0, 12)
		Else
			lblName.Text = riderName
		End If
		lblName.TextSize = lblSupport.TextSize
		lblName.TextColor = Colors.RGB(255, 255, 255)
		lblName.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblName, lblRider.Left, 0, lblRider.Width, resultPanel.Height)
		
		'Add label displaying current rider class (or score, depending on what results are displayed)
		Dim lblRiderClass As Label
		lblRiderClass.Initialize("lblClass")
		lblRiderClass.Text = crs.GetString("trrClass")
		lblRiderClass.TextSize = lblSupport.TextSize
		lblRiderClass.TextColor = Colors.RGB(150, 150, 150)
		lblRiderClass.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblRiderClass, lblClass.Left, 0, lblRider.Left + lblRider.Width, resultPanel.Height)
		
		'Add label displaying current rider's time
		Dim lblRiderTime As Label
		lblRiderTime.Initialize("lblTime")
		lblRiderTime.Text = crs.GetString("trqsTime")
		lblRiderTime.TextSize = lblSupport.TextSize
		lblRiderTime.TextColor = Colors.RGB(34, 180, 231)
		lblRiderTime.Gravity = Gravity.CENTER_VERTICAL
		resultPanel.AddView(lblRiderTime, lblTime.Left, 0, lblTime.Width, resultPanel.Height)	
	Next
 	pnlResults.Height = (placedPanels * panelHeight)
	
	SVResults.ScrollPosition = currentScroll
	ProgressDialogHide
End Sub

Sub ClearScreen
	lblNoData.Visible = True
	'Remove all previously displayed results
	pnlResults.RemoveAllViews
End Sub

Sub lblDay1_Click
	currentScroll = 0
	SetActiveTab(False, True, isStart)
End Sub

Sub lblDay2_Click
	currentScroll = 0
	SetActiveTab(False, False, isStart)
End Sub

Sub lblQualification_Click
	currentScroll = 0
	SetActiveTab(True, False, isStart)
End Sub

Sub lblStart_Click
	currentScroll = 0
	SetActiveTab(isQualification, isDayOne, True)
End Sub

Sub lblLive_Click
	currentScroll = 0
	SetActiveTab(isQualification, isDayOne, False)
End Sub

Sub imgArrowStart_Click
	currentScroll = 0
	SetActiveTab(isQualification, isDayOne, True)
End Sub

Sub imgArrowLive_Click
	currentScroll = 0
	SetActiveTab(isQualification, isDayOne, False)
End Sub

Sub imgHome_Click
	StartActivity(Main)
	Activity.Finish
End Sub

Sub refreshTimer_tick
	currentScroll = SVResults.ScrollPosition
	If isStart = False Then
		SetActiveTab(isQualification, isDayOne, isStart)
	End If
End Sub