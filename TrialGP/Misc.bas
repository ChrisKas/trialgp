﻿Type=StaticCode
Version=6.31
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
Sub Process_Globals

End Sub

Sub HorizontalCenterView(v As View, parent As View)
    v.Left = parent.Width / 2 - v.Width / 2
End Sub

Sub VerticalCenterView(v As View, parent As View)
    v.Top = parent.Height / 2 - v.Height / 2
End Sub

Sub SetTextShadow(pView As View, pRadius As Float, pDx As Float, pDy As Float, pColor As Int)
   Dim ref As Reflector
   
   ref.Target = pView
   ref.RunMethod4("setShadowLayer", Array As Object(pRadius, pDx, pDy, pColor), Array As String("java.lang.float", "java.lang.float", "java.lang.float", "java.lang.int"))
End Sub